using System;
using System.Collections.Generic;
using System.Linq;
using Common.Data;
using Common.Settings;
using Common.Signals;
using UniRx;
using UnityEngine;
using Utils;
using Zenject;
using Upgrades;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Bricks
{
    public class BricksController
    {
        public IObservable<string> AreFloorBricksInitialized => _initializationSubject;
        private ReactiveDictionary<string, ReactiveCollection<BaseBrick>> _floorsBricks =
            new ReactiveDictionary<string, ReactiveCollection<BaseBrick>>();
        private Dictionary<BaseBrick, IDisposable> _isAlvieStreams = new Dictionary<BaseBrick, IDisposable>();
        private SignalBus _signalBus;
        private readonly List<BrickSettings> _brickSettings;
        private Subject<string> _initializationSubject = new Subject<string>();
        private readonly UpgradesController _upgradesController;

        //Temporary settings
        private readonly int _brickTypesCount;
        private readonly int[] _brickWeights = { 1, 1, 1, 1 };
        private IDisposable _blowUpStream;

        public BricksController(SignalBus signalBus, List<BrickSettings> brickSettings, UpgradesController upgradesController)
        {
            _signalBus = signalBus;
            _brickSettings = brickSettings;
            _brickTypesCount = brickSettings.Count;
            _upgradesController = upgradesController;

        }
        public void Initialize(string floorId, BaseBrick[] bricks)
        {
            InitBlocksContainer(floorId);
            var ordinaryBricks = bricks.Where(b => b.BrickType.Equals(BrickType.Ordinary)).ToList();
            var otherBricks = bricks.Where(b => !b.BrickType.Equals(BrickType.Ordinary)).ToList();

            var topPriority = ordinaryBricks.Where(b => b is BalconyBrick).ToList();

            var bricksX1 = ordinaryBricks.Where(b => b.BrickSizeType.Equals(BrickSizeType.X1)).Except(topPriority).ToList();
            var bricksX2 = ordinaryBricks.Where(b => b.BrickSizeType.Equals(BrickSizeType.X2)).Except(topPriority).ToList();
            var bricksX4 = ordinaryBricks.Where(b => b.BrickSizeType.Equals(BrickSizeType.X4Cube) || b.BrickSizeType.Equals(BrickSizeType.X4Vertical))
                .Except(topPriority).ToList();

            var brickCounts = new int[4];
            for (var i = _brickWeights.Length - 1; i > 0; i--)
            {
                var brickWeight = _brickWeights[i] * _upgradesController.GetUpgradedValue(UpgradeType.Damage);
                brickCounts[i] = Mathf.FloorToInt(ordinaryBricks.Count * .01f * brickWeight);
            }

            var bricksForPlace = ordinaryBricks.OrderBy(_ => Random.value).ToList();

            for (var i = brickCounts.Length - 1; i > 0; i--)
            {
                if (bricksForPlace.Count <= 0)
                    break;
                for (var j = brickCounts[i]; j > 0; j--)
                {
                    if (bricksForPlace.Count <= 0)
                        break;
                    var brick = bricksForPlace.Last();
                    InitBlock(floorId, brick, _brickSettings[i]);
                    bricksForPlace.Remove(brick);
                }
            }
            bricksForPlace.ForEach(brick =>
            {
                InitBlock(floorId, brick, _brickSettings[0]);
            });

            DestroyUnusedBricks(topPriority);
            DestroyUnusedBricks(bricksX1);
            DestroyUnusedBricks(bricksX2);
            DestroyUnusedBricks(bricksX4);
            otherBricks.ForEach(brick => InitBlock(floorId, brick, new BrickSettings() { BrickType = brick.BrickType, BaseBrickHp = 1 }));

            _initializationSubject.OnNext(floorId);

            void DestroyUnusedBricks(List<BaseBrick> baseBricks)
            {
                baseBricks.ForEach(brick =>
                {
                    if (!brick.IsInitialized)
                        Object.Destroy(brick.gameObject);
                });
            }
        }

        public IReadOnlyReactiveCollection<BaseBrick> GetAliveBricks(string floorId)
        {
            return _floorsBricks.ContainsKey(floorId) ? _floorsBricks[floorId] : null;
        }

        public bool IsInitialized(string floorId)
        {
            return _floorsBricks.ContainsKey(floorId);
        }
        private void IsAliveCheck(string floorId, BaseBrick brick)
        {
            if (!brick.IsAlive.Value)
            {
                DestroyBrick(floorId, brick, true);
                if (brick.BrickType.Equals(BrickType.TNT))
                {
                    _signalBus.Fire(new GameSignals.TntDestroyed());
                    BlowUpAllBricks(floorId);
                }
            }
        }
        private void DestroyBrick(string floorId, BaseBrick brick, bool animate)
        {
            if (_isAlvieStreams.TryGetValue(brick, out var disposable))
            {
                disposable?.Dispose();
                _isAlvieStreams.Remove(brick);
                _floorsBricks[floorId].Remove(brick);
            }
            if (brick != null)
            {
                if (animate)
                {
                    _signalBus.Fire(new GameSignals.BrickDestroyed() { FloorId = floorId, Brick = brick });
                    _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "coins" });
                    brick.Destruct(() =>
                    {
                        Object.Destroy(brick.gameObject);
                    });
                }
                else
                {
                    _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "coins" });
                    Object.Destroy(brick.gameObject);
                }
            }
        }

        public void DestroyAllBricks(string floorId, bool animate)
        {
            for (int i = _floorsBricks[floorId].Count - 1; i >= 0; i--)
            {
                DestroyBrick(floorId, _floorsBricks[floorId][i], animate);
            }
        }
        public void BlowUpAllBricks(string floorId)
        {
            var blowBricks = _floorsBricks[floorId].OrderBy(_ => Random.value).ToList();
            var interval = GameConstants.BRICKS_EXPLOSION_DURATION / blowBricks.Count;
            _blowUpStream?.Dispose();
            _blowUpStream = Observable.Interval(TimeSpan.FromSeconds(interval)).Subscribe(i =>
            {
                if (i < blowBricks.Count)
                {
                    var brick = blowBricks[(int) i];
                    if (_floorsBricks[floorId].Contains(brick))
                        DestroyBrick(floorId, brick, true);
                }
                else
                {
                    _blowUpStream?.Dispose();
                }
            });
        }
        
        private void InitBlock(string floorId, BaseBrick brick, BrickSettings brickSettings)
        {
            brick.Initialize(brickSettings);
            _floorsBricks[floorId].Add(brick);
            _isAlvieStreams.Add(brick, brick.CurrentHp.Subscribe(_ => IsAliveCheck(floorId, brick)));
        }

        private void InitBlocksContainer(string floorId)
        {
            if (!_floorsBricks.ContainsKey(floorId))
            {
                _floorsBricks.Add(floorId, new ReactiveCollection<BaseBrick>());
            }
        }
    }
}
