using System;
using System.Linq;
using Common;
using Common.Data;
using Common.Settings;
using Common.Signals;
using UniRx;
using UnityEngine;
using Utils;
using Zenject;

namespace Bricks
{
    public abstract class BaseBrick : MonoBehaviour, IGameEntity
    {
        public ReadOnlyReactiveProperty<float> CurrentHp => _currentHp.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<float> MaxHp => _maxHp.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<bool> IsAlive => _currentHp.Select(hp => hp > 0).ToReadOnlyReactiveProperty();
        public EntityType EntityType => EntityType.Brick;
        public abstract BrickType BrickType { get; }
        public bool IsInitialized { get; private set; }
        public string Id { get; protected set; }
        public ReadOnlyReactiveProperty<Vector2> WorldPosition =>
            Observable.Return((Vector2) transform.position).ToReadOnlyReactiveProperty();
        public BrickSizeType BrickSizeType => brickSizeType;

        public ReadOnlyReactiveProperty<bool> IsImmuneToDamage => _isImmuneToDamage.ToReadOnlyReactiveProperty();
        
        private ReactiveProperty<bool> _isImmuneToDamage = new ReactiveProperty<bool>();
        private ReactiveProperty<float> _currentHp = new ReactiveProperty<float>();
        private ReactiveProperty<float> _maxHp = new ReactiveProperty<float>(float.Epsilon);
        
        [SerializeField] private BrickSizeType brickSizeType = BrickSizeType.X1;

        protected SignalBus SignalBus;
        
        [Inject]
        private void Inject(SignalBus signalBus)
        {
            SignalBus = signalBus;
        }

        public virtual void Initialize(BrickSettings brickSettings)
        {
            Id = brickSettings.Id;
            var hp = CalculateDurability(brickSettings.BaseBrickHp);
            SetMaxHp(hp);
            Heal(hp);
            IsInitialized = true;
        }

        public virtual void SetMaxHp(float newHp)
        {
            _maxHp.Value = newHp;
            SetCurrentHp(_currentHp.Value);
        }

        public virtual void TakeDamage(float damageHp)
        {
            if (!_isImmuneToDamage.Value)
                SetCurrentHp(_currentHp.Value - damageHp);
        }

        public virtual void Heal(float healHp)
        {
            SetCurrentHp(_currentHp.Value + healHp);
        }

        public virtual void SetCurrentHp(float newHp)
        {
            _currentHp.Value = Mathf.Clamp(newHp, 0, _maxHp.Value);
        }

        protected virtual float CalculateDurability(float startDurability) {
            return startDurability * brickSizeType.GetSizeMultiplier();
        }

        public virtual void Destruct(Action action)
        {
            Id = string.Empty;
            action.Invoke();
            SignalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "coins" });
        }

        public virtual void SetDamageImmunity(bool isImmune)
        {
            _isImmuneToDamage.Value = isImmune;
        }
    }
}
