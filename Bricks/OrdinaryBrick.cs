﻿using System;
using Common.Settings;
using Common.Signals;
using UnityEngine;

namespace Bricks
{
    public class OrdinaryBrick : BaseBrick
    {
        [SerializeField] protected OrdinaryBrickView ordinaryBrickView;
        public override BrickType BrickType => BrickType.Ordinary;

        public override void Initialize(BrickSettings brickSettings)
        {
            ordinaryBrickView.Initialize(BrickSizeType, brickSettings);
            base.Initialize(brickSettings);
        }

        public override void TakeDamage(float damageHp)
        {
            SignalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "brick" });
            ordinaryBrickView.ChangeOnDefaultSkin();
            ordinaryBrickView.Shake();
            base.TakeDamage(damageHp);
        }

        public override void SetCurrentHp(float newHp)
        {
            base.SetCurrentHp(newHp);
            ordinaryBrickView.UpdateView(CurrentHp.Value / MaxHp.Value);
        }

        public override void Destruct(Action action)
        {
            Id = string.Empty;
            ordinaryBrickView.StopShake();
            ordinaryBrickView.OnDestructAnimation(() => action.Invoke());
        }
    }
}