﻿using System;
using Entities.Enemy;
using UnityEngine;

namespace Bricks
{
    public class BalconyBrick : OrdinaryBrick
    {
        public Vector2 EnemySpawnPosition => _enemySpawnTransform.position;
        [SerializeField] private Transform _enemySpawnTransform;
        [SerializeField] private Transform _balconyShadowTransform;
        private EnemyFacade _enemy;

        public void AttachEnemyToBalcony(EnemyFacade enemy)
        {
            _enemy = enemy;
            _enemy.transform.SetParent(transform);
        }
        public override void Destruct(Action action)
        {
            if (_enemy != null)
            {
                _enemy.PunchToDeath();
                _enemy.transform.SetParent(null);
            }
            TurnIntoOrdinaryBlock();
            base.Destruct(action);
        }

        public void TurnIntoOrdinaryBlock()
        {
            _balconyShadowTransform.gameObject.SetActive(false);
        }
    }
}