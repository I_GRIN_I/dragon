﻿using Common.Settings;
using Common.Signals;
using System;
using UnityEngine;

namespace Bricks
{
    public class TNT_Brick : BaseBrick
    {
        [SerializeField] protected OrdinaryBrickView ordinaryBrickView;
        public override BrickType BrickType => BrickType.TNT;

        public override void TakeDamage(float damageHp)
        {
            SignalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "explosion" });
            base.TakeDamage(damageHp);
        }

        public override void Destruct(Action action)
        {
            ordinaryBrickView.OnDestructAnimation(() => base.Destruct(action));
        }
    }
}