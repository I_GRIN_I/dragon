﻿using Common.Settings;
using Common.Signals;

namespace Bricks
{
    public class ImmutableBrick : BaseBrick
    {
        public override BrickType BrickType => BrickType.Immutable;

        public override void TakeDamage(float damageHp)
        {
            SignalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "metal", Volume = .3f });

            base.TakeDamage(damageHp);
        }

        public override void Initialize(BrickSettings brickSettings)
        {
            base.Initialize(brickSettings);
            SetDamageImmunity(true);
        }
    }
}