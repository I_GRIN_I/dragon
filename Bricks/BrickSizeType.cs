namespace Bricks
{
    public enum BrickSizeType
    {
        X1,
        X2,
        X4Vertical,
        X4Cube
    }
}