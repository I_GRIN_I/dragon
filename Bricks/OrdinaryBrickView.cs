using System;
using System.Collections;
using System.Linq;
using Common.Settings;
using Common.Settings.Registries;
using ModestTree;
using UnityEngine;
using DG.Tweening;
using Utils;
using Random = UnityEngine.Random;

namespace Bricks
{
    public class OrdinaryBrickView : MonoBehaviour
    {
        [SerializeField, Range(0, 1f)] private float _durability;
        [Space]
        [SerializeField] private ParticleSystem _destroyParticles;
        [SerializeField] private GeneralBrickSettingsRegistry _generalBrickSettingsRegistry;
        [SerializeField] private SpriteRenderer _typeRenderer;
        [SerializeField] private SpriteMask _typeMask;
        [SerializeField] private SpriteRenderer _damageRenderer;
        [SerializeField] private SpriteRenderer _damageMultiplyRenderer;
        private bool _haveDefaultSkin = false;
        private BrickTypeResource _brickTypeResource;
        private Tweener _shakeAnimation;
        private BrickSizeType _sizeType;

        public void Initialize(BrickSizeType sizeType, BrickSettings brickSettings)
        {
            _sizeType = sizeType;
            var blockTypeSetting = _brickTypeResource =
                brickSettings.BrickTypeResources.FirstOrDefault(s => s.SizeType.Equals(sizeType));
            if (blockTypeSetting != null)
            {
                if (blockTypeSetting.BricksSpriteVariations != null &&
                    !blockTypeSetting.BricksSpriteVariations.IsEmpty())
                {
                    _typeRenderer.sprite = _typeMask.sprite = blockTypeSetting.BricksSpriteVariations.OrderBy(_ => Random.value).First();
                    if (blockTypeSetting.HaveDefaultSkin && _typeRenderer.sprite != blockTypeSetting.BricksSpriteVariations.First())
                        _haveDefaultSkin = true;
                }
                else
                {
                    _typeRenderer.sprite = _typeMask.sprite = blockTypeSetting.BrickSprite;
                }

                _damageRenderer.flipX = _damageMultiplyRenderer.flipX = Random.value > 0.5f;
                _damageRenderer.flipY = _damageMultiplyRenderer.flipY = Random.value > 0.5f;
            }
        }
        public void UpdateView(float durability)
        {
            var damageSettings = _generalBrickSettingsRegistry.Settings.Damage.OrderBy(s => s.DamageThreshold)
                .FirstOrDefault(s => durability <= s.DamageThreshold);
            var isDamaged = damageSettings != null;
            _damageRenderer.gameObject.SetActive(isDamaged);
            _damageMultiplyRenderer.gameObject.SetActive(isDamaged);
            if (isDamaged)
            {
                var resources = damageSettings.Resources.First(r => r.SizeType.Equals(_sizeType));
                _damageRenderer.sprite = resources.BrickSprite;
                _damageMultiplyRenderer.sprite = resources.BrickMultiplySprite;
            }
        }

        public void OnDestructAnimation(Action callback)
        {
            if (isActiveAndEnabled)
                StartCoroutine(PlayDestructAnimation(callback));
            else
                callback.Invoke();
        }

        private IEnumerator PlayDestructAnimation(Action callback)
        {
            var boxCollider2D = GetComponentInParent<BoxCollider2D>();
            if (boxCollider2D != null) Destroy(boxCollider2D);
            _typeRenderer.enabled = _damageRenderer.enabled = _damageMultiplyRenderer.enabled = false;
            _destroyParticles.Clear();
            _destroyParticles.Play();
            yield return new WaitWhile(() => _destroyParticles.IsAlive());
            callback.Invoke();
        }

        public void Shake()
        {
            StopShake();
            _shakeAnimation = transform.DOShakePosition(.5f, .1f, 15);
        }

        public void StopShake()
        {
            if (_shakeAnimation.IsActive())
            {
                _shakeAnimation.Kill(true);   
            }
        }

        public void ChangeOnDefaultSkin()
        {
            if(_haveDefaultSkin)
                _typeRenderer.sprite = _typeMask.sprite = _brickTypeResource.BricksSpriteVariations.First();
        }

        private void OnValidate()
        {
            UpdateView(_durability);
        }
    }
}