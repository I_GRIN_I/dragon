using System;
using System.Collections.Generic;
using System.Linq;
using Bricks;
using Common;
using Common.Data;
using Common.Settings;
using Common.Signals;
using Entities.Enemy;
using Entities.Player;
using UniRx;
using UnityEngine;
using Utils;
using Zenject;
using Object = UnityEngine.Object;

namespace Floors
{
    public class Floor : MonoBehaviour
    {
        public class Factory : PlaceholderFactory<Object, Floor>
        { }

        private const float InteriorScale = .38f;
        [SerializeField] [ReadOnly] private int _destructableBlocksCount;
        [SerializeField] [ReadOnly] private int _balconySlotCount;
        [SerializeField] private Transform _interiorsParent;
        [SerializeField] private List<Transform> _borders;
        [SerializeField] protected ParticleSystem ExplosionParticle;
        public IObservable<Floor> IsInitializedObservable => _bricksController.AreFloorBricksInitialized
            .Merge(_enemiesController.AreFloorEnemiesInitialized).Where(_ => IsInitialized)
            .Select(_ => this);

        public bool AreAllBricksDestroyed => _bricksController.GetAliveBricks(Id.ToString()).Count(b=>b.BrickType == BrickType.Ordinary) <= 0;

        public IObservable<int> BricksCountChanged => _bricksController.GetAliveBricks(Id.ToString())
            .ObserveCountChanged();
        public IObservable<bool> AreAllEnemiesKilled => _enemiesController.GetAliveEnemies(Id.ToString()).ObserveCountChanged()
            .Where(enemiesLeft => enemiesLeft <= 0)
            .Select(enemiesLeft => enemiesLeft <= 0);
        public bool IsInitialized =>
            _bricksController.IsInitialized(Id.ToString()) && _enemiesController.IsInitialized(Id.ToString());
        public string FloorId { get; private set; }
        public Guid Id { get; private set; }
        
        public FloorType FloorType { get; private set; }
        
        private BricksController _bricksController;
        private List<FloorSettings>  _floorSettings;
        private EnemiesController _enemiesController;
        private SceneContextWrapper _wrapper;
        private FloorInteriorSettings _floorInteriorSettings;
        private SignalBus _signalBus;

        [Inject]
        private void Construct(BricksController bricksController, EnemiesController enemiesController,
            List<FloorSettings> floorsSettings, SceneContextWrapper wrapper, FloorInteriorSettings floorInteriorSettings, SignalBus signalBus)
        {
            _wrapper = wrapper;
            _enemiesController = enemiesController;
            _floorSettings = floorsSettings;
            _bricksController = bricksController;
            _floorInteriorSettings = floorInteriorSettings;

            _signalBus = signalBus;
            _signalBus.Subscribe<GameSignals.TntDestroyed>(Explosion);
        }

        private void Explosion()
        {
            if(ExplosionParticle != null)
                ExplosionParticle?.Play();
            _signalBus.Unsubscribe<GameSignals.TntDestroyed>(Explosion);
        }

        private void OnValidate()
        {
            var bricks = transform.GetComponentsInChildren<BaseBrick>();
            _destructableBlocksCount =
                Mathf.RoundToInt(bricks.Where(b => b.BrickType.Equals(BrickType.Ordinary))
                    .Select(brick => brick.BrickSizeType.GetSizeMultiplier()).Sum());
            _balconySlotCount =
                Mathf.RoundToInt(bricks.Where(brick=>brick is BalconyBrick).Count());
        }
        
        public void Initialize(string floorId, Guid id)
        {
            Id = id;
            FloorId = floorId;
            name = floorId + $"_{id}(Clone)";
            var floorSetting = _floorSettings.FirstOrDefault(f => f.Id.Equals(FloorId));
            
            if (floorSetting == null)
                throw new NullReferenceException($"Floor Settings with ID {FloorId} wasn't found!");
            FloorType = floorSetting.FloorType;
            
            var bricks = transform.GetComponentsInChildren<BaseBrick>();
        
            _bricksController.Initialize(Id.ToString(), bricks);

            var balconyBricks = bricks.Where(b => b is BalconyBrick && b.isActiveAndEnabled).Select(b => b as BalconyBrick).ToList();
            _enemiesController.Initialize(Id.ToString(), floorSetting.Enemies, balconyBricks);

            if (_interiorsParent)
            {
                for (int i = 0; i < 8; i++)
                {
                    var interior = _floorInteriorSettings.Interiors.OrderBy(_ => UnityEngine.Random.value).First();
                    var interiorSpriteRenderer = new GameObject("interior").AddComponent<SpriteRenderer>();
                    interiorSpriteRenderer.sortingOrder = -30;
                    interiorSpriteRenderer.sprite = interior.Sprite;
                    interiorSpriteRenderer.transform.SetParent(_interiorsParent);
                    interiorSpriteRenderer.transform.localScale = new Vector3(InteriorScale, InteriorScale, 1);
                    interiorSpriteRenderer.transform.localPosition = new Vector3(0, i * 2);
                }
            }
        }

        public bool IsFloorInCameraView()
        {
            var _camera = _wrapper.ActiveCamera.Value;
            return _borders.Any(b =>
            {
                var viewport = _camera.WorldToViewportPoint(b.position);
                return viewport.x > 0 && viewport.x < 1 && viewport.y > 0 && viewport.y < 1;
            });
        }

        public void PauseActivityFloorEnemies()
        {
            _enemiesController.PauseEnemiesActivity(Id.ToString());
        }
        public void ResumeActivityFloorEnemies()
        {
            _enemiesController.ResumeEnemiesActivity(Id.ToString());
        }
        public void Dispose(bool animate = false)
        {
            _enemiesController.DestroyAllEnemies(Id.ToString(), true);
            _bricksController.DestroyAllBricks(Id.ToString(), animate);
        }
    }
}