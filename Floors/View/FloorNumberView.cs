using Progress;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using Zenject;

public class FloorNumberView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    private ProgressController _progressController;


    [Inject]
    private void Construct(ProgressController progressController)
    {
        
        _progressController = progressController;
    }
    void Start()
    {
        _progressController.Level.Subscribe(OnLevelChanged).AddTo(this);
    }

    private void OnLevelChanged(long level)
    {
        _text.text = $"Floor {level + 1}";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
