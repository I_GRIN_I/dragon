using UI;
using UnityEngine;

namespace Floors
{
    public class PitStopFloor : Floor
    {
        [SerializeField] private Transform _pitStopPosition;
        public Vector3 PitStopWorldPosition => _pitStopPosition.position;
    }
}