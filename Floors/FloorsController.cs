using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.GameStates;
using Common.GameStates.States;
using Common.Settings;
using UniRx;
using Utils;
using DG.Tweening;
using Entities.Player.Data;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Floors
{
    public class FloorsController
    {
        private ReactiveCollection<Floor> _activeFloors = new ReactiveCollection<Floor>();
        private readonly Floor.Factory _floorFactory;
        private readonly List<FloorSettings> _floorSettings;
        private readonly LevelSequenceSettings _sequenceSettings;
        private readonly SceneContextWrapper _sceneWrapper;
        public Floor CurrentFloor { get; private set; }

        public FloorsController(Floor.Factory floorFactory, List<FloorSettings> floorSettings,
            LevelSequenceSettings sequenceSettings, SceneContextWrapper sceneWrapper)
        {
            _floorFactory = floorFactory;
            _floorSettings = floorSettings;
            _sequenceSettings = sequenceSettings;
            _sceneWrapper = sceneWrapper;
        }

        internal void MoveFloorsAnimation(float duration)
        {
            for (int i = _activeFloors.Count - 1; i >= 0; i--)
            {
                var floor = _activeFloors[i];
                if (floor.IsFloorInCameraView() || floor.Id.Equals(CurrentFloor.Id))
                {
                    floor.transform.DOMoveY(floor.transform.position.y - GameConstants.FLOOR_HIGHT, duration);
                }
                else
                {
                    DestroyFloor(floor.Id);
                }
            }
        }

        public Floor InitializeFloor(string floorId, bool spawnFloorAbove = false)
        {
            var floorSetting = _floorSettings.FirstOrDefault(s => s.Id.Equals(floorId));
            if (floorSetting == null)
            {
                UnityEngine.Debug.LogError($"Floor with \"{floorId}\" doesn't exist");
                return null;
            }
            var floor = _floorFactory.Create(floorSetting.Prefab);
            var anchorTransform = _sceneWrapper.GetTransform(TransformType.TowerAnchor);
            floor.transform.position = GameHelper.GetFloorPositionWithLevel(spawnFloorAbove,anchorTransform.position);
            var id = Guid.NewGuid();
            floor.Initialize(floorId, id);
            _activeFloors.Add(floor);
            return CurrentFloor = floor;
        }

        public string GetFloorIdByLevel(long level)
        {
            if (!string.IsNullOrEmpty(_sequenceSettings.DebugLevel))
                return _sequenceSettings.DebugLevel;

            if (level < _sequenceSettings.TutorialSequence.Count)
            {
                return _sequenceSettings.TutorialSequence[(int)level];
            }
            var pooledLevels = level - _sequenceSettings.TutorialSequence.Count;
            var levelIndex = (int) (pooledLevels % _sequenceSettings.Sequence.Count);
            var type = _sequenceSettings.Sequence[levelIndex];
            var floorsPool = _floorSettings.Where(f => f.IncludeInPool && f.FloorType.Equals(type))
                .OrderBy(_ => Random.value).ToList();
            var floorId = floorsPool.First().Id;
            
            if (CurrentFloor != null && floorId.Equals(CurrentFloor.FloorId))
            {
                floorId = floorsPool.Last().Id;
            }
            return floorId;
        }

        public void DestroyFloor(Guid floorId)
        {
            var floor = _activeFloors.FirstOrDefault(f => f.Id.Equals(floorId));
            if (floor != null)
            {
                floor.Dispose();
                _activeFloors.Remove(floor);
                if (CurrentFloor != null && CurrentFloor.Id.Equals(floorId)) 
                    CurrentFloor = null;
                Object.Destroy(floor.gameObject);
            }
        }
    }
}
