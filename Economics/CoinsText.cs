﻿using System;
using Common.Signals;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Utils;
using Zenject;

namespace Economics
{
    public class CoinsText : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _text;
        private Sequence _tween;
        
        [Inject]
        public void Initialize(SignalBus signalBus, CurrencyController currency)
        {
            if(currency.CumulativeCoins != null)
                ChangeCoins(currency.CumulativeCoins, false);
            signalBus.Subscribe<EconomicsSignals.CoinsChanged>(signal =>
            {
                ChangeCoins(signal.Value, true);
            });
        }
        public void ChangeCoins(Currency coins, bool animate)
        {
            if(_tween.IsActive())
                _tween.Kill(true);
            if (animate)
            {
                _tween = DOTween.Sequence()
                    .Append(_text.transform.DOScale(new Vector3(1.2f, 1.1f, 1f), 0.1f).From(Vector3.one)
                        .SetEase(Ease.InOutCubic))
                    .Append(_text.transform.DOScale(Vector3.one, 0.1f)
                        .SetEase(Ease.InOutCubic))
                    .OnComplete(() => _text.text = GameHelper.CoinsFormat(coins));
            }
            else
            {
                _text.text = GameHelper.CoinsFormat(coins);
            }
        }
    }
}