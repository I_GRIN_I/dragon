using Zenject;

namespace Economics
{
    public class EconomicsInstaller : Installer
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<CurrencyController>().AsSingle().NonLazy();
            Container.BindInterfacesTo<CoinsAccuralRule>().AsSingle().NonLazy();
        }
    }
}
