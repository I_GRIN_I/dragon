using Common.Data;
using Common.Signals;
using Progress;
using System.Numerics;
using UnityEngine;
using Upgrades;
using Zenject;

namespace Economics
{
    public class CurrencyController : IInitializable, ISaveLoad
    {
        public Currency CumulativeCoins => Wallet + _levelBuffer.Value + _chainBuffer.Value;
        public Currency Wallet { get; private set; } = new Currency(0);
        private CurrencyBuffer _levelBuffer = new CurrencyBuffer();
        private CurrencyBuffer _chainBuffer = new CurrencyBuffer();
        private SignalBus _signalBus;
        private readonly UpgradesController _upgradesController;
        public float IncomeMultiplier { get; private set; } = 1f;
        public CurrencyController(SignalBus signalBus, UpgradesController upgradesController)
        {
            _signalBus = signalBus;
            _upgradesController = upgradesController;
        }
        public void Initialize()
        {
            _signalBus.Subscribe<UpgradesSignals.UpgradeUpdated>(signal =>
            {
                if (signal.UpgradeType == UpgradeType.Income)
                    IncomeMultiplier = _upgradesController.GetUpgradedValue(UpgradeType.Income);
            });
            
            _signalBus.Subscribe<EconomicsSignals.PurchaseRequest>(signal =>
            {
                var result = TrySpendCoins(signal.Price);
                signal.Callback?.Invoke(result);
            });
        }
        
        public void AddCoinsToWallet(float coins)
        {
            Wallet += Mathf.FloorToInt(coins);
            _signalBus.Fire(new EconomicsSignals.CoinsChanged() { Value = CumulativeCoins });
        }

        public void AddCoinsToLevel(float coins)
        {
            _levelBuffer.Add(Mathf.FloorToInt(coins));
            _signalBus.Fire(new EconomicsSignals.CoinsChanged() { Value = CumulativeCoins });
        }

        public void LevelBufferToChain()
        {
            _chainBuffer.Add(_levelBuffer);
            _levelBuffer.Clean();
        }

        public void ChainBufferToWallet()
        {
            Wallet += _chainBuffer.Value;
            _chainBuffer.Clean();
        }

        public void CleanLevelBuffer()
        {
            _levelBuffer.Clean();
            _signalBus.Fire(new EconomicsSignals.CoinsChanged() { Value = CumulativeCoins });
        }
        

        public bool TrySpendCoins(Currency coins)
        {
            if(Wallet - coins >= 0)
            {
                Wallet -= coins;
                _signalBus.Fire(new EconomicsSignals.CoinsChanged() { Value = CumulativeCoins });
                return true;
            }
            return false;
        }

        public void Load(ProgressData progressData)
        {
            Wallet = new Currency(progressData.Coins);
            _chainBuffer = new CurrencyBuffer(progressData.ChainCoins);
            _signalBus.Fire(new EconomicsSignals.CoinsChanged() { Value = CumulativeCoins });
        }

        public void Save(ProgressData progressData)
        {
            progressData.Coins = Wallet.ToString();
            progressData.ChainCoins = _chainBuffer.Value.ToString();
        }
    }
}
