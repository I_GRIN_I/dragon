﻿namespace Economics
{
    public class CurrencyBuffer
    {
        public Currency Value { get; private set; }

        public CurrencyBuffer() => Clean();

        public CurrencyBuffer(string count) => Value = count;

        public void Add(CurrencyBuffer buffer) => Value += buffer.Value;
        public void Add(int value) => Value += value;

        public void Clean() => Value = 0;
    }
}
