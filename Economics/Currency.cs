using System.Numerics;

public class Currency
{
    public BigInteger Value { get; private set; }

    public Currency(BigInteger value)
    {
        Value = value;
    }

    public Currency(string value)
    {
        Value = BigInteger.Parse(value);
    }
    public override string ToString()
    {
        return Value.ToString();
    }

    public static Currency operator +(Currency a, Currency b) => new Currency(a.Value + b.Value);
    public static Currency operator ++(Currency a) => new Currency(a.Value + 1);
    public static Currency operator -(Currency a, Currency b) => new Currency(a.Value - b.Value);
    public static Currency operator -(Currency a, int b) => new Currency(a.Value - b);
    public static Currency operator --(Currency a) => new Currency(a.Value - 1);
    public static Currency operator *(Currency a, Currency b) => new Currency(a.Value * b.Value);
    public static float operator *(Currency a, float b) => (long)a.Value * b;
    public static double operator *(Currency a, double b) => (long)a.Value * b;
    public static bool operator >=(Currency a, int b) => a.Value >= b;
    public static bool operator <=(Currency a, int b) => a.Value <= b;

    public static implicit operator Currency(int v) => new Currency(v);
    public static implicit operator Currency(string v) => new Currency(v);
}
