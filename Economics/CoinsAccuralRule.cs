using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Settings;
using Common.Signals;
using Effects;
using Entities.Player.Data;
using UnityEngine;
using Utils;
using Zenject;
using Random = UnityEngine.Random;

namespace Economics
{
    public class CoinsAccuralRule : IInitializable, IDisposable
    {
        private readonly SignalBus _signalBus;
        private readonly CurrencyController _currencyController;
        private readonly EffectsManager _effectsManager;
        private readonly SceneContextWrapper _sceneContextWrapper;

        public CoinsAccuralRule(SignalBus signalBus, CurrencyController currencyController,
            EffectsManager effectsManager, SceneContextWrapper sceneContextWrapper)
        {
            _signalBus = signalBus;
            _currencyController = currencyController;
            _effectsManager = effectsManager;
            _sceneContextWrapper = sceneContextWrapper;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<GameSignals.BrickDestroyed>(OnBrickDestroyed);
        }

        private void OnBrickDestroyed(GameSignals.BrickDestroyed signal)
        {
            var uiRectTransform = _sceneContextWrapper.GetTransform(RectTransformType.UICoin);
            var coinsToAdd = signal.Brick.MaxHp.Value;
            _currencyController.AddCoinsToLevel(coinsToAdd * _currencyController.IncomeMultiplier);
            if (uiRectTransform != null)
                PlayFlyingCoinEffects(signal.Brick.WorldPosition.Value, uiRectTransform,
                    Mathf.RoundToInt(Mathf.Clamp(coinsToAdd * _currencyController.IncomeMultiplier, 3, 15)));
            else
                _effectsManager.PlayEffect<JumpCoinEffectView>(signal.Brick.WorldPosition.Value);
        }

        private void PlayFlyingCoinEffects(Vector2 startPos, RectTransform uiRectTransform,
            int coinsCount)
        {
            var angleOffset = 360 / coinsCount;
            var startAngle = Random.value * 360;
            for (int i = 0, angle = 0; i < coinsCount; i++, angle+=angleOffset)
            {
                var vector = Vector2.up * (Random.value * 0.5f + 0.5f);
                var data = new JumpCoinEffectView.Data
                {
                    EndFlyPoint = uiRectTransform.position,
                    DurationDispersion = 0.5f,
                    JumpDirection = 0.75f * vector.Rotate(startAngle + angle)
                };
                
                _effectsManager.PlayEffect<JumpCoinEffectView, JumpCoinEffectView.Data>(data, startPos);   
            }
        }

        public void Dispose()
        {
            _signalBus.TryUnsubscribe<GameSignals.BrickDestroyed>(OnBrickDestroyed);
        }
    }
}