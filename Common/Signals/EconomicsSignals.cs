﻿using System;

namespace Common.Signals
{
    public static class EconomicsSignals
    {
        public class CoinsChanged
        {
            public Currency Value;
        }
        
        public class PurchaseRequest
        {
            public Currency Price;
            public Action<bool> Callback;
        }
    }
}