using Bricks;
using Entities.Enemy;
using Projectiles;
using UnityEngine;

namespace Common.Signals
{
    public static class GameSignals
    {
        public class GameLoadRequest
        { }
        public class GameSaveRequest
        { }

        public class PlayerFiredProjectile
        {
            public Vector3 StartPosition;
            public Vector3 Direction;
        }
        public class ProjectileHit
        {
            public BaseProjectile Projectile;
            public GameObject CollidedEntity;
        }
        public class BrickDestroyed
        {
            public string FloorId;
            public BaseBrick Brick;
        }
        public class EnemyKilled
        {
            public string FloorId;
            public EnemyFacade Enemy;
        }
        public class SplitProjectileRequest
        {
            public BaseProjectile RequestSource;
            public BaseProjectile ProjectileShardsPrefab;
            public Vector2 Origin;
            public Vector2 InitialTarget;
            public int NumberOfShards;
            public int OffsetAngle;
        }

        public class RetryRequest
        {
        }
        public class TntDestroyed
        {
        }
    }
}