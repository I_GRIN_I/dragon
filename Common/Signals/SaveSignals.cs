﻿using Progress;

namespace Common.Signals
{
    public static class SaveSignals
    {
        public class Save
        {
            public readonly ProgressData ProgressData;
            public Save(ProgressData progressData)
            {
                ProgressData = progressData;
            }
        }
        public class Load
        {
            public readonly ProgressData ProgressData;
            public Load(ProgressData progressData)
            {
                ProgressData = progressData;
            }
        }
    }
}