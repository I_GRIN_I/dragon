﻿namespace Common.Signals
{
    public static class AudioSignals
    {
        public class PlaySound
        {
            public string SoundName;
            public bool RandomPitch;
            public float Volume = 1f;
        }
        public class StopSound
        {
            public string SoundName;
        }
    }
}