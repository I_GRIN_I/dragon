﻿using Common.Data;

namespace Common.Signals
{
    public static class ViewSignals
    {
        public class SetActiveView
        {
            public ViewType ViewType;
            public bool IsActive;

            public SetActiveView(ViewType viewType, bool isActive)
            {
                ViewType = viewType;
                IsActive = isActive;
            }
        }

        public class LeavePitStopButtonHit
        {
        }
    }
}