﻿using Common.Data;

namespace Common.Signals
{
    public static class UpgradesSignals
    {
        public class ShowWindow {}
        public class HideWindow {}
        public class UpgradeUpdated
        {
            public UpgradeType UpgradeType;
        }
    }
}