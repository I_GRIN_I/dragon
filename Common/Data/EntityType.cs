namespace Common.Data
{
    public enum EntityType
    {
        Player,
        Enemy,
        Brick
    }
}