namespace Common.Data
{
    public enum FloorType
    {
        Regular,
        Treasure,
        Boss,
        PitStop
    }
}