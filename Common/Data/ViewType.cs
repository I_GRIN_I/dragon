﻿namespace Common.Data
{
    public enum ViewType
    {
        LostView,
        PauseView,
        PauseButton,
    }
}