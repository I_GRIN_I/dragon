﻿namespace Common.Data
{
    public enum UpgradeType
    {
        Damage,
        Speed,
        Income,
        HP,
        ChargeTime
    }
}
