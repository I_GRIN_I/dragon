using Common.Controllers;
using Common.Rules;
using Economics;
using Entities.Player;
using Entities.Player.Data;
using UnityEngine;
using Zenject;

namespace Common.Bootstrap
{
    public class SceneInstaller : MonoInstaller
    {
        [SerializeField] private CoinsText _coinsText;
        [Header("Player")]
        [SerializeField] private PlayerFacade _playerFacade;
        [Header("Scene Transforms")]
        [SerializeField] private Transform _playerAnchorTransform;
        [SerializeField] private Transform _towerAnchorTransform;
        [SerializeField] private RectTransform _aimAreaTransform;
        [SerializeField] private RectTransform _moveAreaTransform;
        [SerializeField] private RectTransform _flyAreaTransform;
        [SerializeField] private RectTransform _uiCoinTransform;
        [Header("Misc")]
        [SerializeField] private Camera _sceneCamera;
        [SerializeField] private GameLoadedTrigger _loadedTrigger;
        [SerializeField] private SceneContextWrapperBinder _wrapper;
        [SerializeField] private AudioController _audioController;
        
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ProjectilesController>().AsSingle().NonLazy();
            
            Container.Bind<CoinsText>().FromInstance(_coinsText);
            
            Container.Bind<Transform>().WithId(TransformType.PlayerAnchor).FromInstance(_playerAnchorTransform);
            Container.Bind<Transform>().WithId(TransformType.TowerAnchor).FromInstance(_towerAnchorTransform);
            Container.Bind<RectTransform>().WithId(RectTransformType.AimArea).FromInstance(_aimAreaTransform);
            Container.Bind<RectTransform>().WithId(RectTransformType.MoveArea).FromInstance(_moveAreaTransform);
            Container.Bind<RectTransform>().WithId(RectTransformType.FlyArea).FromInstance(_flyAreaTransform);
            Container.Bind<RectTransform>().WithId(RectTransformType.UICoin).FromInstance(_uiCoinTransform);

            Container.BindInstance(_sceneCamera);
            Container.BindInstance(_loadedTrigger);
            Container.BindInstance(_wrapper);
            Container.Bind<PlayerFacade>().FromInstance(_playerFacade).AsSingle();
            
            Container.BindInterfacesTo<BindPlayerAsTargetForEnemiesRule>().AsSingle().NonLazy();
        }
    }
}