using PlayerInput;
using Zenject;

namespace Bootstrap
{
    public class InputInstaller : Installer
    {
        public override void InstallBindings()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Container.BindInterfacesAndSelfTo<TouchInput>().AsSingle().NonLazy();
#else
            Container.BindInterfacesAndSelfTo<EditorInput>().AsSingle().NonLazy();
#endif
        }
    }
}