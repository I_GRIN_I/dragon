using Common.Settings;
using Common.Settings.Registries;
using UnityEngine;
using Zenject;

namespace Common.Bootstrap
{
    public class SettingsRegistriesInstaller : MonoInstaller
    {
        [SerializeField] private GeneralSettingsRegistry _generalSettingsRegistry; 
        [SerializeField] private PlayerSettingsRegistry _playerSettingsRegistry;
        [SerializeField] private EnemiesSettingsRegistry _enemiesSettingsRegistry;
        [SerializeField] private BricksSettingsRegistry _bricksSettingsRegistry;
        [SerializeField] private UpgradeSettingsRegistry _upgradeSettingsRegistry;
        [SerializeField] private FloorsSettingsRegistry _floorsSettingsRegistry;
        [SerializeField] private FloorInteriorSettingsRegistry _floorInteriorSettingsRegistry;
        [SerializeField] private LevelSequenceSettingsRegistry _levelSequenceSettingsRegistry;
        [SerializeField] private EffectSettingsRegistry _effectSettingsRegistry;
        public override void InstallBindings()
        {
            Container.BindInstance(_playerSettingsRegistry.Settings).AsSingle();
            Container.BindInstance(_generalSettingsRegistry.Settings).AsSingle();
            Container.BindInstance(_enemiesSettingsRegistry.Settings).AsSingle();
            Container.BindInstance(_bricksSettingsRegistry.Settings).AsSingle();
            Container.BindInstance(_upgradeSettingsRegistry.Settings).AsSingle();
            Container.BindInstance(_floorsSettingsRegistry.Settings).AsSingle();
            Container.BindInstance(_floorInteriorSettingsRegistry.Settings).AsSingle();
            Container.BindInstance(_levelSequenceSettingsRegistry.Settings).AsSingle();
            Container.BindInstance(_effectSettingsRegistry.Settings).AsSingle();
        }
    }
}