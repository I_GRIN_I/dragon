using System.Collections.Generic;
using System.Linq;
using Bootstrap;
using Bricks;
using Common.GameStates;
using Common.Rules;
using Common.Settings;
using Common.Settings.Registries;
using Economics;
using Effects;
using Entities.Enemy;
using Entities.Enemy.Bootstrap;
using Floors;
using Progress;
using Projectiles;
using UI.Health;
using UnityEngine;
using Upgrades;
using Zenject;

namespace Common.Bootstrap
{
    public class MainInstaller : MonoInstaller
    {
        [SerializeField] private EnemiesSettingsRegistry _enemiesSettingsRegistry;

        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            Container.Install<InputInstaller>();
            Container.Install<SignalsInstaller>();
            Container.Install<EconomicsInstaller>();

            Container.BindInterfacesAndSelfTo<UpgradesController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<BricksController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<EnemiesController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<ProgressController>().AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<PlayerFacadeContextWrapper>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SceneContextWrapper>().AsSingle().NonLazy();

            Container.Bind<EnemiesManager>().AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<SplitProjectileRule>().AsSingle().NonLazy();
            
            Container.Bind<ProjectileManager>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<EffectsManager>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<FloorsController>().AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<GameStateController>().AsSingle().NonLazy();
            
            InstallFactories();
        }

        public void InstallFactories()
        {
            Container.BindFactory<Object, Floor, Floor.Factory>().FromFactory<PrefabFactory<Floor>>();
            Container.BindFactory<Object, BaseProjectile, BaseProjectile.Factory>()
                .FromFactory<PrefabFactory<BaseProjectile>>();
            Container.BindFactory<Object, HeartView, HeartView.Factory>()
                .FromFactory<PrefabFactory<HeartView>>();
            BindEnemies();
        }

        public void BindEnemies()
        {
            Dictionary<string, EnemiesFactory> factories = _enemiesSettingsRegistry.Settings.ToDictionary(e => e.Id, enemySettings =>
            {
                var enemyFactory = new EnemiesFactory(Container, enemySettings);
                Container.Bind<EnemiesFactory>().FromInstance(enemyFactory).AsTransient();
                Container.Bind<EnemyFacade>()
                    .WithId(enemySettings.Id)
                    .FromSubContainerResolve()
                    .ByNewContextPrefab(enemySettings.EnemyView)
                    .AsTransient();


                return enemyFactory;
            });
            Container.BindInstance(factories).AsSingle();
        }
    }
}