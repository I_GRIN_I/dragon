using Common.Signals;
using Zenject;

namespace Common.Bootstrap
{
    public class SignalsInstaller : Installer
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<GameSignals.GameLoadRequest>().OptionalSubscriber();
            Container.DeclareSignal<GameSignals.GameSaveRequest>().OptionalSubscriber();
            Container.DeclareSignal<GameSignals.PlayerFiredProjectile>().OptionalSubscriber();
            Container.DeclareSignal<GameSignals.ProjectileHit>();
            Container.DeclareSignal<GameSignals.SplitProjectileRequest>();
            Container.DeclareSignal<GameSignals.BrickDestroyed>();
            Container.DeclareSignal<GameSignals.EnemyKilled>().OptionalSubscriber();
            
            Container.DeclareSignal<GameSignals.RetryRequest>();

            Container.DeclareSignal<GameSignals.TntDestroyed>();
            
            Container.DeclareSignal<PauseSignals.PauseFloorRequest>();
            Container.DeclareSignal<PauseSignals.ResumeFloorRequest>();

            Container.DeclareSignal<ViewSignals.SetActiveView>();
            Container.DeclareSignal<ViewSignals.LeavePitStopButtonHit>().OptionalSubscriber();

            Container.DeclareSignal<SaveSignals.Save>();
            Container.DeclareSignal<SaveSignals.Load>();

            Container.DeclareSignal<UpgradesSignals.UpgradeUpdated>();
            Container.DeclareSignal<UpgradesSignals.ShowWindow>();
            Container.DeclareSignal<UpgradesSignals.HideWindow>();

            Container.DeclareSignal<EconomicsSignals.CoinsChanged>();
            Container.DeclareSignal<EconomicsSignals.PurchaseRequest>();

            Container.DeclareSignal<AudioSignals.PlaySound>();
            Container.DeclareSignal<AudioSignals.StopSound>();
        }
    }
}