using UnityEngine;

namespace Common
{
    public abstract class BaseSettingsRegistry<TSettings> : ScriptableObject  where TSettings : class
    {
        [SerializeField] private TSettings _settings;
        public TSettings Settings => _settings;
    }
}