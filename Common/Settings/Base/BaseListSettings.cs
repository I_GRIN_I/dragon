using UnityEngine;

namespace Common.Settings.Base
{
    public abstract class BaseListSettings
    {
        public virtual string Id => _id;
        [SerializeField] private string _id;
    }
}