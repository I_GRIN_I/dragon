using System.Collections.Generic;
using Common.Settings.Base;
using UnityEngine;

namespace Common
{
    public abstract class BaseSettingsListRegistry<TSettings> : ScriptableObject  where TSettings : BaseListSettings
    {
        [SerializeField] private List<TSettings> _settings;
        public List<TSettings> Settings => _settings;
    }
}