using System;
using Common.Data;
using Common.Settings.Base;
using Effects;

namespace Common.Settings
{
    [Serializable]
    public class EffectSettings : BaseListSettings
    {
        public BaseEffectView EffectPrefab;
        public float EffectDuration;
        public int InitialSize;
        public int MaxCapacity;
    }
}