using System;
using System.Collections.Generic;
using Common.Data;
using Common.Settings.Base;
using UnityEngine.Serialization;

namespace Common.Settings
{
    [Serializable]
    public class LevelSequenceSettings
    {
        [FormerlySerializedAs("LevelSetup")]
        public List<string> TutorialSequence;
        public List<FloorType> Sequence;
        public string DebugLevel;
    }

}