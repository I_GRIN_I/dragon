using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "LevelSequenceSettings", menuName = "Settings/Level Sequence Settings")]
    public class LevelSequenceSettingsRegistry : BaseSettingsRegistry<LevelSequenceSettings>
    {
    }
}