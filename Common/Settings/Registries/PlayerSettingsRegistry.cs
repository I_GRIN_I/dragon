using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "PlayerSettingsRegistry", menuName = "Settings/Player Settings")]
    public class PlayerSettingsRegistry : BaseSettingsRegistry<PlayerSettings>
    {
    }
}