using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "BricksSettingsRegistry", menuName = "Settings/Bricks Settings")]
    public class BricksSettingsRegistry : BaseSettingsListRegistry<BrickSettings>
    {
    }
}