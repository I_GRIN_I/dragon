using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "FloorsSettingsRegistry", menuName = "Settings/Floors Settings")]
    public class FloorsSettingsRegistry : BaseSettingsListRegistry<FloorSettings>
    {
    }
}