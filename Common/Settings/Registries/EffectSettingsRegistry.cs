using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "EffectSettingsRegistry", menuName = "Settings/Effect Settings")]
    public class EffectSettingsRegistry : BaseSettingsListRegistry<EffectSettings>
    {
    }
}