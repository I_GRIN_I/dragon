using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "UpgradeSettingsRegistry", menuName = "Settings/Upgrade Settings")]
    public class UpgradeSettingsRegistry : BaseSettingsListRegistry<UpgradeSettings>
    {
    }
}