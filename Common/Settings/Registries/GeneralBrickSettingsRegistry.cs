using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "GeneralBrickSettings", menuName = "Settings/General Brick Settings")]
    public class GeneralBrickSettingsRegistry : BaseSettingsRegistry<GeneralBrickSettings>
    {
    }
}