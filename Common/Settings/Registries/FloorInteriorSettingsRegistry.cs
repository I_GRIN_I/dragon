﻿using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "FloorInteriorSettingsRegistry", menuName = "Settings/Floors Interior Settings")]
    public class FloorInteriorSettingsRegistry : BaseSettingsRegistry<FloorInteriorSettings>
    {
    }
}