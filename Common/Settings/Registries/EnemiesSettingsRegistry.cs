using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "EnemiesSettingsRegistry", menuName = "Settings/Enemies Settings")]
    public class EnemiesSettingsRegistry : BaseSettingsListRegistry<EnemySettings>
    {
    }
}