using UnityEngine;

namespace Common.Settings.Registries
{
    [CreateAssetMenu(fileName = "GeneralSettingsRegistry", menuName = "Settings/General Settings")]
    public class GeneralSettingsRegistry : BaseSettingsRegistry<GeneralSettings>
    {
    }
}