using System;
using System.Collections.Generic;
using Common.Data;
using Common.Settings.Base;
using Entities.Enemy;

namespace Common.Settings
{
    [Serializable]
    public class EnemySettings : BaseListSettings
    {
        public EnemyFacade EnemyView;
        public EnemyType EnemyType;
        public string Name;
        public float HP;
        public float AnimationPathBounds;
        public bool IsImmuneToFireballs;
        public List<SpecialSettings> Specials;
    }
}