using System;
using System.Collections.Generic;
using Bricks;
using Common.Settings.Base;
using UnityEngine;

namespace Common.Settings
{
    [Serializable]
    public class BrickSettings : BaseListSettings
    {
        public float BaseBrickHp;
        public BrickType BrickType;
        public List<BrickTypeResource> BrickTypeResources;
    }

    public enum BrickType
    {
        Ordinary,
        TNT,
        Phylactery,
        Immutable
    }

    [Serializable]
    public class BrickTypeResource
    {
        public BrickSizeType SizeType;
        public bool HaveDefaultSkin;
        public List<Sprite> BricksSpriteVariations;
        public Sprite BrickSprite;
    }
}