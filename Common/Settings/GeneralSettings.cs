using System;
using UnityEngine;

namespace Common.Settings
{
    [Serializable]
    public class GeneralSettings
    {
        public float AimLineHoldTime;
        public int AimLineReflections;

        [Range(0,1f)]public float EnemyStartChargeProjectileDispersion;
    }
}