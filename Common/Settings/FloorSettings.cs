using System;
using System.Collections.Generic;
using Common.Data;
using Common.Settings.Base;
using Floors;
using UnityEngine;

namespace Common.Settings
{
    [Serializable]
    public class FloorSettings : BaseListSettings
    {
        public FloorType FloorType;
        public Floor Prefab;
        public bool IncludeInPool = true;
        public List<EnemySetupSettings> Enemies;
    }

    [Serializable]
    public class BrickSetupSettings
    {
        public string Id;
        public int Count;
    }
    [Serializable]
    public class EnemySetupSettings
    {
        public string Id;
        public int Count;
    }
    [Serializable]
    public class Interior
    {
        public Sprite Sprite;
        public Vector2Int Sizes;
    }

    [Serializable]
    public class FloorInteriorSettings
    {
        public List<Interior> Interiors;
    }

}