using System;
using Common.Data;
using Common.Settings.Base;

namespace Common.Settings
{
    [Serializable]
    public class UpgradeSettings : BaseListSettings
    {
        public UpgradeType UpgradeType;
        public float UpgradeCost;
        public float ValuePerLevel;
    }
}