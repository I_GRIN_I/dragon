using System;
using System.Collections.Generic;
using Entities.View;
using Projectiles;
using UnityEngine;

namespace Common.Settings
{
    [Serializable]
    public class PlayerSettings
    {
        [Header("Configs")]
        public float BaseVelocity;
        public float BaseHealth;
        public float HealthUpgradeMultiplier;
        public SpecialSettings SpecialSettings;
        [Space]
        [Header("Resources")]
        public Transform BaseBlock;
        public GameObject HealthPrefab;
    }
    
}