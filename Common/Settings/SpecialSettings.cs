using System;
using Projectiles;
using UnityEngine;

namespace Common.Settings
{
    [Serializable]
    public class SpecialSettings
    {
        public BaseProjectile Projectile;
        [Tooltip("Seconds need to be able to fire a projectile")]
        public float FireChargeTime = 1f;
        [Tooltip("Damage dealt by projectile")]
        public float Damage = 1;
        [Tooltip("Speed of projectile")]
        public float Speed = 2f;
        [Tooltip("Can this projectile stun the target?")]
        public bool CanStun;
        [Tooltip("Stun duration")]
        public float StunDuration;

        public SpecialSettings() { }
        public SpecialSettings(SpecialSettings specialSettings)
        {
            Projectile = specialSettings.Projectile;
            FireChargeTime = specialSettings.FireChargeTime;
            Damage = specialSettings.Damage;
            Speed = specialSettings.Speed;
            CanStun = specialSettings.CanStun;
            StunDuration = specialSettings.StunDuration;
        }
    }

}