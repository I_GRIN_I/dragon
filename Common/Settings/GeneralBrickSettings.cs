using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Settings
{
    [Serializable]
    public class GeneralBrickSettings
    {
        public List<BrickDamageResource> Damage;
        [Space]
        public Transform BaseBlock;
    }
    
    [Serializable]
    public class BrickDamageResource
    {
        [Range(0,1f)] public float DamageThreshold;
        public List<BrickDestroyResource> Resources;
    }
    
    [Serializable]
    public class BrickDestroyResource : BrickTypeResource
    {
        public Sprite BrickMultiplySprite;
    }
}