namespace Common.GameStates
{
    public interface IStateContext
    { }
    public interface IStateContext<TContext,TState> : IStateContext
        where TState : IGameState<TContext>
        where TContext : IStateContext
    {
        void TransitionTo(TState state);
    }
}