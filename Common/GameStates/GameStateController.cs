using System;
using Common.GameStates.States;
using Economics;
using Floors;
using Progress;
using Projectiles;
using UniRx;
using Upgrades;
using Zenject;

namespace Common.GameStates
{
    public class GameStateController : IStateContext<GameStateController,BaseGameState>, IInitializable
    {
        public SignalBus SignalBus { get; }
        public PlayerFacadeContextWrapper PlayerWrapper { get; }
        public FloorsController FloorsController { get; }
        public ProgressController ProgressController { get; }
        public ProjectileManager ProjectileManager { get; }
        public SceneContextWrapper SceneWrapper { get; }
        public CurrencyController CurrencyController { get; }

        public GameStateController(SignalBus signalBus, PlayerFacadeContextWrapper playerWrapper,
            FloorsController floorsController, ProgressController progressController,
            ProjectileManager projectileManager, SceneContextWrapper sceneWrapper,
            CurrencyController currencyController)
        {
            SignalBus = signalBus;
            PlayerWrapper = playerWrapper;
            FloorsController = floorsController;
            ProgressController = progressController;
            ProjectileManager = projectileManager;
            SceneWrapper = sceneWrapper;
            CurrencyController = currencyController;
        }
        public IObservable<Unit> StateChanged =>_state.AsUnitObservable();

        public BaseGameState CurrentState => _state.Value;

        private ReactiveProperty<BaseGameState> _state =
            new ReactiveProperty<BaseGameState>( new EmptyState());

        public void Initialize()
        {
            TransitionTo(new PreloadState());
        }

        public void TransitionTo(BaseGameState state)
        {
            CurrentState.Exit();
            _state.Value = state;
            CurrentState.SetContext(this);
            CurrentState.Enter();
        }
    }
}