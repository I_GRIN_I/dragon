using Common.Signals;
using System;
using Floors;
using UniRx;

namespace Common.GameStates.States
{
    public class PitStopStageState : BaseGameState
    {
        private static readonly float EnterAnimationDuration = 2.5f;
        private static readonly float ExitAnimationDuration = 1.2f;
        public override void Enter()
        {
            base.Enter();
            EnablePauseForState();
            Context.CurrencyController.ChainBufferToWallet();
            
            Context.FloorsController.CurrentFloor.PauseActivityFloorEnemies();
            if (Context.FloorsController.CurrentFloor is PitStopFloor floor)
            {
                var player = Context.PlayerWrapper.PlayerFacade;
                player.MovePlayerToWorldPosition(floor.PitStopWorldPosition, EnterAnimationDuration);
                Context.SignalBus.Fire<UpgradesSignals.ShowWindow>();
                Observable.Timer(TimeSpan.FromSeconds(1f))
                    .Subscribe(_ =>
                    {
                        Context.SignalBus.Fire(new AudioSignals.PlaySound() { SoundName = "pitStop" });
                        player.Heal(player.MaxHp.Value);
                    });
                Context.SignalBus.Subscribe<ViewSignals.LeavePitStopButtonHit>(OnFinishedFloor);
            }
            else
                OnFinishedFloor();
        }

        private void OnFinishedFloor()
        {
            Context.SignalBus.TryUnsubscribe<ViewSignals.LeavePitStopButtonHit>(OnFinishedFloor);
            Context.SignalBus.Fire<UpgradesSignals.HideWindow>();
            Observable.Timer(TimeSpan.FromSeconds(ExitAnimationDuration)).Subscribe(_ =>
            {
                Context.TransitionTo(new WonFloorStageState());
            }).AddTo(CompositeDisposable);
        }

        public override void Exit()
        { 
            base.Exit();
            Context.SignalBus.TryUnsubscribe<ViewSignals.LeavePitStopButtonHit>(OnFinishedFloor);

            Context.PlayerWrapper.PlayerFacade.Pause();
        }
    }
}