using System;
using Common.Signals;
using Entities.Player;
using UniRx;

namespace Common.GameStates.States
{
    public class FloorStageState : BaseGameState
    {
        private float _lastHp;

        public override void Enter()
        {
            base.Enter();
            EnablePauseForState();
            var floor = Context.FloorsController.CurrentFloor;
            
            Context.ProgressController.SetFloorId(floor.FloorId);
            Context.SignalBus.Fire<GameSignals.GameSaveRequest>();
            
            floor.ResumeActivityFloorEnemies();
            var player = Context.PlayerWrapper.PlayerFacade;
            
            player.Resume();
            if (!player.IsAlive.Value)
            {
                OnPlayerDied();
                return;
            }
            
            if (floor.AreAllBricksDestroyed)
            {
                OnDestroyedFloor();
                return;
            }
            
            player.IsAlive.Where(isAlive => !isAlive).Throttle(TimeSpan.FromMilliseconds(100))
                .Subscribe(_ => OnPlayerDied())
                .AddTo(CompositeDisposable);
            
            floor.BricksCountChanged.Where(areBricksDestroyed => floor.AreAllBricksDestroyed)
                .Throttle(TimeSpan.FromMilliseconds(100))
                .Subscribe(_ => OnDestroyedFloor()).AddTo(CompositeDisposable);
            
            _lastHp = player.CurrentHp.Value;
            player.CurrentHp.Subscribe(hp => UpdatedCurrentHp(player, hp)).AddTo(CompositeDisposable);
        }

        private void UpdatedCurrentHp(PlayerFacade player, float hp)
        {
            if (_lastHp > hp)
            {
                player.SetDamageImmunity(true);
                Observable.Timer(TimeSpan.FromSeconds(3f))
                    .Subscribe(_ => player.SetDamageImmunity(false));
            }
            _lastHp = hp;
        }

        private void OnDestroyedFloor()
        {
            Context.TransitionTo(new WonFloorStageState());
        }

        private void OnPlayerDied()
        {
            Context.TransitionTo(new LostFloorStageState());
        }
        
        public override void Exit()
        { 
            base.Exit();
            Context.FloorsController.CurrentFloor.PauseActivityFloorEnemies();
            Context.PlayerWrapper.PlayerFacade.Pause();
            
        }
    }
}