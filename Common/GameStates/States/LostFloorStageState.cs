﻿using Common.Data;
using Common.Signals;
using System;
using UniRx;

namespace Common.GameStates.States
{
    public class LostFloorStageState : BaseGameState
    {
        public override void Enter()
        {
            base.Enter();
            Context.CurrencyController.CleanLevelBuffer();
            Context.SignalBus.Fire(new ViewSignals.SetActiveView(ViewType.LostView, true));
            Context.SignalBus.Subscribe<GameSignals.RetryRequest>(RetryFloor);
        }

        private void RetryFloor()
        {
            var playerFacade = Context.PlayerWrapper.PlayerFacade;
            playerFacade.Heal(playerFacade.MaxHp.Value);
            
            var id = Context.FloorsController.CurrentFloor.Id;
            var floorId = Context.FloorsController.CurrentFloor.FloorId;
            Context.FloorsController.DestroyFloor(id);
            Context.FloorsController.InitializeFloor(floorId);
            playerFacade.MovePlayerToStart();
            Context.TransitionTo(new FloorStageState());
        }

        public override void Exit()
        { 
            base.Exit();
            Context.SignalBus.Fire(new ViewSignals.SetActiveView(ViewType.LostView, false));
            Context.SignalBus.TryUnsubscribe<GameSignals.RetryRequest>(RetryFloor);
            Context.ProjectileManager.DestroyAllProjectiles();
        }
    }
}