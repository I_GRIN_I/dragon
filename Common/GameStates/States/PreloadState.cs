using System;
using DG.Tweening;
using Entities.Player.Data;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common.GameStates.States
{
    public class PreloadState : BaseGameState
    {
        public override void Enter()
        {
            base.Enter();
            Application.targetFrameRate = 60;
            MainThreadDispatcher.Initialize();
            DOTween.SetTweensCapacity(2560, 2560);
            DOTween.Init(useSafeMode: true, logBehaviour: LogBehaviour.ErrorsOnly);

            var activeScene = SceneManager.GetActiveScene();
            if (Context.SceneWrapper.IsBoundTransform(TransformType.TowerAnchor))
            {
                OnSceneLoaded(activeScene, LoadSceneMode.Single);
            }
            else
            {
                Context.SceneWrapper.TransformBound.Throttle(TimeSpan.FromMilliseconds(100))
                    .Where(type => type.Equals(TransformType.TowerAnchor))
                    .Subscribe(_ => OnSceneLoaded(activeScene, LoadSceneMode.Single)).AddTo(CompositeDisposable);
            }
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Context.TransitionTo(new SetupSceneState());
        }
        
        
        public override void Exit()
        {
            base.Exit();
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
}