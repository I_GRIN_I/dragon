﻿using Common.Data;
using Common.Signals;

namespace Common.GameStates.States
{
    public class PauseFloorStageState : BaseGameState
    {
        private readonly BaseGameState _stateFrom;

        public PauseFloorStageState(BaseGameState stateFrom)
        {
            _stateFrom = stateFrom;
        }

        public override void Enter()
        {
            base.Enter();
            Context.SignalBus.Fire(new ViewSignals.SetActiveView(ViewType.PauseButton, false));
            Context.SignalBus.Fire(new ViewSignals.SetActiveView(ViewType.PauseView, true));
            Context.SignalBus.Subscribe<PauseSignals.ResumeFloorRequest>(OnResumeFloor);
            Context.ProjectileManager.PauseProjectiles();
        }

        private void OnResumeFloor()
        {
            Context.TransitionTo(_stateFrom);
        }

        public override void Exit()
        { 
            base.Exit();
            Context.SignalBus.Fire(new ViewSignals.SetActiveView(ViewType.PauseButton, true));
            Context.SignalBus.Fire(new ViewSignals.SetActiveView(ViewType.PauseView, false));
            Context.SignalBus.TryUnsubscribe<PauseSignals.ResumeFloorRequest>(OnResumeFloor);
            Context.ProjectileManager.ResumeProjectiles();
        }
    }
}