﻿using Common.Signals;
using System;
using UniRx;
using Utils;

namespace Common.GameStates.States
{
    public class WonFloorStageState : BaseGameState
    {
        private static readonly float AnimationDuration = 2.5f;
        public override void Enter()
        {
            base.Enter();
            Context.CurrencyController.LevelBufferToChain();
            var playerFacade = Context.PlayerWrapper.PlayerFacade;
            Context.ProjectileManager.DestroyAllProjectiles();
            Context.ProgressController.IncreaseLevel();

            Context.SignalBus.Fire<GameSignals.GameSaveRequest>();

            var newLvl = Context.ProgressController.Level.Value;
            var floor = Context.FloorsController.InitializeFloor(Context.FloorsController.GetFloorIdByLevel(newLvl), true);
            Context.FloorsController.MoveFloorsAnimation(AnimationDuration);
            playerFacade.MovePlayerToStart(AnimationDuration * 1.5f);
            Observable.Timer(TimeSpan.FromSeconds(AnimationDuration)).Subscribe(_ =>
            {
                Context.TransitionTo(floor.FloorType.StateFromFloorType());
            }).AddTo(CompositeDisposable);
        }

        public override void Exit()
        {
            base.Exit();
            var playerFacade = Context.PlayerWrapper.PlayerFacade;
            playerFacade.SetDamageImmunity(false);
        }
    }
}