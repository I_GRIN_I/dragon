using System;
using Common.Data;
using Common.Signals;
using Floors;
using UniRx;
using Utils;

namespace Common.GameStates.States
{
    public class SetupSceneState : BaseGameState
    {
        public override void Enter()
        {
            base.Enter();
            Context.SignalBus.Fire<GameSignals.GameLoadRequest>();

            var level = Context.ProgressController.Level.Value;
            string floorId = Context.ProgressController.CurrentFloorId;
            if (string.IsNullOrEmpty(floorId))
            {
                floorId = Context.FloorsController.GetFloorIdByLevel(level);
            }
            
            var floor = Context.FloorsController.InitializeFloor(floorId);

            Context.PlayerWrapper.PlayerFacadeInitialized.Throttle(TimeSpan.FromMilliseconds(100)).Subscribe(player =>
            {
                player.Initialize();
                player.Pause();
                CheckTransitionConditions(floor);
            }).AddTo(CompositeDisposable);
        }

        private void CheckTransitionConditions(Floor floor)
        {
            if (IsPLayerInitialized())
                Context.TransitionTo(floor.FloorType.StateFromFloorType());
        }

        private bool IsPLayerInitialized()
        {
            return Context.PlayerWrapper.IsInitialized && Context.PlayerWrapper.PlayerFacade.IsInitialized;
        }
    }
}