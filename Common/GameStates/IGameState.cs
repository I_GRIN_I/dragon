using Zenject;

namespace Common.GameStates
{
    public interface IGameState<TContext> where TContext : IStateContext
    {
        void SetContext(TContext context);
        void Enter();
        void Exit();
    }
}