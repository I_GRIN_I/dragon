using Common.GameStates.States;
using Common.Signals;
using UniRx;
using Utils;

namespace Common.GameStates
{
    public abstract class BaseGameState : IGameState<GameStateController>
    {
        protected GameStateController Context;
        protected CompositeDisposable CompositeDisposable = new CompositeDisposable();
        
        public void SetContext(GameStateController context)
        {
            Context = context;
        }
        public virtual void Enter()
        {
            UnityEngine.Debug.Log(DebugConstants.GameState + $"Enter into {GetType().Name}.");
        }
        protected void EnablePauseForState()
        {
            Context?.SignalBus.Subscribe<PauseSignals.PauseFloorRequest>(OnPauseFloor);
        }
        private void OnPauseFloor()
        {
            Context.TransitionTo(new PauseFloorStageState(this));
        }
        public virtual void Exit()
        {
            UnityEngine.Debug.Log(DebugConstants.GameState + $"Exit from {GetType().Name}.");
            CompositeDisposable.Clear();
            Context?.SignalBus.TryUnsubscribe<PauseSignals.PauseFloorRequest>(OnPauseFloor);
        }
    }
}