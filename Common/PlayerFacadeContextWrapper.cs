
using System;
using Entities.Player;
using Entities.Player.Data;
using UniRx;
using UnityEngine;
using Zenject;

namespace Common
{
    public class PlayerFacadeContextWrapper
    {
        public bool IsInitialized => _playerFacade.Value != null;
        public PlayerFacade PlayerFacade => _playerFacade.Value;
        public IObservable<PlayerFacade> PlayerFacadeInitialized => _playerFacade.Where(v=>v!= null);

        private ReactiveProperty<PlayerFacade> _playerFacade = new ReactiveProperty<PlayerFacade>();

        public void Initialize(PlayerFacade playerFacade)
        {
            if (IsInitialized)
                Dispose();
            _playerFacade.Value = playerFacade;
        }

        public void Dispose()
        {
            _playerFacade.Value = null;
        }
    }
    
    public class SceneContextWrapper
    {
        private ReactiveDictionary<TransformType, Transform> _transforms =
            new ReactiveDictionary<TransformType, Transform>();
        
        private ReactiveDictionary<RectTransformType, RectTransform> _rectTransforms =
            new ReactiveDictionary<RectTransformType, RectTransform>();
        
        private ReactiveProperty<Camera> _camera = new ReactiveProperty<Camera>();
        public IObservable<TransformType> TransformBound => _transforms.ObserveAdd().Select(pair => pair.Key);
        public IObservable<RectTransformType> RectTransformBound => _rectTransforms.ObserveAdd().Select(pair => pair.Key);
        public ReadOnlyReactiveProperty<Camera> ActiveCamera => _camera.ToReadOnlyReactiveProperty();
        public bool IsBoundTransform(TransformType transformType) => _transforms.ContainsKey(transformType);

        public void BindTransform(TransformType transformType, Transform transform)
        {
            if (IsBoundTransform(transformType))
                DisposeTransform(transformType);
            _transforms.Add(transformType, transform);
        }

        public Transform GetTransform(TransformType transformType)
        {
            if (_transforms.TryGetValue(transformType, out var value))
            {
                return value;
            }
            return null;
        }
        public void DisposeTransform(TransformType transformType)
        {
            _transforms.Remove(transformType);
        }
        public bool IsBoundRectTransform(RectTransformType transformType) => _rectTransforms.ContainsKey(transformType);

        public void BindRectTransform(RectTransformType transformType, RectTransform transform)
        {
            if (IsBoundRectTransform(transformType))
                DisposeRectTransform(transformType);
            _rectTransforms.Add(transformType, transform);
        }
        public void BindCamera(Camera camera)
        {
            _camera.Value = camera;
        }

        public RectTransform GetTransform(RectTransformType transformType)
        {
            if (_rectTransforms.TryGetValue(transformType, out var value))
            {
                return value;
            }
            return null;
        }
        public void DisposeRectTransform(RectTransformType transformType)
        {
            _rectTransforms.Remove(transformType);
        }
    }
}