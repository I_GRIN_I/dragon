using Entities.Enemy;
using Entities.Player;
using Zenject;

namespace Common.Rules
{
    public class BindPlayerAsTargetForEnemiesRule : IInitializable
    {
        private readonly PlayerFacade _playerFacade;
        private readonly EnemiesController _enemiesController;

        public BindPlayerAsTargetForEnemiesRule(PlayerFacade playerFacade, EnemiesController enemiesController)
        {
            _playerFacade = playerFacade;
            _enemiesController = enemiesController;
        }
        public void Initialize()
        {
            _enemiesController.SetAimTarget(_playerFacade.WorldPosition);
        }
    }
}
