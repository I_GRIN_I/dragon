using Common.Data;
using Common.Signals;
using Projectiles;
using UnityEngine;
using Utils;
using Zenject;

namespace Common.Rules
{
    public class SplitProjectileRule : IInitializable
    {
        private readonly ProjectileManager _projectileManager;
        private readonly SignalBus _signalBus;

        public SplitProjectileRule(ProjectileManager projectileManager, SignalBus signalBus)
        {
            _projectileManager = projectileManager;
            _signalBus = signalBus;
        }
        public void Initialize()
        {
            _signalBus.Subscribe<GameSignals.SplitProjectileRequest>(request =>
            {
                var source = _projectileManager.GetProjectileSource(request.RequestSource);
                _projectileManager.DestroyProjectile(request.RequestSource);
                int sumAnge = request.OffsetAngle * request.NumberOfShards;
                var halfSumAnge = sumAnge / 2;
                var displacement = request.InitialTarget - request.Origin;
                var offset = sumAnge / (request.NumberOfShards - 1);
                for (int angleOffset = -halfSumAnge; angleOffset <= halfSumAnge; angleOffset+= offset)
                {
                    var newTargetDistance = displacement.Rotate(angleOffset);
                    _projectileManager.InvokeProjectile(request.ProjectileShardsPrefab, source,request.Origin, newTargetDistance);
                }
            });
        }
    }
}