﻿using System.Linq;
using Common.Data;
using Common.Signals;
using Entities.Enemy;
using Entities.Player;
using Projectiles;
using UnityEngine;
using Zenject;

namespace Common.Controllers
{
    public class ProjectilesController : IInitializable, IFixedTickable
    {
        private readonly float _screenRelativeBorder = 0.05f;
        private readonly SignalBus _signalBus;
        private readonly ProjectileManager _projectileManager;
        private readonly Camera _camera;

        public ProjectilesController(SignalBus signalBus, ProjectileManager projectileManager, Camera camera)
        {
            _signalBus = signalBus;
            _projectileManager = projectileManager;
            _camera = camera;
        }
        public void Initialize()
        {
            _signalBus.Subscribe<GameSignals.ProjectileHit>(ResolveCollision);
        }

        private void ResolveCollision(GameSignals.ProjectileHit signal)
        {
            var gameEntity = signal.CollidedEntity.GetComponentInParent<IGameEntity>();
            
            ResolveCollidedEntity(signal.Projectile, gameEntity);
            ResolveProjectile(signal.Projectile, gameEntity);
        }

        private void ResolveCollidedEntity(BaseProjectile projectile, IGameEntity gameEntity)
        {
            if (gameEntity == null)
            {
                UnityEngine.Debug.Log("Target IGameEntity is null");
                return;
            }
            
            if (gameEntity is PlayerFacade player)
            {
                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "dragonHit" });
                if (projectile.CanStun)
                    player.Stun(projectile.StunDuration);
            }
            else
            {
                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "hit" });
            }
            
            gameEntity.TakeDamage(projectile.Damage);
        }

        private void ResolveProjectile(BaseProjectile projectile, IGameEntity gameEntity)
        {
            if (gameEntity != null && gameEntity.EntityType == EntityType.Player)
            {
                _projectileManager.DestroyProjectile(projectile);
            }
        }

        public void FixedTick()
        {
            ResolveViewportPositionForPlayerProjectiles();
            ResolveViewportPositionForEnemyProjectiles();
        }

        private void ResolveViewportPositionForPlayerProjectiles()
        {
            var projectiles = _projectileManager.ActiveProjectiles.Where(p => p.Value.Equals(EntityType.Player))
                .Select(p => p.Key).ToList();
            
            for (int i = projectiles.Count - 1; i >= 0; i--)
            {
                var projectile = projectiles[i];
                var position = projectile.transform.position;
                var viewportPos = _camera.WorldToViewportPoint(position);
                if (viewportPos.x < 0 - _screenRelativeBorder || viewportPos.x > 1f + _screenRelativeBorder ||
                    viewportPos.y < 0 - _screenRelativeBorder ||  viewportPos.y > 1f + _screenRelativeBorder)
                {
                    _projectileManager.DestroyProjectile(projectile, false);
                }
            }

        }
        
        private void ResolveViewportPositionForEnemyProjectiles()
        {
            var projectiles = _projectileManager.ActiveProjectiles.Where(p => p.Value.Equals(EntityType.Enemy))
                .Select(p => p.Key).ToList();
            for (int i = projectiles.Count - 1; i >= 0; i--)
            {
                var projectile = projectiles[i];
                var position = projectile.transform.position;
                var viewportPos = _camera.WorldToViewportPoint(position);
                if (viewportPos.x < 0 - _screenRelativeBorder || viewportPos.x > 1f + _screenRelativeBorder ||
                    viewportPos.y < 0 - _screenRelativeBorder || viewportPos.y > 1f + _screenRelativeBorder)
                {
                    _projectileManager.DestroyProjectile(projectile, false);
                }
            }
        }
    }
}