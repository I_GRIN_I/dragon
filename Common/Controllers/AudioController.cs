using Common.Signals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Zenject;
using System.Linq;
using Progress;
using UnityEngine.UI;
using UniRx;

public class AudioController : MonoBehaviour
{
    private const string SoundsPath = "Audio/Sound/";

    private SignalBus _signalBus;
    [SerializeField]
    private AudioMixerGroup _soundGroup;

    [SerializeField]
    private int _sourcesCount = 10;
    private List<AudioSource> _sources;

    [SerializeField]
    private Toggle _soundToggle;
    private bool _soundEnabled;

    private LocalSaveController _saveController;

    private Dictionary<string, AudioClip> _cachedClips = new Dictionary<string, AudioClip>();

    [Inject]
    public void Initialize(SignalBus signalBus, LocalSaveController saveController)
    {
        _signalBus = signalBus;
        _saveController = saveController;
    }
    private void Start()
    {
        InitializeSounds();
        _signalBus.Subscribe<AudioSignals.PlaySound>(signal =>
        {
            var source = _sources.Where(x =>
            {
                return !x.isPlaying;
            }).FirstOrDefault();
            if (!source)
                source = GetNewSource();
            if (!_cachedClips.ContainsKey(signal.SoundName))
                _cachedClips.Add(signal.SoundName, Resources.Load<AudioClip>($"{SoundsPath}{signal.SoundName}"));
            source.clip = _cachedClips[signal.SoundName];
            if (signal.RandomPitch)
                source.pitch = Random.Range(0.85f, 1.15f);
            source.volume = signal.Volume;
            source.Play();
        });
        _signalBus.Subscribe<AudioSignals.StopSound>(signal =>
        {
            if (!_cachedClips.ContainsKey(signal.SoundName))
                return;
            _sources.Where(x =>
            {
                return x.isPlaying && x.clip == _cachedClips[signal.SoundName];
            }).ToList().ForEach(source => source.Stop());
        });
        _soundToggle.onValueChanged.AsObservable()
            .Subscribe(enabled =>
            {
                _soundGroup.audioMixer.SetFloat("SoundVolume", enabled ? -10 : -80);
                _saveController.ProgressData.Sound = enabled;
            });
        _soundToggle.isOn = _saveController.ProgressData.Sound;
    }

    public AudioSource GetNewSource()
    {
        var source = gameObject.AddComponent<AudioSource>();
        source.outputAudioMixerGroup = _soundGroup;
        source.playOnAwake = false;
        _sources.Add(source);
        return source;
    }

    public void InitializeSounds()
    {
        _sources = new List<AudioSource>(_sourcesCount);
        for (int i = 0; i < _sourcesCount; i++)
        {
            GetNewSource();
        }
    }
}
