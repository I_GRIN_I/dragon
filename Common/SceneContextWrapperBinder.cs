using System;
using Entities.Player.Data;
using UnityEngine;
using Zenject;

namespace Common
{
    public class SceneContextWrapperBinder : MonoBehaviour
    {
        [Inject(Id = TransformType.PlayerAnchor)]
        private Transform _player;
        [Inject(Id = TransformType.TowerAnchor)]
        private Transform _tower;
        [Inject(Id = RectTransformType.UICoin)]
        private RectTransform _uiCoin;
        
        private SceneContextWrapper _wrapper;
        private Camera _camera;

        [Inject]
        private void Construct(SceneContextWrapper wrapper, Camera camera)
        {
            _camera = camera;
            _wrapper = wrapper;
        }

        private void Awake()
        {
            _wrapper.BindTransform(TransformType.PlayerAnchor, _player);
            _wrapper.BindTransform(TransformType.TowerAnchor, _tower);
            _wrapper.BindRectTransform(RectTransformType.UICoin, _uiCoin);
            _wrapper.BindCamera(_camera);
        }
    }
}