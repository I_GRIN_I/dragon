using Common.Signals;
using UnityEngine;
using Zenject;

namespace Common
{
    public class GameLoadedTrigger : MonoBehaviour
    {
        private SignalBus _signalBus;
        [Inject]
        protected void Construct(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }
    }
}
