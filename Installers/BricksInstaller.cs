﻿using Bricks;
using Zenject;
using BrickSignals;

public class BricksInstaller : Installer
{
    public override void InstallBindings()
    {
        Container.BindInterfacesTo<BricksController>().AsSingle();
        //Container.DeclareSignal<HitBrick>();
    }
}