using Zenject;
using Projectiles;
using UnityEngine;

public class ProjectilesInstaller : Installer
{
    public override void InstallBindings()
    {
        Container.BindFactory<GameObject, Vector2, Vector2, float, int, Projectile, PlaceholderProjectileFactory>().FromFactory<ProjectileFactory>();
        Container.BindInterfacesTo<ProjectilesController>().AsSingle().NonLazy();
    }
}
