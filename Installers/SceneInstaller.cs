using Economics;
using UnityEngine;
using Zenject;

public class SceneInstaller : MonoInstaller
{
    [SerializeField]
    private CoinsText _coinsText;
    public override void InstallBindings()
    {
        Container.Bind<CoinsText>().FromInstance(_coinsText);
    }
}
