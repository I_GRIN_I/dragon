﻿using Bricks;
using Projectiles;
namespace BrickSignals
{
    public class HitBrick
    {
        public readonly BaseBrick Brick;
        public readonly Projectile Projectile;
        public HitBrick(BaseBrick brick, Projectile projectile)
        {
            Brick = brick;
            Projectile = projectile;
        }
    }
}
