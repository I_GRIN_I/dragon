﻿using Common.Signals;
using Progress;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class ProgressInstaller : MonoInstaller
    {
        [SerializeField] private ProgressDataRegistry _progressDataRegistry;
        public override void InstallBindings()
        {
            InstallSave();
            InstallLoad();

            Container.BindInterfacesAndSelfTo<LocalSaveController>().AsSingle().NonLazy();
            Container.BindInstance(_progressDataRegistry).AsSingle();
        }
        private void InstallSave()
        {
            Container.BindSignal<SaveSignals.Save>().ToMethod(signal =>
            {
                Container.ResolveAll<ISaveLoad>().ForEach(saveLoad => saveLoad.Save(signal.ProgressData));
            });
        }
        private void InstallLoad()
        {
            Container.BindSignal<SaveSignals.Load>().ToMethod(signal =>
            {
                Container.ResolveAll<ISaveLoad>().ForEach(saveLoad => saveLoad.Load(signal.ProgressData));
            });
        }
    }
}