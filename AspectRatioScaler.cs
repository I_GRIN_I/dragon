using System;
using UnityEngine;
public class AspectRatioScaler : MonoBehaviour
{
    [SerializeField] private Vector2 _referenceResolution;
    [SerializeField] private Transform _targetTransform;
    private Vector3 _initPos;

    private void Awake()
    {
        _initPos = _targetTransform.position;
        UpdatePos();
    }

    private float GetScaleFactor()
    {
        return (Screen.width * _referenceResolution.y) / (Screen.height *_referenceResolution.x);
    }
    
    private void UpdatePos()
    {
        var scaleFactor = GetScaleFactor();
        _targetTransform.position = Vector3.right * _initPos.x * scaleFactor + Vector3.up * _initPos.y;
    }

}
