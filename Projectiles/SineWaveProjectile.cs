﻿using Common.Signals;
using UnityEngine;

namespace Projectiles
{
    public class SineWaveProjectile : BaseProjectile
    {
        [SerializeField] private float _waveAmplitude = 0.5f;
        [SerializeField] private float _waveFrequency = 10f;
        
        private float elapsedTime = 0;
        protected override void UpdateProjectilePath()
        {
            elapsedTime += Time.fixedDeltaTime;
            var localHorizontalMovement = (Vector2) transform.right * (Time.fixedDeltaTime * Speed * TimeMultiplier);
            var localVerticalMovement = (Vector2) transform.up * (Mathf.Sin(_waveFrequency * elapsedTime) * _waveAmplitude * Time.fixedDeltaTime * TimeMultiplier);
            
            Rigidbody.position = Rigidbody.position + localHorizontalMovement + localVerticalMovement;
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            SignalBus.Fire(new GameSignals.ProjectileHit { Projectile = this, CollidedEntity = other.gameObject });
        }
    }
}