﻿using BrickSignals;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Common.Signals;
using System;

namespace Projectiles
{
    public class ProjectilesController : IInitializable
    {
        private readonly PlaceholderProjectileFactory _projectileFactory;
        private readonly SignalBus _signalBus;
        private GameObject _fireballPrefab;

        public ProjectilesController(PlaceholderProjectileFactory projectileFactory, SignalBus signalBus)
        {
            _projectileFactory = projectileFactory;
            _signalBus = signalBus;
        }

        public Projectile CreateProjectile(GameObject projectilePrefab, Vector2 position, Vector2 direction, float speed, int bounces)
        {
            return _projectileFactory.Create(projectilePrefab, position, direction, speed, bounces);
        }

        public Projectile ThrowFireball(Vector2 from, Vector2 direction, float speed, int bounces)
        {
            return CreateProjectile(_fireballPrefab, from, direction, speed, bounces);
        }

        public void Initialize()
        {
            _fireballPrefab = Resources.Load<GameObject>("Prefabs/Projectiles/Fireball");
            _signalBus.Subscribe<GameSignals.PlayerFiredProjectile>(PlayerThrowFireball);
        }

        private void PlayerThrowFireball(GameSignals.PlayerFiredProjectile signal)
        {
            ThrowFireball(signal.StartPosition, signal.Direction, 300, 3);
        }
    }
}
