
using Common.Signals;
using UnityEngine;
using Zenject;

namespace Projectiles
{
    public class SplitProjectile : StraightProjectile
    {
        [Space(20f)]
        [SerializeField] private BaseProjectile _splitShardsPrefab;
        [SerializeField] private int _numberOfShards;
        [SerializeField] private int _offsetAngle;
        
        protected override void UpdateProjectilePath()
        {
            base.UpdateProjectilePath();
            var currentDistance = Vector2.Distance(TargetPosition, transform.position);
            if (currentDistance / TargetDistance.magnitude <= 0.5f)
            {
                UnityEngine.Debug.Log("Passe half of path!");
                SignalBus.Fire(new GameSignals.SplitProjectileRequest()
                {
                    RequestSource = this,
                    InitialTarget = TargetPosition,
                    NumberOfShards = _numberOfShards,
                    OffsetAngle = _offsetAngle,
                    Origin = Rigidbody.position,
                    ProjectileShardsPrefab = _splitShardsPrefab
                });
            }
        }
    }
}