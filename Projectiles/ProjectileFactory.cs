﻿using UnityEngine;
using Zenject;

namespace Projectiles
{
    public class ProjectileFactory : IFactory<GameObject, Vector2, Vector2, float, int, Projectile>
    {
        private DiContainer _container;

        public ProjectileFactory(DiContainer container)
        {
            _container = container;
        }
        public Projectile Create(GameObject prefab, Vector2 position, Vector2 direction, float speed, int bounces)
        {
            return _container.InstantiatePrefab(prefab, new GameObjectCreationParameters
            {
                Position = position
            }).GetComponent<Projectile>().Init(direction, speed, bounces);
        }
    }
    public class PlaceholderProjectileFactory : PlaceholderFactory<GameObject, Vector2, Vector2, float, int, Projectile>
    {
    }
}
