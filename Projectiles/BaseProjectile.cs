using System;
using System.Collections;
using Common.Settings;
using Common.Signals;
using Entities;
using UnityEngine;
using Upgrades;
using Zenject;
using UniRx;
using Object = UnityEngine.Object;

// ReSharper disable Unity.InefficientPropertyAccess

namespace Projectiles
{
    public abstract class BaseProjectile : MonoBehaviour, ITimeDependent
    {
        public class Factory : PlaceholderFactory<Object, BaseProjectile>
        { }
        [SerializeField] protected Rigidbody2D Rigidbody;
        [SerializeField] private float _defaultDamage;
        [SerializeField] private float _defaultSpeed;
        [SerializeField] private bool _defaultCanStun;
        [SerializeField] private float _defaultStunDuration;
        [SerializeField] protected ParticleSystem HitParticle;
        [SerializeField] protected string HitSound;

        protected Vector2 Direction => _dirVector.normalized;
        protected Vector2 TargetDistance => _dirVector;
        protected Vector2 TargetPosition => _startPosition + _dirVector;
        
        protected SignalBus SignalBus;
        private Vector3 _startPosition;
        private Vector3 _dirVector;
        private bool _wasReflected;
        private bool _simulatePath = true;
        
        public int ReflectionsCount { get; private set; }
        public float Speed => _defaultSpeed;
        public float Damage => _defaultDamage;
        public bool CanStun => _defaultCanStun;
        public float StunDuration => _defaultStunDuration;

        public float TimeMultiplier { get; private set; } = 1f;
        

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            SignalBus = signalBus;
        }
        public void Initialize(Vector3 position, Vector3 direction)
        {
            transform.SetParent(null);
            Rigidbody.position = transform.position = _startPosition = position;
            transform.right = _dirVector = direction;
        }

        public void Initialize(Vector3 position, Vector3 direction, SpecialSettings fireSettings)
        {
            _defaultDamage = fireSettings.Damage;
            _defaultSpeed = fireSettings.Speed;
            _defaultCanStun = fireSettings.CanStun;
            _defaultStunDuration = fireSettings.StunDuration;
            
            Initialize(position, direction);
        }
        

        protected void FixedUpdate()
        {
            if (!_simulatePath) return;
            _wasReflected = false;
            UpdateProjectilePath();
        }

        protected abstract void UpdateProjectilePath();
        

        private IEnumerator ScheduleDisposal(float lifetime)
        {
            float timeElapsed = 0f;
            while (timeElapsed < lifetime)
            {
                yield return null;
                timeElapsed += Time.deltaTime * TimeMultiplier;
            }
            Destroy(gameObject);
        }

        public void ReflectDirection(Vector2 inNormal)
        {
            if (!_wasReflected)
            {
                _dirVector = Vector2.Reflect(_dirVector, inNormal);
                transform.right = _dirVector;
                _wasReflected = true;
            }
        }

        public void SetTimeMultiplier(float timeMultiplier)
        {
            TimeMultiplier = timeMultiplier;
        }

        public void Destroy(bool effects)
        {
            _simulatePath = false;
            if (effects && HitParticle)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    var childs = transform.GetChild(i).GetComponent<ParticleSystem>();
                    if (!childs) continue;
                    if(!childs.Equals(HitParticle))
                        childs.gameObject.SetActive(false);
                }
                HitParticle.Clear();
                HitParticle.Play();
                if(!string.IsNullOrWhiteSpace(HitSound))
                    SignalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = HitSound });
                var particleObject = gameObject;
                Observable.Timer(TimeSpan.FromSeconds(HitParticle.main.duration))
                    .Subscribe(_ =>
                    {
                        Object.Destroy(particleObject);
                    });
                Object.Destroy(this);
            }
            else
                Object.Destroy(gameObject);
        }
    }
}
