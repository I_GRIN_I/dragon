using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Data;
using Common.Settings;
using UniRx;
using UnityEngine;

namespace Projectiles
{
    public class ProjectileManager
    {
        private BaseProjectile.Factory _factory;

        private ReactiveDictionary<BaseProjectile, EntityType> _activeProjectiles =
            new ReactiveDictionary<BaseProjectile, EntityType>();
        public IReadOnlyReactiveDictionary<BaseProjectile, EntityType> ActiveProjectiles => _activeProjectiles;
        
        public ProjectileManager(BaseProjectile.Factory factory)
        {
            _factory = factory;
        }
        public void InvokeProjectile(SpecialSettings fireSettings, EntityType source, Vector2 worldPos, Vector2 moveVector)
        {
            var projectile =  _factory.Create(fireSettings.Projectile);
            projectile.Initialize(worldPos, moveVector, fireSettings);
            _activeProjectiles.Add(projectile, source);
        } 
        public void InvokeProjectile(BaseProjectile projectilePrefab, EntityType source, Vector2 worldPos, Vector2 moveVector)
        {
            var projectile =  _factory.Create(projectilePrefab);
            projectile.Initialize(worldPos, moveVector);
            _activeProjectiles.Add(projectile, source);
        }

        public void PauseProjectiles()
        {
            foreach (var projectile in _activeProjectiles.Keys)
            {
                projectile.SetTimeMultiplier(0f);
            }
        }
        public void ResumeProjectiles()
        {
            foreach (var projectile in _activeProjectiles.Keys)
            {
                projectile.SetTimeMultiplier(1f);
            }
        }


        public void DestroyProjectile(BaseProjectile baseProjectile, bool effects = true)
        {
            if (_activeProjectiles.ContainsKey(baseProjectile))
            {
                _activeProjectiles.Remove(baseProjectile);
            }
            baseProjectile?.Destroy(effects);
        }

        public void DestroyAllProjectiles()
        {
            var keys = _activeProjectiles.Keys.ToList();
            foreach (var projectile in keys)
            {
                DestroyProjectile(projectile);
            }
        }

        public EntityType GetProjectileSource(BaseProjectile projectile)
        {
            return _activeProjectiles[projectile];
        }
    }
}
