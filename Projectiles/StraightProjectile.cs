﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Signals;
using UnityEngine;

namespace Projectiles
{
    public class StraightProjectile : BaseProjectile
    {
        [SerializeField] private bool _useCast;
        [SerializeField] private LayerMask _sphereCastMask;
        private List<RaycastHit2D> _hits = new List<RaycastHit2D>();
        private float _ballRadius;

        private void Awake()
        {
            var f_collider = GetComponent<CircleCollider2D>();
            var f_ransform = GetComponent<Transform>();
            if (f_collider != null && f_ransform != null)
            {
                _ballRadius = f_collider.radius * f_ransform.localScale.x;
            }
        }
        protected override void UpdateProjectilePath()
        {
            var value = Rigidbody.position + Direction * (Speed * Time.fixedDeltaTime * TimeMultiplier);
            if (_useCast)
            {
                _hits.Clear();
                var filter = new ContactFilter2D();
                filter.SetLayerMask(_sphereCastMask);
                var distance = Vector2.Distance(Rigidbody.position, value);
                var hitsCount = Physics2D.CircleCast(Rigidbody.position, _ballRadius, Direction, filter, _hits, distance);
                if (hitsCount > 0)
                {
                    var closestHit = _hits.First();
                    value = closestHit.point + closestHit.normal * _ballRadius;
                    ReflectDirection(closestHit.normal);
                    SignalBus.Fire(new GameSignals.ProjectileHit { Projectile = this, CollidedEntity = closestHit.collider.gameObject });
                }
            }

            Rigidbody.position = value;
        }
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            if(!_useCast)
                SignalBus.Fire(new GameSignals.ProjectileHit { Projectile = this, CollidedEntity = other.gameObject });
        }
    }
}