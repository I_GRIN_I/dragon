using Bricks;
using BrickSignals;
using UnityEngine;
using Zenject;

namespace Projectiles
{
    public class Projectile : MonoBehaviour
    {
        public Vector2 Direction { get; private set; }
        public float Speed { get; private set; }
        public int Bounces { get; private set; }
        private Rigidbody2D _rigidbody;
        private SignalBus _signalBus;

        public Projectile Init(Vector2 direction, float speed, int bounces)
        {
            Direction = direction;
            Speed = speed;
            Bounces = bounces;
            return this;
        }

        [Inject]
        public void Initialize(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            _rigidbody.MovePosition(_rigidbody.position + Direction * (Speed * Time.deltaTime));
        }

        public bool SpendBounceOrDestroy()
        {
            if(--Bounces <= 0)
            {
                Destroy(gameObject);
                return true;
            }
            return false;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var brick = collision.collider.GetComponent<BaseBrick>();
            if (brick)
            {
                _signalBus.Fire(new HitBrick(brick, this));
                if (SpendBounceOrDestroy())
                    return;
                var contact = collision.GetContact(0);
                var sumContact = contact.normal;
                Direction = Vector2.Reflect(Direction, sumContact);
            }
        }
    }
}
