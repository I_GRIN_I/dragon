using System;
using System.Collections.Generic;
using System.Linq;
using Common.Data;
using Common.Settings;
using Common.Signals;
using Economics;
using Progress;
using UnityEngine;
using Zenject;

namespace Upgrades
{
    public class UpgradesController : ISaveLoad
    {
        private Dictionary<UpgradeType, int> _upgrades = new Dictionary<UpgradeType, int>();
        
        private readonly PlayerSettings _playerSettings;
        private readonly List<UpgradeSettings> _upgradeSettings;
        private SignalBus _signalBus;
        public UpgradesController(SignalBus signalBus, PlayerSettings playerSettings, List<UpgradeSettings> upgradeSettings)
        {
            _playerSettings = playerSettings;
            _upgradeSettings = upgradeSettings;
            _signalBus = signalBus;
            InitUpgrades();
        }

        private void InitUpgrades()
        {
            foreach (var upgradeStr in Enum.GetNames(typeof(UpgradeType)))
            {
                if (Enum.TryParse<UpgradeType>(upgradeStr,out var upgradeType))
                    _upgrades.Add(upgradeType, 0);
            }
        }

        public int GetUpgradeCost(UpgradeType upgradeType)
        {
            var upgradeSetting = _upgradeSettings.First(s => s.UpgradeType.Equals(upgradeType));
            var upgradeLevel = GetUpgradeLevel(upgradeType);
            return Mathf.RoundToInt(upgradeSetting.UpgradeCost * Mathf.Pow(1.1601f, upgradeLevel - 1));
        }
        public int GetUpgradeLevel(UpgradeType upgradeType)
        {
            TryAdd(upgradeType);
            return _upgrades[upgradeType];
        }

        private void TryAdd(UpgradeType upgradeType)
        {
            if (!_upgrades.ContainsKey(upgradeType))
            {
                _upgrades.Add(upgradeType, 0);
            }
        }

        public float GetUpgradedValue(UpgradeType upgradeType)
        {
            var upgradeSetting = _upgradeSettings.First(s => s.UpgradeType.Equals(upgradeType));
            switch (upgradeType)
            {
                case UpgradeType.Damage:
                    return _playerSettings.SpecialSettings.Damage + _upgrades[UpgradeType.Damage] * upgradeSetting.ValuePerLevel;
                case UpgradeType.Speed:
                    return _playerSettings.BaseVelocity + _upgrades[UpgradeType.Speed] * upgradeSetting.ValuePerLevel;
                case UpgradeType.Income:
                    return 1 + _upgrades[UpgradeType.Income] * upgradeSetting.ValuePerLevel;
                case UpgradeType.HP:
                    return _playerSettings.BaseHealth + _upgrades[UpgradeType.HP] * upgradeSetting.ValuePerLevel;
                case UpgradeType.ChargeTime:
                    return _playerSettings.SpecialSettings.FireChargeTime / (Mathf.Pow(upgradeSetting.ValuePerLevel, _upgrades[UpgradeType.ChargeTime]));
                default:
                    throw new ArgumentOutOfRangeException(nameof(upgradeType), upgradeType, null);
            }
        }
        public void TryToBuyUpgrade(UpgradeType upgradeType)
        {
            TryAdd(upgradeType);

            Action<bool> callback = (isSucceed) =>
            {
                if (isSucceed)
                {
                    _upgrades[upgradeType]++;
                    _signalBus.Fire(new UpgradesSignals.UpgradeUpdated() {UpgradeType = upgradeType});
                }
            };
            
            _signalBus.Fire(new EconomicsSignals.PurchaseRequest()
                {Price = GetUpgradeCost(upgradeType), Callback = callback});
        }
        public void Load(ProgressData progressData)
        {
            _upgrades[UpgradeType.Damage] = progressData.DamageUpgradeLevel;
            _upgrades[UpgradeType.Speed] = progressData.SpeedUpgradeLevel;
            _upgrades[UpgradeType.Income] = progressData.IncomeUpgradeLevel;
            _upgrades[UpgradeType.HP] = progressData.HPUpgradeLevel;
            _upgrades[UpgradeType.ChargeTime] = progressData.ChargeTimeUpgradeLevel;
            _signalBus.Fire(new UpgradesSignals.UpgradeUpdated() {UpgradeType = UpgradeType.Damage});
            _signalBus.Fire(new UpgradesSignals.UpgradeUpdated() {UpgradeType = UpgradeType.Speed});
            _signalBus.Fire(new UpgradesSignals.UpgradeUpdated() {UpgradeType = UpgradeType.Income});
            _signalBus.Fire(new UpgradesSignals.UpgradeUpdated() {UpgradeType = UpgradeType.HP});
            _signalBus.Fire(new UpgradesSignals.UpgradeUpdated() {UpgradeType = UpgradeType.ChargeTime});
        }

        public void Save(ProgressData progressData)
        {
            progressData.DamageUpgradeLevel = _upgrades[UpgradeType.Damage];
            progressData.SpeedUpgradeLevel = _upgrades[UpgradeType.Speed];
            progressData.IncomeUpgradeLevel = _upgrades[UpgradeType.Income];
            progressData.HPUpgradeLevel = _upgrades[UpgradeType.HP];
            progressData.ChargeTimeUpgradeLevel = _upgrades[UpgradeType.ChargeTime];
        }
    }
}
