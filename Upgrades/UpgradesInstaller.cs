﻿using Upgrades;
using Zenject;

public class UpgradesInstaller : Installer
{
    public override void InstallBindings()
    {
        Container.BindInterfacesTo<UpgradesController>().AsSingle();
    }
}
