using UniRx;

namespace Progress
{
    public class ProgressController : ISaveLoad
    {
        public string CurrentFloorId { get; private set; }
        public ReadOnlyReactiveProperty<long> Level => _level.ToReadOnlyReactiveProperty();
        private ReactiveProperty<long> _level = new ReactiveProperty<long>(); 
        public void Save(ProgressData progressData)
        {
            progressData.CurrentLevel = _level.Value;
            progressData.FloorId = CurrentFloorId;
        }
        public void Load(ProgressData progressData)
        {
            _level.Value = progressData.CurrentLevel;
            CurrentFloorId = progressData.FloorId;
        }

        public void IncreaseLevel()
        {
            _level.Value++;
        }
        public void SetFloorId(string floorId)
        {
            CurrentFloorId = floorId;
        }
    }
}