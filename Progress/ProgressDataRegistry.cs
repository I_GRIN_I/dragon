using System;
using UnityEngine;

namespace Progress
{
    [CreateAssetMenu(fileName = "ProgressDataRegistry", menuName = "Data/Progress", order = 0)]
    public class ProgressDataRegistry : ScriptableObject
    {
        public ProgressData Data;
    }
}