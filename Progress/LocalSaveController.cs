using Common.Signals;
using Utils;
using Zenject;

namespace Progress
{
    public class LocalSaveController
    {
        private const string SavedPrefsKey = "Progress";

        private readonly SignalBus _signalBus;
        private readonly ProgressDataRegistry _registry;

        public ProgressData ProgressData { get; private set; }


        public LocalSaveController(SignalBus signalBus, ProgressDataRegistry registry)
        {
            _signalBus = signalBus;
            _registry = registry;
            
            _signalBus.Subscribe<GameSignals.GameSaveRequest>(_ => SaveProgress());
            _signalBus.Subscribe<GameSignals.GameLoadRequest>(_ => LoadProgress());
        }
        public void LoadProgress()
        {
            ProgressData = GameHelper.LoadDataFromPlayerPrefs<ProgressData>(SavedPrefsKey) ?? new ProgressData();
            _signalBus.Fire(new SaveSignals.Load(ProgressData));
            UpdateRegistry();    

        }

        public void SaveProgress()
        {
            _signalBus.Fire(new SaveSignals.Save(ProgressData));
            GameHelper.SaveDataToPlayerPrefs(ProgressData, SavedPrefsKey);
            UpdateRegistry();
        }
        
        private void UpdateRegistry()
        {
            _registry.Data = ProgressData;
        }

    }
}