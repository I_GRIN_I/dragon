﻿using System;

namespace Progress
{
    [Serializable]
    public class ProgressData
    {
        public string Coins;
        public string ChainCoins;
        public long CurrentLevel;
        public string FloorId;

        public int DamageUpgradeLevel;
        public int SpeedUpgradeLevel;
        public int IncomeUpgradeLevel;
        public int HPUpgradeLevel;
        public int ChargeTimeUpgradeLevel;

        public bool Sound;
        public ProgressData()
        {
            Coins = "0";
            ChainCoins = "0";
            Sound = true;
        }
    }
}