using UnityEngine;

namespace Progress
{
    public interface ISaveLoad
    {
        public void Save(ProgressData progressData);
        public void Load(ProgressData progressData);
    }
}