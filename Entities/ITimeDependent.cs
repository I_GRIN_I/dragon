namespace Entities
{
    public interface ITimeDependent
    {
        float TimeMultiplier { get; }
        void SetTimeMultiplier(float timeMultiplier);
    }
}