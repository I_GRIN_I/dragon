using System;
using Common;
using Common.Data;
using Common.Settings;
using Common.Signals;
using Projectiles;
using UniRx;
using UnityEngine;
using Zenject;

namespace Entities
{
    public class FireController : IDisposable, ITickable
    {
        public ReadOnlyReactiveProperty<bool> IsAbleToFire => _isAbleToFire.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<bool> IsReadyToFire =>
            _fireCooldown.AsUnitObservable()
                .Merge(_isAbleToFire.AsUnitObservable())
                .Merge(_currentSpecial.AsUnitObservable())
                .Select(_=>_currentSpecial.HasValue && _isAbleToFire.Value && _fireCooldown.Value <= 0)
            .ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<float> FireCooldown => _fireCooldown.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<float> FireCooldownRelative => _fireCooldown.Select(cooldown=>Mathf.Clamp01(cooldown / FireRate)).ToReadOnlyReactiveProperty();
        public float FireRate { get; private set; }
        private ReactiveProperty<bool> _isAbleToFire = new ReactiveProperty<bool>();
        private ReactiveProperty<float> _fireCooldown = new ReactiveProperty<float>(float.MaxValue);
        private readonly IGameEntity _gameEntity;
        private readonly SignalBus _signalBus;
        private readonly ProjectileManager _projectileManager;

        private ReactiveProperty<SpecialSettings> _currentSpecial = new ReactiveProperty<SpecialSettings>();

        public FireController(IGameEntity gameEntity, SignalBus signalBus,
            ProjectileManager projectileManager, [InjectOptional] bool isAbleByDefault)
        {
            _gameEntity = gameEntity;
            _signalBus = signalBus;
            _projectileManager = projectileManager;
            if (isAbleByDefault) 
                _isAbleToFire.Value = true;
        }

        public void ChargeProjectile(SpecialSettings specialSettings)
        {
            _currentSpecial.Value = specialSettings;
            _fireCooldown.Value = specialSettings.FireChargeTime;
        }
        public void Fire(Vector2 originWorldPos, Vector2 targetWorldPos)
        {
            if (IsReadyToFire.Value)
            {
                var direction = targetWorldPos - originWorldPos;
                if (direction  == Vector2.zero)
                {
                    Debug.LogError("No direction for projectile!");
                    return;
                }
                _projectileManager.InvokeProjectile(_currentSpecial.Value, _gameEntity.EntityType, originWorldPos, direction);
                
                _signalBus.Fire(new GameSignals.PlayerFiredProjectile(){ Direction = direction.normalized, StartPosition = originWorldPos});

                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = _gameEntity.EntityType == EntityType.Player ? $"fireball{UnityEngine.Random.Range(0,3)}" : "magic" });

                ChargeProjectile(_currentSpecial.Value);
            }
            else
            {
                Debug.LogError("[FireController] Tried to fire projectile, but wasn't ready to fire!");
            }
        }
        
        public void Tick()
        {
            _fireCooldown.Value = Mathf.Max(0, _fireCooldown.Value - Time.deltaTime);
        }
        public void SetIsAbleToFire(bool isAble)
        {
            _isAbleToFire.Value = isAble;
        }
        public void SetFireRate(float fireRate)
        {
            FireRate = fireRate;
        }

        public void Dispose()
        {
            _isAbleToFire?.Dispose();
            _fireCooldown?.Dispose();
        }
    }
}
