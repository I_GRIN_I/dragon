using UniRx;
using UnityEngine;

namespace Entities
{
    public interface IAimController
    {
        ReadOnlyReactiveProperty<bool> IsAbleToAim { get; }
        ReadOnlyReactiveProperty<bool> IsAiming { get; }
        ReadOnlyReactiveProperty<Vector2> TargetWorldPosition { get; }
        ReadOnlyReactiveProperty<Vector2> OriginWorldPosition { get; }
        void SetIsAbleToAim(bool isAble);
    }
}