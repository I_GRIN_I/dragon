using Entities.Player.Data;
using System;
using UniRx;
using UnityEngine;
using Zenject;

namespace Entities
{
    public interface IHealthController : IHealthState
    {
        public void SetCurrentHp(float newHp);
        public void SetMaxHp(float newHp);
        public void TakeDamage(float damageHp);
        public void Heal(float healHp);
        public void SetDamageImmunity(bool isImmune);
    }
    public interface IHealthState
    {
        ReadOnlyReactiveProperty<float> CurrentHp { get; }
        ReadOnlyReactiveProperty<float> MaxHp { get; }
        ReadOnlyReactiveProperty<bool> IsAlive { get; }
        ReadOnlyReactiveProperty<bool> IsImmuneToDamage { get; }
    }

    class HealthController : IHealthController
    {
        public ReadOnlyReactiveProperty<float> CurrentHp => _currentHp.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<float> MaxHp => _maxHp.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<bool> IsAlive => _currentHp.Select(hp => hp > 0).ToReadOnlyReactiveProperty();

        public ReadOnlyReactiveProperty<bool> IsImmuneToDamage => _isImmune.ToReadOnlyReactiveProperty();

        private ReactiveProperty<float> _currentHp = new ReactiveProperty<float>();
        private ReactiveProperty<float> _maxHp = new ReactiveProperty<float>();
        private ReactiveProperty<bool> _isImmune = new ReactiveProperty<bool>();
        private ParticleSystem _healParticle;

        public HealthController([InjectOptional] float startHp, [InjectOptional(Id = ParticleSystemType.Heal)] ParticleSystem healParticle)
        {
            _healParticle = healParticle;
            SetMaxHp(startHp);
            Heal(startHp, false);
        }
        public void SetMaxHp(float newHp)
        {
            _maxHp.Value = newHp;
            SetCurrentHp(CurrentHp.Value);
        }

        public void TakeDamage(float damageHp)
        {
            if (_isImmune.Value)
            {
                return;
            }

            SetCurrentHp(CurrentHp.Value - damageHp);
        }

        public void Heal(float healHp)
        {
            Heal(healHp, _healParticle ? true : false);
        }

        public void Heal(float healHp, bool effect)
        {
            if (effect)
            {
                _healParticle.Play();
                Observable.Timer(TimeSpan.FromSeconds(1.5f))
                    .Subscribe(_ => _healParticle.Stop());
            }
            SetCurrentHp(CurrentHp.Value + healHp);
        }

        public void SetCurrentHp(float newHp)
        {
            _currentHp.Value = Mathf.Clamp(newHp, 0, MaxHp.Value);
        }

        public void SetDamageImmunity(bool isImmune)
        {
            _isImmune.Value = isImmune;
        }
    }
}
