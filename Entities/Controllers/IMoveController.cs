using UniRx;
using UnityEngine;

namespace Entities
{
    public interface IMoveController
    {
        ReadOnlyReactiveProperty<Vector2> CurrentWorldPosition { get; }
        ReadOnlyReactiveProperty<bool> IsAbleToMove { get; }
        float Speed { get; }
        void MoveToWorldPosition(Vector2 targetPosition);
        void SetSpeed(float speed);
        void SetIsAbleToMove(bool isAble);
    }
}