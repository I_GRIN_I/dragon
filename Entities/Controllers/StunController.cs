using Common.Signals;
using Entities.Player.Data;
using System;
using UniRx;
using UnityEngine;
using Zenject;

namespace Entities
{
    public class StunController : IResettable, ITickable
    {
        public ReadOnlyReactiveProperty<bool> IsStunned => _isStunned.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<float> TimeLeft => _stunTimeLeft.ToReadOnlyReactiveProperty();
        public float StunDuration { get; private set; } = float.Epsilon;
        private ReactiveProperty<bool> _isStunned = new ReactiveProperty<bool>();
        private ReactiveProperty<float> _stunTimeLeft = new ReactiveProperty<float>();
        private SignalBus _signalBus;
        private ParticleSystem _stunParticle;
        
        public StunController(SignalBus signalBus, [InjectOptional(Id = ParticleSystemType.Stun)] ParticleSystem particleSystem)
        {
            _signalBus = signalBus;
            _stunParticle = particleSystem;
        }

        public void Stun(float stunDuration)
        {
            _isStunned.Value = true;
            _stunTimeLeft.Value = StunDuration = stunDuration;
            _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "stun", Volume = .2f });
            _stunParticle.Play();
            Observable.Timer(TimeSpan.FromSeconds(stunDuration))
                .Subscribe(_ =>
                {
                    _signalBus.Fire(new AudioSignals.StopSound() { SoundName = "stun" });
                    _stunParticle.Stop();
                });
        }

        public void EndStun()
        {
            _isStunned.Value = false;
            _stunTimeLeft.Value = 0;
            StunDuration = float.Epsilon;
        }

        public void Reset()
        {
            EndStun();
        }

        public void Tick()
        {
            if (_isStunned.Value)
            {
                _stunTimeLeft.Value = Mathf.Max(0, _stunTimeLeft.Value - Time.deltaTime);
                if (_stunTimeLeft.Value <= 0)
                {
                    EndStun();
                }
            }
        }
    }
}