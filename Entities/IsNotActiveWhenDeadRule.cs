using System;
using UniRx;
using Zenject;

namespace Entities
{
    public class IsNotActiveWhenDeadRule : IInitializable, IDisposable
    {
        private readonly IHealthController _healthController;
        private readonly IMoveController _moveController;
        private readonly IAimController _aimController;
        private readonly FireController _fireController;
        private IDisposable _isAliveStream;

        public IsNotActiveWhenDeadRule(IHealthController healthController, IMoveController moveController,
            IAimController aimController, FireController fireController)
        {
            _healthController = healthController;
            _moveController = moveController;
            _aimController = aimController;
            _fireController = fireController;
        }

        public void Initialize()
        {
            _isAliveStream = _healthController.IsAlive.Subscribe(isAlive =>
            {
                _moveController.SetIsAbleToMove(isAlive);
                _aimController.SetIsAbleToAim(isAlive);
                _fireController.SetIsAbleToFire(isAlive);
            });
        }

        public void Dispose()
        {
            _isAliveStream?.Dispose();
        }
    }
}