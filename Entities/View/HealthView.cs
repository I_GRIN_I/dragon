using DG.Tweening;
using TMPro;
using UniRx;
using UnityEngine;
using Utils;
using Zenject;

namespace Entities.View
{
    public class HealthView : MonoBehaviour
    {
        [SerializeField] private TransformProgressBar _progressBar;
        [SerializeField] private float _animationDuration;
        [SerializeField] private Ease _animationEase;
        private IHealthState _healthState;
        private Tweener _tween;

        [Inject]
        private void Construct(IHealthState healthState)
        {
            _healthState = healthState;
        }

        private void Start()
        {
            _healthState.CurrentHp.Merge(_healthState.MaxHp).Subscribe(_ => UpdateView()).AddTo(this);
            _healthState.IsAlive.Subscribe(isAlive => ShowHideView(isAlive)).AddTo(this);
        }

        private void ShowHideView(bool isAlive)
        {
            gameObject.SetActive(isAlive);
        }

        private void UpdateView(bool playAnimation = true)
        {
            float maxHp = _healthState.MaxHp.Value == 0 ? float.Epsilon : _healthState.MaxHp.Value;
            var relativeHp = _healthState.CurrentHp.Value / maxHp;
            KillTweenIfActive();
            if (playAnimation)
            {
                _tween = _progressBar.DOProgress(relativeHp, _animationDuration).SetEase(_animationEase).Play();
            }
            else
            {
                _progressBar.Progress = relativeHp;
            }
        }

        private void KillTweenIfActive()
        {
            if (_tween.IsActive())
            {
                _tween.Kill();
            }
        }

        private void OnDestroy()
        {
            KillTweenIfActive();
        }
    }
}
