using Common.Settings;
using UnityEngine;
using Zenject;

namespace Entities.Enemy
{
    public class EnemiesFactory : IFactory<Vector2, EnemyFacade>
    {
        private readonly DiContainer _container;
        private readonly EnemySettings _enemySettings;

        public EnemiesFactory(DiContainer container, EnemySettings enemySettings)
        {
            _container = container;
            _enemySettings = enemySettings;
        }
        public EnemyFacade Create(Vector2 pos)
        {
            var enemy =  _container.ResolveId<EnemyFacade>(_enemySettings.Id);
            enemy.Initialize(pos);
            return enemy;
        }
    }
}