using System;
using System.Collections.Generic;
using Common.Settings;
using UniRx;
using Zenject;
using Random = UnityEngine.Random;

namespace Entities.Enemy
{
    public class AutoFireWhenIsReadyToFireRule : IInitializable, IDisposable
    {
        private readonly FireController _fireController;
        private readonly IAimController _aimController;
        private readonly List<SpecialSettings> _specialSetupSettings;
        private readonly GeneralSettings _generalSettings;
        private IDisposable _fireStream;

        private int specialIndex = 0;

        private bool CanFire => _fireController.IsReadyToFire.Value && _aimController.IsAiming.Value;

        public AutoFireWhenIsReadyToFireRule(FireController fireController, IAimController aimController, List<SpecialSettings> specialSetupSettings, GeneralSettings generalSettings)
        {
            _fireController = fireController;
            _aimController = aimController;
            _specialSetupSettings = specialSetupSettings;
            _generalSettings = generalSettings;
        }
        public void Initialize()
        {
            var startSpecial = GetCurrentSpecial();
            startSpecial.FireChargeTime = startSpecial.FireChargeTime * (1 - Random.value * _generalSettings.EnemyStartChargeProjectileDispersion);
            _fireController.ChargeProjectile(startSpecial);
            
            _fireStream = _fireController.IsReadyToFire.AsUnitObservable()
                .Merge(_aimController.IsAbleToAim.AsUnitObservable())
                .Where(_=>CanFire)
                .Subscribe(_ =>
                {
                    _fireController.Fire(_aimController.OriginWorldPosition.Value, _aimController.TargetWorldPosition.Value);
                    _fireController.ChargeProjectile(GetCurrentSpecial());
                });
        }

        private SpecialSettings GetCurrentSpecial()
        {
            var currentSpecial = _specialSetupSettings[specialIndex];
            specialIndex = specialIndex + 1 >= _specialSetupSettings.Count ? 0 : specialIndex + 1;
            return currentSpecial;
        }


        public void Dispose()
        {
            _fireStream?.Dispose();
        }
    }
}