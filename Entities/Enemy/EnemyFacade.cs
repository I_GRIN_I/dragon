﻿using Common;
using Common.Data;
using Common.Settings;
using Entities.Enemy.Controllers;
using UniRx;
using UnityEngine;
using Zenject;
using Upgrades;
using Common.Signals;

namespace Entities.Enemy
{
    public class EnemyFacade : MonoBehaviour, IGameEntity
    {
        private IHealthController _healthController;
        private IMoveController _moveController;
        private AutoAimController _aimController;
        private FireController _fireController;
        private PunchController _punchController;
        private UpgradesController _upgradesController;
        private SignalBus _signalBus;
        public EnemySettings Settings { get; private set; }

        [Inject]
        private void Construct(IHealthController healthController, IMoveController moveController,
            EnemySettings enemySettings, AutoAimController aimController, FireController fireController, PunchController punchController, UpgradesController upgradesController, SignalBus signalBus)
        {
            _punchController = punchController;
            _fireController = fireController;
            _aimController = aimController;
            Settings = enemySettings;
            _moveController = moveController;
            _healthController = healthController;
            _upgradesController = upgradesController;
            _signalBus = signalBus;
        }

        public void Initialize(Vector2 position)
        {
            _moveController.MoveToWorldPosition(position);
            int finalHp = Mathf.FloorToInt(Settings.HP * Mathf.Pow(_upgradesController.GetUpgradedValue(UpgradeType.Damage), EnemiesManager.MOB_DIFFICULTY)) + 1;
            _healthController.SetMaxHp(finalHp);
            _healthController.Heal(finalHp);
            _healthController.SetDamageImmunity(Settings.IsImmuneToFireballs);
            Pause();
        }
        public EntityType EntityType => EntityType.Enemy;
        public ReadOnlyReactiveProperty<Vector2> WorldPosition => _moveController.CurrentWorldPosition;
        public ReadOnlyReactiveProperty<float> CurrentHp => _healthController.CurrentHp;
        public ReadOnlyReactiveProperty<float> MaxHp => _healthController.MaxHp;
        public ReadOnlyReactiveProperty<bool> IsAlive => _healthController.IsAlive;

        public bool IsOnPause { get; private set; }

        public ReadOnlyReactiveProperty<bool> IsImmuneToDamage => _healthController.IsImmuneToDamage;

        public void Pause()
        {
            AllowAimAndShoot(false);
            IsOnPause = true;
        }

        public void Resume()
        {
            if (IsAlive.Value)
            {
                AllowAimAndShoot(true);
            }
            IsOnPause = false;
        }

        public void SetMaxHp(float newHp) => _healthController.SetMaxHp(newHp);
        public void TakeDamage(float damageHp) => _healthController.TakeDamage(damageHp);
        public void Heal(float healHp) => _healthController.Heal(healHp);
        public void SetCurrentHp(float newHp) => _healthController.Heal(newHp);

        public void AttachAimTarget(ReadOnlyReactiveProperty<Vector2> worldPos) =>
            _aimController.SetTargetWorldPoint(worldPos);

        public void AllowAimAndShoot(bool isAllowed)
        {
            _aimController.SetIsAbleToAim(isAllowed);
            _fireController.SetIsAbleToFire(isAllowed);
        }

        public void PunchToDeath(float force = 50f)
        {
            foreach (var collider in GetComponentsInChildren<Collider2D>())
            {
                collider.enabled = false;
            }
            _punchController.PunchPhysics(force);
            _healthController.TakeDamage(_healthController.CurrentHp.Value);
        }

        public void SetDamageImmunity(bool isImmune) => _healthController.SetDamageImmunity(isImmune);

    }
}