using System.Collections.Generic;
using Common;
using Common.Settings;
using Entities.Enemy.Controllers;
using Spine.Unity;
using UnityEngine;
using Zenject;

namespace Entities.Enemy.Bootstrap
{
    public class EnemyInstaller : MonoInstaller
    {
        [Header("Facade")]
        [SerializeField] private EnemyFacade _enemy;
        [Header("Settings")]
        [SerializeField] private Rigidbody2D _enemyRigidbody;
        [SerializeField] private Transform _enemyTransform;
        [SerializeField] private Transform _aimOriginTransform;
        [SerializeField] private SkeletonAnimation _skeletonAnimation;
        [SerializeField] private EnemySettings _enemySetting;

        // [Inject]
        // private void Construct(EnemySettings enemySettings)
        // {
        //     _enemySetting = enemySettings ?? _enemySetting;
        // }
        public override void InstallBindings()
        {
            InstallMovement();
            InstallAiming();
            InstallFiring();
            InstallHealth();
            
            Container.Bind<EnemySettings>().FromInstance(_enemySetting).AsSingle();
        }

        private void InstallFiring()
        {
            Container.BindInterfacesAndSelfTo<FireController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<AutoFireWhenIsReadyToFireRule>().AsSingle().NonLazy();
            
            Container.Bind<List<SpecialSettings>>().FromInstance(_enemySetting.Specials).WhenInjectedInto<AutoFireWhenIsReadyToFireRule>();
            //Container.Bind<bool>().FromInstance(true).WhenInjectedInto<FireController>();
            Container.Bind<IGameEntity>().FromInstance(_enemy).WhenInjectedInto<FireController>();
        }

        private void InstallAiming()
        {
            Container.BindInterfacesAndSelfTo<AutoAimController>().AsSingle().NonLazy();
            //Container.Bind<bool>().FromInstance(true).WhenInjectedInto<AutoAimController>();
            Container.Bind<Transform>().FromInstance(_aimOriginTransform).WhenInjectedInto<AutoAimController>();
            Container.Bind<SkeletonAnimation>().FromInstance(_skeletonAnimation).WhenInjectedInto<AutoAimController>();
            Container.Bind<float>().FromInstance(_enemySetting.AnimationPathBounds).WhenInjectedInto<AutoAimController>();
        }
        private void InstallMovement()
        {
            Container.BindInterfacesAndSelfTo<InstantMoveController>().AsSingle().NonLazy();
            Container.Bind<Transform>().FromInstance(_enemyTransform).WhenInjectedInto<InstantMoveController>();
        }

        private void InstallHealth()
        {
            Container.BindInterfacesAndSelfTo<IsNotActiveWhenDeadRule>().AsSingle().NonLazy();
            
            Container.BindInterfacesAndSelfTo<HealthController>().AsSingle().NonLazy();
            Container.Bind<float>().FromInstance(_enemySetting.HP).WhenInjectedInto<HealthController>();
            
            Container.BindInterfacesAndSelfTo<PunchController>().AsSingle().NonLazy();
            Container.Bind<Rigidbody2D>().FromInstance(_enemyRigidbody).WhenInjectedInto<PunchController>();
        }

    }
}