using System.Collections.Generic;
using System.Linq;
using Common.Settings;
using UnityEngine;
using Zenject;

namespace Entities.Enemy
{
    public class EnemiesManager
    {
        public const float MOB_DIFFICULTY = .55f;

        private readonly List<EnemySettings> _enemySettings;

        private List<EnemyFacade> _enemies = new List<EnemyFacade>();
        private Dictionary<string, EnemiesFactory> _enemyFactories;

        public EnemiesManager(Dictionary<string, EnemiesFactory>  enemyFactories, List<EnemySettings> enemySettings)
        {
            _enemyFactories = enemyFactories;
            _enemySettings = enemySettings;
        }

        public EnemyFacade InvokeEnemy(string enemyId, Vector2 position)
        {
            var factory = _enemyFactories[enemyId];
            var enemy = factory.Create(position);
            _enemies.Add(enemy);
            return enemy;
        }
        
        public void DestroyEnemy(EnemyFacade enemy)
        {
            _enemies.Remove(enemy);
            if (enemy != null)
            {
                Object.Destroy(enemy.gameObject);
            }
        }
        public void DestroyAllEnemies()
        {
            for (var i = _enemies.Count - 1; i >=0 ; i--)
            {
               DestroyEnemy(_enemies[i]);
            }
        }
    }
}