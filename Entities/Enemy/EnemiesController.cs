using System;
using System.Collections.Generic;
using System.Linq;
using Bricks;
using Common.Settings;
using Common.Signals;
using UniRx;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Entities.Enemy
{
    public class EnemiesController
    {
        private readonly EnemiesManager _enemiesManager;
        private readonly SignalBus _signalBus;
        public IObservable<string> AreFloorEnemiesInitialized => _initializationSubject;
        private ReactiveDictionary<string, ReactiveCollection<EnemyFacade>> _floorsEnemies =
            new ReactiveDictionary<string, ReactiveCollection<EnemyFacade>>();
        private Dictionary<EnemyFacade, IDisposable> _isAlvieStreams = new Dictionary<EnemyFacade, IDisposable>();
        private Subject<string> _initializationSubject = new Subject<string>();
        private CompositeDisposable _compositeDisposable = new CompositeDisposable();

        private ReadOnlyReactiveProperty<Vector2> _aimTarget;
        public EnemiesController(EnemiesManager enemiesManager, SignalBus signalBus)
        {
            _enemiesManager = enemiesManager;
            _signalBus = signalBus;
        }
        public void Initialize(string floorId, List<EnemySetupSettings> enemies, List<BalconyBrick> balconyBricks)
        {
            InitEnemiesContainer(floorId);
            var bricksCopy = balconyBricks.ToList();
            foreach (var enemySetup in enemies)
            {
                var maxEnemies = Mathf.Min(enemySetup.Count, bricksCopy.Count);
                for (int i = 0; i < maxEnemies; i++)
                {
                    var brick = bricksCopy.Last();
                    InitEnemy(floorId, _aimTarget, enemySetup, brick);
                    bricksCopy.Remove(brick);
                }
            }
            bricksCopy.ForEach(excessiveBlock=>excessiveBlock.TurnIntoOrdinaryBlock());
            _initializationSubject.OnNext(floorId);
        }

        public void SetAimTarget(ReadOnlyReactiveProperty<Vector2> aimTarget)
        {
            _aimTarget = aimTarget;
            foreach (var enemy in _isAlvieStreams.Keys)
            {
                enemy.AttachAimTarget(aimTarget);
            }
        }
        private void InitEnemy(string floorId, ReadOnlyReactiveProperty<Vector2> aimTarget, EnemySetupSettings enemySetup, BalconyBrick brick)
        {
            var enemy = _enemiesManager.InvokeEnemy(enemySetup.Id, brick.EnemySpawnPosition);
            enemy.AttachAimTarget(aimTarget);
            brick.AttachEnemyToBalcony(enemy);
            
            _floorsEnemies[floorId].Add(enemy);
            _isAlvieStreams.Add(enemy, enemy.CurrentHp.Subscribe(_=>IsAliveCheck(floorId, enemy)));
        }

        private void IsAliveCheck(string floorId, EnemyFacade enemy)
        {
            if (!enemy.IsAlive.Value)
            {
                DestroyEnemy(floorId, enemy, true);
            }
        }
        public IReadOnlyReactiveCollection<EnemyFacade> GetAliveEnemies(string floorId)
        {
            return _floorsEnemies.ContainsKey(floorId) ? _floorsEnemies[floorId] : null;
        }
        private void DestroyEnemy(string floorId, EnemyFacade enemy, bool animate)
        {
            _isAlvieStreams.TryGetValue(enemy, out var disposable);
            disposable?.Dispose();
            _isAlvieStreams.Remove(enemy);
            _floorsEnemies[floorId].Remove(enemy);
            _signalBus.Fire(new GameSignals.EnemyKilled() { FloorId = floorId, Enemy = enemy });

            if (animate)
            {
                enemy.PunchToDeath(250f);
                Observable.Timer(TimeSpan.FromSeconds(5f)).Subscribe(_ =>
                {
                    _enemiesManager.DestroyEnemy(enemy);
                }).AddTo(_compositeDisposable);
            }
            else
            {
                _enemiesManager.DestroyEnemy(enemy);
            }
        }

        private void InitEnemiesContainer(string floorId)
        {
            if (!_floorsEnemies.ContainsKey(floorId))
            {
                _floorsEnemies.Add(floorId, new ReactiveCollection<EnemyFacade>());   
            }
        }
        public bool IsInitialized(string floorId)
        {
            return _floorsEnemies.ContainsKey(floorId);
        }
        public void DestroyAllEnemies(string floorId, bool animate = false)
        {
            for (int i = _floorsEnemies[floorId].Count - 1; i >= 0; i--)
            {
                DestroyEnemy(floorId, _floorsEnemies[floorId][i], animate);
            }
        }
        public void Dispose()
        {
            foreach (var floor in _floorsEnemies)
            {
                DestroyAllEnemies(floor.Key);   
            }
            _floorsEnemies?.Dispose();
            _initializationSubject?.Dispose();
            _compositeDisposable?.Dispose();
        }

        public void ResumeEnemiesActivity(string floorId)
        {
            foreach (var enemyFacade in _floorsEnemies[floorId])
            {
                enemyFacade.Resume();
            }
        }

        public void PauseEnemiesActivity(string floorId)
        {
            foreach (var enemyFacade in _floorsEnemies[floorId])
            {
                enemyFacade.Pause();
            }
        }
    }
}