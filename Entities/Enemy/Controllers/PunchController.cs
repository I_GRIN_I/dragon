﻿using Common.Signals;
using UnityEngine;
using Zenject;

namespace Entities.Enemy.Controllers
{
    public class PunchController
    {
        private readonly Rigidbody2D _rigidbody;
        private readonly SignalBus _signalBus;
        private bool _punched = false;

        public PunchController(Rigidbody2D rigidbody, SignalBus signalBus)
        {
            _rigidbody = rigidbody;
            _signalBus = signalBus;
        }

        public void PunchPhysics(float force = 50f)
        {
            if (_punched) return;
            var randomed = Random.Range(0, 100);
            if (randomed <= 2)
                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "scream" });
            else if(randomed <= 70)
            {
                randomed = Random.Range(0, 2);
                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = $"fall{randomed}" });
            }
            _rigidbody.bodyType = RigidbodyType2D.Dynamic;
            _rigidbody.AddForce(force * (Vector2.up + Vector2.right));
            _rigidbody.AddTorque(-10f);
            _punched = true;
        }
    }
}