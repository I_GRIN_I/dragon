using System;
using UniRx;
using UnityEngine;
using Zenject;

namespace Entities.Enemy.Controllers
{
    public class InstantMoveController : IMoveController, IFixedTickable
    {
        private readonly Transform _transform;

        public ReadOnlyReactiveProperty<Vector2> CurrentWorldPosition => _currentWorldPosition.ToReadOnlyReactiveProperty();
        private ReactiveProperty<Vector2> _currentWorldPosition = new ReactiveProperty<Vector2>();
        public ReadOnlyReactiveProperty<bool> IsAbleToMove => _isAbleToMove.ToReadOnlyReactiveProperty();
        public ReactiveProperty<bool> _isAbleToMove = new ReactiveProperty<bool>();
        public float Speed { get; private set; }

        public InstantMoveController(Transform transform)
        {
            _transform = transform;
        }

        public void MoveToWorldPosition(Vector2 targetPosition)
        {
            _transform.position = targetPosition;
        }

        public void SetSpeed(float speed)
        {
            Speed = speed;
        }

        public void SetIsAbleToMove(bool isAble)
        {
            _isAbleToMove.Value = isAble;
        }

        public void FixedTick()
        {
            _currentWorldPosition.Value = (Vector2) _transform.position;
        }
    }
}