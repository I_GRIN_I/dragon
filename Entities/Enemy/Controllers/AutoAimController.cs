using Entities.Player;
using Spine.Unity;
using UniRx;
using UnityEngine;
using Zenject;

namespace Entities.Enemy.Controllers
{
    public class AutoAimController : IAimController, IFixedTickable
    {

        public ReadOnlyReactiveProperty<bool> IsAbleToAim => _isAbleToAim.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<bool> IsAiming => _isAbleToAim.Where(isAble=>isAble && _targetWorldPosition != null).ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<Vector2> TargetWorldPosition => _targetWorldPosition;
        public ReadOnlyReactiveProperty<Vector2> OriginWorldPosition =>
            _originWorldPosition.ToReadOnlyReactiveProperty();

        private const int _levelHeight = 14;
        private ReactiveProperty<bool> _isAbleToAim = new ReactiveProperty<bool>();
        private ReactiveProperty<Vector2> _originWorldPosition = new ReactiveProperty<Vector2>();
        private ReadOnlyReactiveProperty<Vector2> _targetWorldPosition;
        private readonly Transform _aimOrigin;
        private readonly SkeletonAnimation _skeletonAnimation;
        private readonly float _animationPathBound;

        public AutoAimController(Transform aimOrigin, SkeletonAnimation skeletonAnimation, float animationPathBound, [InjectOptional] bool isAbleToAimByDefault)
        {
            if (isAbleToAimByDefault)
                _isAbleToAim.Value = true;
            _aimOrigin = aimOrigin;
            _skeletonAnimation = skeletonAnimation;
            _animationPathBound = animationPathBound;
        }
        public void SetIsAbleToAim(bool isAble)
        {
            _isAbleToAim.Value = isAble;
        }

        public void SetTargetWorldPoint(ReadOnlyReactiveProperty<Vector2> targetWorldPos)
        {
            _targetWorldPosition = targetWorldPos;
        }

        public void FixedTick()
        {
            _originWorldPosition.Value = _aimOrigin.position;
            if (_skeletonAnimation)
            {
                var finalPosition = Mathf.Clamp(Mathf.Clamp(OriginWorldPosition.Value.y - TargetWorldPosition.Value.y, 0, _levelHeight) / _levelHeight * _animationPathBound, 0, _animationPathBound);
                if(!finalPosition.Equals(_skeletonAnimation.skeleton.FindPathConstraint("Bow_move_path_PK").Position))
                    _skeletonAnimation.skeleton.FindPathConstraint("Bow_move_path_PK").Position = finalPosition;
            }
        }
    }
}