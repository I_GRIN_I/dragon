using Common.Data;
using Entities;
using UniRx;
using UnityEngine;

namespace Common
{
    public interface IGameEntity : IHealthController
    {
        EntityType EntityType { get; }
        ReadOnlyReactiveProperty<Vector2> WorldPosition { get; }
    }
}