namespace Entities
{
    /// <summary>
    /// TODO: Need for resetting class's states before next level
    /// </summary>
    public interface IResettable
    {
        void Reset();
    }
}