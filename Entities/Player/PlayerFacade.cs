using Common;
using Common.Data;
using Common.Settings;
using Entities.Player.Controllers;
using UniRx;
using UnityEngine;
using Upgrades;
using Zenject;

namespace Entities.Player
{
    public class PlayerFacade : MonoBehaviour, IGameEntity
    {
        private MoveByTweenController _moveController;
        private IAimController _aimController;
        private FireController _fireController;
        private IHealthController _healthController;
        private StunController _stunController;
        private PlayerFacadeContextWrapper _wrapper;

        public EntityType EntityType => EntityType.Player;

        public ReadOnlyReactiveProperty<float> CurrentHp => _healthController.CurrentHp;
        public ReadOnlyReactiveProperty<float> MaxHp => _healthController.MaxHp;
        public ReadOnlyReactiveProperty<bool> IsAlive => _healthController.IsAlive;
        public float Speed => _moveController.Speed;
        public ReadOnlyReactiveProperty<bool> IsAbleToMove => _moveController.IsAbleToMove;
        public ReadOnlyReactiveProperty<Vector2> WorldPosition => _moveController.CurrentWorldPosition;
        public ReadOnlyReactiveProperty<bool> IsAbleToAim => _aimController.IsAbleToAim;
        public ReadOnlyReactiveProperty<bool> IsAbleToFire => _fireController.IsAbleToFire;
        public ReadOnlyReactiveProperty<bool> IsReadyToFire => _fireController.IsReadyToFire;
        public ReadOnlyReactiveProperty<float> FireCooldown => _fireController.FireCooldown;
        public ReadOnlyReactiveProperty<bool> IsStunned => _stunController.IsStunned;
        public ReadOnlyReactiveProperty<bool> IsInMove => _moveController.IsInMove;
        public ReadOnlyReactiveProperty<bool> IsInputMoveEnabled => _isInputMoveEnabled.ToReadOnlyReactiveProperty();
        private ReactiveProperty<bool> _isInputMoveEnabled = new ReactiveProperty<bool>();
        private PlayerSettings _playerSettings;
        private UpgradesController _upgradesController;

        public bool IsInitialized { get; private set; }
        public bool IsOnPause { get; private set; }
        public float FireRate => _fireController.FireRate;

        public ReadOnlyReactiveProperty<bool> IsImmuneToDamage => _healthController.IsImmuneToDamage;

        [Inject]
        public void Construct(MoveByTweenController moveController, IAimController aimController,
            FireController fireController, IHealthController healthController, StunController stunController,
            PlayerFacadeContextWrapper wrapper, PlayerSettings playerSettings, UpgradesController upgradesController)
        {
            _upgradesController = upgradesController;
            _playerSettings = playerSettings;
            _wrapper = wrapper;
            _stunController = stunController;
            _healthController = healthController;
            _moveController = moveController;
            _aimController = aimController;
            _fireController = fireController;
        }
        public void Awake()
        {
            _wrapper.Initialize(this);
        }

        public void Initialize()
        {
            SetHealth(_upgradesController.GetUpgradedValue(UpgradeType.HP));
            _moveController.SetIsAbleToMove(true);
            IsInitialized = true;
            IsImmuneToDamage.Subscribe(x => {
                foreach (var collider in GetComponentsInChildren<Collider2D>())
                {
                    collider.enabled = !x;
                }
            });
        }
        private void OnDestroy() => _wrapper.Dispose();

        private void SetHealth(float hp)
        {
            _healthController.SetMaxHp(hp);
            _healthController.SetCurrentHp(hp);
        }

        public void Pause()
        {
            SetIsInputMoveEnabled(false);
            _aimController.SetIsAbleToAim(false);
            _fireController.SetIsAbleToFire(false);
            _moveController.StopMovement();
            IsOnPause = true;
        }

        public void Resume()
        {
            SetIsInputMoveEnabled(true);
            if (IsAlive.Value)
            {
                _aimController.SetIsAbleToAim(true);
                _fireController.SetIsAbleToFire(true);
                _moveController.TryUpdateMovement();
            }
            IsOnPause = false;
        }

        public void SetMaxHp(float newHp) => _healthController.SetMaxHp(newHp);
        public void TakeDamage(float damageHp) => _healthController.TakeDamage(damageHp);
        public void Heal(float healHp) => _healthController.Heal(healHp);
        public void SetIsInputMoveEnabled(bool isAble) => _isInputMoveEnabled.Value = isAble;
        public void MovePlayerToWorldPosition(Vector3 targetPos) => _moveController.MoveToWorldPosition(targetPos);
        public void MovePlayerToWorldPosition(Vector3 targetPos, float duration) => _moveController.MoveToWorldPosition(targetPos, duration);
        public void MovePlayerToStart(float duration = 0f) =>
            _moveController.MoveToWorldPosition(transform.parent.TransformPoint(Vector3.zero), duration);
        public void Stun(float stunDuration) => _stunController.Stun(stunDuration);

        public void SetDamageImmunity(bool isImmune) => _healthController.SetDamageImmunity(isImmune);

        public void SetCurrentHp(float newHp) => _healthController.SetCurrentHp(newHp);
    }
}