namespace Entities.Player.Data
{
    public enum TransformType
    {
        Player,
        PlayerAnchor,
        TowerAnchor
    }
    
    public enum RectTransformType
    {
        AimArea,
        MoveArea,
        FlyArea,
        UICoin
    }
}