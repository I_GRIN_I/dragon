using System;
using Entities.Player.Data;
using PlayerInput;
using UniRx;
using UnityEngine;
using Zenject;

namespace Entities.Player.Controllers
{
    public class PlayerAimController : IInitializable, IDisposable, IFixedTickable, IAimController
    {
        private readonly IPlayerInput _playerInput;
        private readonly Camera _camera;
        private readonly Transform _aimOrigin;
        public ReadOnlyReactiveProperty<bool> IsAbleToAim => _isAbleToAim.ToReadOnlyReactiveProperty();
        private ReactiveProperty<bool> _isAbleToAim = new ReactiveProperty<bool>();
        public ReadOnlyReactiveProperty<bool> IsAiming => _isAiming.ToReadOnlyReactiveProperty();
        private ReactiveProperty<bool> _isAiming = new ReactiveProperty<bool>();
        
        public ReadOnlyReactiveProperty<Vector2> TargetWorldPosition => _targetWorldPoint.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<Vector2> OriginWorldPosition  =>
            _originWorldPosition.ToReadOnlyReactiveProperty();
        private ReactiveProperty<Vector2> _targetWorldPoint = new ReactiveProperty<Vector2>();
        public ReactiveProperty<Vector2> _originWorldPosition = new ReactiveProperty<Vector2>();

        [Inject(Id = RectTransformType.AimArea)]
        private RectTransform _aimTransform;
        private IDisposable _inputPosStream;

        public PlayerAimController(IPlayerInput playerInput, Camera camera, Transform aimOrigin, [InjectOptional] bool isAbleToAimByDefault)
        {
            if (isAbleToAimByDefault)
                _isAbleToAim.Value = true;
            
            _playerInput = playerInput;
            _camera = camera;
            _aimOrigin = aimOrigin;
        }
        public void Initialize()
        {
            _inputPosStream = _playerInput.ScreenLastInputPosition
                .Where(_ => _playerInput.IsInputActive.Value).AsUnitObservable()
                .Merge(_playerInput.IsInputActive.AsUnitObservable())
                .Merge(_isAbleToAim.AsUnitObservable())
                .Subscribe(_ => UpdateAim(_playerInput.ScreenLastInputPosition.Value));
            _playerInput.IsInputActive.Where(isActive => !isActive).Subscribe(_ => _isAiming.Value = false);
        }

        private void UpdateAim(Vector3 screenInput)
        {
            if (!_playerInput.IsInputActive.Value) return;
            var worldPos = _camera.ScreenToWorldPoint(screenInput);
            _isAiming.Value = _isAbleToAim.Value && RectTransformUtility.RectangleContainsScreenPoint(_aimTransform, screenInput);
            if (_isAiming.Value)
            {
                _targetWorldPoint.Value = Vector3.right * worldPos.x + Vector3.up * worldPos.y;
            }
        }

        public void SetIsAbleToAim(bool isAble)
        {
            _isAbleToAim.Value = isAble;
        }
        public void Dispose()
        {
            _inputPosStream?.Dispose();
        }

        public void FixedTick()
        {
            _originWorldPosition.Value = _aimOrigin.position;
        }
    }
}