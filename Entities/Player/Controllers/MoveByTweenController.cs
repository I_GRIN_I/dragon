using System;
using Common.Data;
using Common.Settings;
using Common.Signals;
using DG.Tweening;
using UniRx;
using UnityEngine;
using Upgrades;
using Zenject;

namespace Entities.Player.Controllers
{
    public class MoveByTweenController : IMoveController, IInitializable, IDisposable
    {
        public ReadOnlyReactiveProperty<Vector2> CurrentWorldPosition => _currentWorldPos.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<bool> IsAbleToMove => _isAbleToMove
            .Merge(_stunController.IsStunned.AsObservable())
            .Select(_ => _isAbleToMove.Value && !_stunController.IsStunned.Value)
            .ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<bool> IsInMove => _isInMove.ToReadOnlyReactiveProperty();
        public float Speed { get; private set; }
        private PlayerSettings _playerSettings;
        private UpgradesController _upgradesController;
        private readonly StunController _stunController;
        private Rigidbody2D _playerRigidbody;
        private SignalBus _signalBus;
        private Ease _easeFunc;
        private ReactiveProperty<Vector2> _currentWorldPos = new ReactiveProperty<Vector2>();
        private ReactiveProperty<bool> _isAbleToMove = new ReactiveProperty<bool>();

        private Vector2 _lastTargetPosition;
        private ReactiveProperty<bool> _isInMove = new ReactiveProperty<bool>();
        private Tween _moveTween;
        private float _blockSizeVertical;
        private IDisposable _isAbleToMoveStream;

        public Vector2 CurrentVelocity { get; private set; }

        public MoveByTweenController(SignalBus signalBus, Rigidbody2D playerRigidbody, PlayerSettings playerSettings,
            StunController stunController, UpgradesController upgradesController, [InjectOptional] bool isAble,
            [InjectOptional] Ease easeFunc = Ease.OutBack)
        {
            _signalBus = signalBus;
            _playerRigidbody = playerRigidbody;
            _playerSettings = playerSettings;
            _stunController = stunController;
            _easeFunc = easeFunc;
            _isAbleToMove.Value = isAble;
            _upgradesController = upgradesController;
        }
        public void Initialize()
        {
            UpdateMoveInfo();
            SetSpeed(_upgradesController.GetUpgradedValue(UpgradeType.Speed));
            _blockSizeVertical = _playerSettings.BaseBlock.localScale.y;
            _isAbleToMoveStream = IsAbleToMove.Subscribe(isAble =>
            {
                if (isAble)
                    TryUpdateMovement();
                else
                    StopMovement();
            });
            _signalBus.Subscribe<UpgradesSignals.UpgradeUpdated>(signal =>
            {
                if (signal.UpgradeType == UpgradeType.Speed)
                    SetSpeed(_upgradesController.GetUpgradedValue(UpgradeType.Speed));
            });
        }
        public void MoveToLocalPosition(Vector2 targetPos, float duration)
        {
            
        }
        public void MoveToWorldPosition(Vector2 targetPos, float duration)
        {
            _lastTargetPosition = targetPos;
            
            if (!IsAbleToMove.Value)
                return;
            
            if (_moveTween.IsActive())
                _moveTween.Kill();
            if (duration <= float.Epsilon)
            {
                _playerRigidbody.MovePosition(targetPos);
            }
            else
            {
                _moveTween = _playerRigidbody.DOMove(targetPos, duration).SetEase(_easeFunc)
                    .OnStart(()=> _isInMove.SetValueAndForceNotify(true))
                    .OnUpdate(UpdateMoveInfo)
                    .OnComplete(() => _isInMove.SetValueAndForceNotify(false)).SetAutoKill();   
            }
        }
        public void MoveToWorldPosition(Vector2 targetPosition)
        {
            var distance = Vector2.Distance(targetPosition, _playerRigidbody.position);
            MoveToWorldPosition(targetPosition,distance / _blockSizeVertical / Speed);
        }

        private void UpdateMoveInfo()
        {
            _currentWorldPos.Value = _playerRigidbody.position;
            CurrentVelocity = _playerRigidbody.velocity;
        }

        public void SetSpeed(float speed)
        {
            Speed = speed;
            //if speed changed, but movement hasn't finished yet we should recalculate our tween
            TryUpdateMovement();
        }

        public void TryUpdateMovement()
        {
            if (_moveTween.IsActive() && _isInMove.Value)
            {
                MoveToWorldPosition(_lastTargetPosition);
            }
        }
        public void StopMovement()
        {
            if (_moveTween.IsActive())
            {
                _moveTween.Pause();
            }
        }
        public void SetIsAbleToMove(bool isAble)
        {
            _isAbleToMove.Value = isAble;
        }

        public void SetEasing(Ease easing)
        {
            _easeFunc = easing;
            TryUpdateMovement();
        }

        public void Dispose()
        {
            if (_moveTween.IsActive())
                _moveTween.Kill();
            _isAbleToMove?.Dispose();
            _isAbleToMoveStream?.Dispose();
        }
    }
}