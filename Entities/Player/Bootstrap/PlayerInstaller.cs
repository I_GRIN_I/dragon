using Common;
using DG.Tweening;
using Entities.Player.Controllers;
using Entities.Player.Data;
using UnityEngine;
using Zenject;

namespace Entities.Player.Bootstrap
{
    public class PlayerInstaller : MonoInstaller
    {
        [Header("Settings")]
        [SerializeField] private Ease _moveEase;
        [SerializeField] private Rigidbody2D _playerRigidbody;
        [SerializeField] private Transform _aimOriginTransform;
        [SerializeField] private ParticleSystem _healParticle;
        [SerializeField] private ParticleSystem _stunParticle;

        public override void InstallBindings()
        {
            InstallMovement();
            InstallFiring();
            InstallAiming();
            InstallHealth();
        }

        private void InstallFiring()
        {
            Container.BindInterfacesAndSelfTo<FireWithInputRule>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<FireController>().AsSingle().NonLazy();
            //Container.Bind<bool>().FromInstance(true).WhenInjectedInto<FireController>();
            Container.Bind<IGameEntity>().FromResolveGetter<PlayerFacade>(player=> player as IGameEntity).WhenInjectedInto<FireController>();
        }

        private void InstallAiming()
        {
            Container.BindInterfacesAndSelfTo<PlayerAimController>().AsSingle().NonLazy();
            //Container.Bind<bool>().FromInstance(true).WhenInjectedInto<PlayerAimController>();
            Container.Bind<Transform>().FromInstance(_aimOriginTransform).WhenInjectedInto<PlayerAimController>();
        }

        private void InstallMovement()
        {
            Container.Bind<ParticleSystem>().WithId(ParticleSystemType.Stun).FromInstance(_stunParticle);

            Container.BindInterfacesAndSelfTo<MovePlayerVerticalWithInputRule>().AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<MoveByTweenController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<StunController>().AsSingle().NonLazy();

            Container.Bind<Ease>().FromInstance(_moveEase).WhenInjectedInto<MoveByTweenController>();
            Container.Bind<Rigidbody2D>().FromInstance(_playerRigidbody).WhenInjectedInto<MoveByTweenController>();
            //Container.Bind<bool>().FromInstance(true).WhenInjectedInto<MoveByTweenController>();
        }

        private void InstallHealth()
        {
            Container.Bind<ParticleSystem>().WithId(ParticleSystemType.Heal).FromInstance(_healParticle);

            Container.BindInterfacesAndSelfTo<IsNotActiveWhenDeadRule>().AsSingle().NonLazy();
            
            Container.BindInterfacesAndSelfTo<HealthController>().AsSingle().NonLazy();
            //Container.Bind<float>().FromInstance(6).WhenInjectedInto<HealthController>();
        }
    }
}