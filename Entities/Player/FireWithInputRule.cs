using System;
using Common.Data;
using Common.Settings;
using Common.Signals;
using PlayerInput;
using UniRx;
using Upgrades;
using Zenject;

namespace Entities.Player
{
    public class FireWithInputRule : IInitializable, IDisposable
    {
        private readonly FireController _fireController;
        private readonly IAimController _aimController;
        private readonly IPlayerInput _playerInput;
        private readonly PlayerSettings _playerSettings;
        private readonly UpgradesController _upgradesController;
        private readonly SignalBus _signalBus;
        private IDisposable _fireStream;

        public FireWithInputRule(FireController fireController, IAimController aimController, IPlayerInput playerInput,
            PlayerSettings playerSettings, UpgradesController upgradesController, SignalBus signalBus)
        {
            _fireController = fireController;
            _aimController = aimController;
            _playerInput = playerInput;
            _playerSettings = playerSettings;
            _upgradesController = upgradesController;
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            RechargePlayersWeapon();
            _fireStream = _playerInput.IsInputActive.Where(isActive =>
                !isActive && _aimController.IsAiming.Value && _fireController.IsReadyToFire.Value)
                .Subscribe(_ =>
            {
                _fireController.Fire(_aimController.OriginWorldPosition.Value, _aimController.TargetWorldPosition.Value);
            });
            _signalBus.Subscribe<UpgradesSignals.UpgradeUpdated>(OnUpgradeBought);
        }

        private void OnUpgradeBought(UpgradesSignals.UpgradeUpdated signal)
        {
            if (signal.UpgradeType.Equals(UpgradeType.Damage) || signal.UpgradeType.Equals(UpgradeType.ChargeTime))
            {
                RechargePlayersWeapon();
            }
        }

        private void RechargePlayersWeapon()
        {
            var special = new SpecialSettings(_playerSettings.SpecialSettings);
            special.Damage = _upgradesController.GetUpgradedValue(UpgradeType.Damage);
            special.FireChargeTime = _upgradesController.GetUpgradedValue(UpgradeType.ChargeTime);
            _fireController.ChargeProjectile(special);
        }

        public void Dispose()
        {
            _fireStream?.Dispose();
        }
    }
}
