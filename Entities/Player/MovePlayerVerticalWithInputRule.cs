using System;
using Common.Settings;
using Entities.Player.Data;
using PlayerInput;
using UniRx;
using UnityEngine;
using Utils;
using Zenject;

namespace Entities.Player
{
    public class MovePlayerVerticalWithInputRule : IInitializable, IDisposable
    {
        private readonly IPlayerInput _playerInput;
        private readonly Camera _camera;
        private readonly PlayerFacade _playerFacade;

        [Inject(Id = TransformType.PlayerAnchor)]
        private readonly Transform _anchorTransform;
        [Inject(Id = RectTransformType.MoveArea)]
        private RectTransform _moveTransform;
        [Inject(Id = RectTransformType.FlyArea)]
        private RectTransform _flyTransform;
        private readonly IMoveController _moveController;
        private IDisposable _inputPosStream;

        public MovePlayerVerticalWithInputRule(IPlayerInput playerInput, IMoveController moveController, Camera camera, PlayerFacade playerFacade)
        {
            _playerInput = playerInput;
            _moveController = moveController;
            _camera = camera;
            _playerFacade = playerFacade;
        }


        public void Initialize()
        {
            
            _inputPosStream = _playerInput.ScreenLastInputPosition
                .Where(_ => _playerInput.IsInputActive.Value && _playerFacade.IsInputMoveEnabled.Value &&
                            RectTransformUtility.RectangleContainsScreenPoint(_moveTransform,
                                _playerInput.ScreenLastInputPosition.Value))
                .Subscribe(lastScreenPos => MovePlayerWithInput(lastScreenPos));
        }

        private void MovePlayerWithInput(Vector3 lastScreenPos)
        {
            var targetVerticalWorldPos =
                GameHelper.ProjectOnVectorScreenToWorldPoint(lastScreenPos, _camera);
            if (!RectTransformUtility.RectangleContainsScreenPoint(_flyTransform,
                                lastScreenPos))
            {
                var corners = new Vector3[4];
                _flyTransform.GetWorldCorners(corners);
                Vector3 topLeft = GameHelper.ProjectOnVectorScreenToWorldPoint(corners[1], _camera),
                    rightBottom = GameHelper.ProjectOnVectorScreenToWorldPoint(corners[3], _camera);
                targetVerticalWorldPos = new Vector3(
                    Mathf.Clamp(targetVerticalWorldPos.x, topLeft.x, rightBottom.x),
                    Mathf.Clamp(targetVerticalWorldPos.y, rightBottom.y, topLeft.y));
            }
            _moveController.MoveToWorldPosition(targetVerticalWorldPos);
        }

        public void Dispose()
        {
            _inputPosStream.Dispose();
        }
    }
}