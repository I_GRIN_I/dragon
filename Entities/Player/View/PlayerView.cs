using System;
using Common.Signals;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UniRx;
using UnityEngine;
using Zenject;

namespace Entities.Player.View
{
    public class PlayerView : MonoBehaviour
    {
        [SerializeField] private Material _playerMaterial;
        private PlayerFacade _player;
        
        private SignalBus _signalBus;
        private static readonly int OutlineWidth = Shader.PropertyToID(OUTLINE_PROPERTY);
        private static readonly int Fill = Shader.PropertyToID(FILL_PROPERTY);
        private static readonly int FillColor = Shader.PropertyToID(FILL_COLOR);
        private Tween _immunityTween;
        private Tween _immunityColorTween;
        private Tween _hpTween;

        private IDisposable _immunityColorTimer;
        private readonly Vector4 _normalFillColor = new Vector4(1, 0, 0, 1);
        
        private const string OUTLINE_PROPERTY = "_ThresholdEnd";
        private const string FILL_PROPERTY = "_FillPhase";
        private const string FILL_COLOR = "_FillColor";
        private float _lastHp;


        [Inject]
        private void Construct(PlayerFacade player, SignalBus signalBus)
        {
            _player = player;
            _signalBus = signalBus;
        }

        private void Start()
        {
            _player.IsImmuneToDamage.Subscribe(isImmune => UpdateImmunityView(isImmune)).AddTo(this);
            _player.CurrentHp.Subscribe(hp => UpdatedCurrentHp(hp)).AddTo(this);
            _lastHp = _player.CurrentHp.Value;
            _playerMaterial.SetFloat(Fill, 0);
            _playerMaterial.SetVector(FillColor, _normalFillColor);
            Observable.Timer(TimeSpan.FromSeconds(.65f))
                .Repeat()
                .Where(x => _player.IsInMove.Value)
                .Subscribe(_ =>
                {
                    _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "fly", Volume = .02f });
                }).AddTo(this);
        }

        private void UpdatedCurrentHp(float hp)
        {
            if (_lastHp > hp)
            {
                if (_hpTween.IsActive())
                {
                    _hpTween.Kill(true);
                }
                _hpTween = DOTween.Sequence()
                    .Append(_playerMaterial.DOFloat(0.9f, Fill, 0.3f))
                    .Append(_playerMaterial.DOFloat(0f, Fill, 0.3f));
                transform.DOShakePosition(.5f, .5f, 15);
            }
            _lastHp = hp;
        }

        private void UpdateImmunityView(bool isImmune)
        {
            var targetValue = isImmune ? 0 : 1;
            if (_immunityTween.IsActive())
            {
                _immunityTween.Kill(true);
            }
            _immunityTween = _playerMaterial.DOFloat(targetValue, OutlineWidth, 0.5f).From(1 - targetValue);
            if (isImmune)
            {
                ImmunityPulseAnimation();
                _immunityColorTimer = Observable.Timer(TimeSpan.FromSeconds(.63f))
                    .Repeat()
                    .Subscribe(ImmunityPulseAnimation)
                    .AddTo(this);
            }
            else
            {
                _immunityColorTimer?.Dispose();
                _playerMaterial.DOVector(_normalFillColor, FillColor, 0.3f);
                Observable.Timer(TimeSpan.FromSeconds(.6f))
                    .Subscribe(_ => _playerMaterial.SetVector(FillColor, _normalFillColor))
                    .AddTo(this);
            }
        }

        private void ImmunityPulseAnimation(long obj = 0)
        {
            if (_immunityColorTween.IsActive())
            {
                _immunityTween.Kill(true);
            }
            _immunityColorTween = DOTween.Sequence()
                   .Append(_playerMaterial.DOVector(_normalFillColor, FillColor, 0.3f))
                   .Append(_playerMaterial.DOVector(new Vector4(1, 0, 0, 0.5f), FillColor, 0.3f));
        }
    }
}
