using System;
using System.Collections.Generic;
using System.Linq;
using Common.Settings;
using UniRx;
using UnityEngine;
using Zenject;

namespace Entities.Player.View
{
    public class AimLineView : MonoBehaviour, IInitializable
    {
        [SerializeField] private LayerMask _sphereCastMask;
        [SerializeField] private LineRenderer _lineRenderer;
        [SerializeField] private Color _readyColor;
        [SerializeField] private Color _notReadyColor;
        private IAimController _aimController;
        private GeneralSettings _generalSettings;
        private FireController _fireController;
        private PlayerSettings _playerSettings;
        private float _ballRadius;

        [Inject]
        protected void Construct(IAimController aimController, FireController fireController, GeneralSettings generalSettings, PlayerSettings playerSettings)
        {
            _playerSettings = playerSettings;
            _fireController = fireController;
            _aimController = aimController;
            _generalSettings = generalSettings;
        }

        public void Initialize()
        {
            var fireball = _playerSettings.SpecialSettings.Projectile;
            var f_collider = fireball.GetComponent<CircleCollider2D>();
            var f_ransform = fireball.GetComponent<Transform>();
            if (f_collider != null && f_ransform != null)
            {
                _ballRadius = f_collider.radius * f_ransform.localScale.x;
            }
            
            _aimController.IsAiming.AsUnitObservable()
                .Merge(_aimController.TargetWorldPosition.AsUnitObservable())
                .Merge(_aimController.OriginWorldPosition.AsUnitObservable())
                .Where(_=>_aimController.IsAiming.Value)
                .Subscribe(_=> UpdateLinePos()).AddTo(this);

            _aimController.IsAiming
                .Where(isAiming=>isAiming)
                .Throttle(TimeSpan.FromSeconds(_generalSettings.AimLineHoldTime))
                .Merge(_aimController.IsAiming.Where(isAiming=>!isAiming))
                .Subscribe(_=>UpdateViewVisibility())
                .AddTo(this);

            _fireController.FireCooldownRelative.Subscribe(UpdateLineColor).AddTo(this);
            _lineRenderer.useWorldSpace = true;
        }

        public void UpdateViewVisibility()
        {
            _lineRenderer.enabled = _aimController.IsAiming.Value;
        }

        private void UpdateLinePos()
        {
            var startPos = _aimController.OriginWorldPosition.Value;
                var endPos = _aimController.TargetWorldPosition.Value;
                var direction = endPos - startPos;
                direction.Normalize();
                var positions = new List<Vector3>();
                positions.Add(startPos);

                var filter = new ContactFilter2D();
                filter.SetLayerMask(_sphereCastMask);
                List<RaycastHit2D> hits = new List<RaycastHit2D>();
                for (int i = 0; i < _generalSettings.AimLineReflections; i++)
                {
                    hits.Clear();
                    
                    var hitsCount = Physics2D.CircleCast(startPos, _ballRadius, direction, filter, hits);
                    if (hitsCount > 0)
                    {
                        var closestHit = hits.First();
                        positions.Add(closestHit.point);

                        direction = Vector2.Reflect(direction, closestHit.normal);
                        startPos = closestHit.point + closestHit.normal * _ballRadius;
                    }
                    else
                    {
                        if (i == 0)
                            positions.Add(endPos);
                        else
                            positions.Add(startPos + direction);
                        break;
                    }
                }

                _lineRenderer.positionCount = positions.Count;
                _lineRenderer.SetPositions(positions.ToArray());
        }

        private void UpdateLineColor(float progress)
        {
            _lineRenderer.endColor = _fireController.IsReadyToFire.Value
                ? new Color(_readyColor.r, _readyColor.g, _readyColor.b, 0)
                : new Color(_notReadyColor.r, _notReadyColor.g, _notReadyColor.b, 0);
            _lineRenderer.startColor = _fireController.IsReadyToFire.Value ? _readyColor : _notReadyColor;
        }
    }
}