using System;
using Common.Data;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Effects
{
    public class JumpCoinEffectView : BaseEffectView<JumpCoinEffectView.Data>
    {
        public class Data
        {
            public Vector3 EndFlyPoint;
            public Vector3 JumpDirection;
            public float DurationDispersion;
        }

        public override EffectType EffectType => EffectType.JumpCoin;
        [SerializeField] private Transform _transform;
        [SerializeField] private SpriteRenderer _renderer;
        [SerializeField] private Animation _animation;
        [SerializeField] private Vector2 _jumpDirection = Vector2.up;
        [SerializeField, Range(0,1f)] private float _disappearInterval;

        public override void StartEffect(float duration, Action<BaseEffectView> onComplete)
        {
            DOTween.Sequence().Prepend(_transform.DOJump(_transform.position + (Vector3)_jumpDirection, 0.5f, 1, duration * _disappearInterval))
                .Join(DOTween.Sequence().Append(_transform.DOScale(1.2f, duration * _disappearInterval / 2).From(0f).SetEase(Ease.OutQuart)).Append(_transform.DOScale(1f, duration * _disappearInterval / 2).SetEase(Ease.OutQuart)))
                .OnComplete(() => onComplete(this)).SetAutoKill();
        }
        public override void StartEffect(Data data, float duration, Action<BaseEffectView> onComplete)
        {
            _animation.Play();
            var dispersion = data.DurationDispersion * (Random.value - 0.5f);
            var calculatedDuration = duration + dispersion;
            
            var startFlyPoint = _transform.position + data.JumpDirection;
            var color = _renderer.color;
            color = new Color(color.r, color.g, color.b, 1f);
            _renderer.color = color;
            var path = new[] {startFlyPoint, Vector3.Lerp(startFlyPoint, data.EndFlyPoint, 0.25f) - Vector3.up,  Vector3.Lerp(startFlyPoint, data.EndFlyPoint, 0.75f), data.EndFlyPoint};
            DOTween.Sequence()
                .Prepend(_transform.DOJump(startFlyPoint, 0.5f, 1, calculatedDuration * _disappearInterval))
                .Join(_transform.DOScale(0.5f, calculatedDuration * _disappearInterval).From(0f).SetEase(Ease.OutQuart))
                .Append(_transform.DOPath(path, calculatedDuration * (0.9f -_disappearInterval), PathType.CatmullRom, PathMode.TopDown2D))
                .Join(_transform.DOScale(0.75f, calculatedDuration * (0.9f -_disappearInterval) * 0.5f).SetEase(Ease.OutQuart))
                .Append(_renderer.DOFade(0, calculatedDuration * 0.1f))
                .OnComplete(() => onComplete(this)).SetAutoKill();
        }
    }
}
