using System;
using System.Collections.Generic;
using System.Linq;
using Common.Settings;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Effects
{
    public class EffectsManager : IInitializable
    {
        private readonly List<EffectSettings> _effectSettings;
        private Dictionary<Type, List<BaseEffectView>> _activeEffects = new Dictionary<Type, List<BaseEffectView>>();
        private Dictionary<Type, List<BaseEffectView>> _pooledEffects = new Dictionary<Type, List<BaseEffectView>>();
        
        private Dictionary<Type, Transform> _activeTransformGroups = new Dictionary<Type, Transform>();
        private Dictionary<Type, Transform> _pooledTransformGroups = new Dictionary<Type, Transform>();

        public EffectsManager(List<EffectSettings> effectSettings)
        {
            _effectSettings = effectSettings;
        }
        
        public void Initialize()
        {
            foreach (var effectSetting in _effectSettings)
            {
                var fxType = effectSetting.EffectPrefab.GetType();
                InstantiateTransformGroup(_activeTransformGroups, fxType, "Active FX Container");
                InstantiateTransformGroup(_pooledTransformGroups, fxType, "Pooled FX Container");
                _activeEffects.Add(fxType, new List<BaseEffectView>());
                _pooledEffects.Add(fxType, new List<BaseEffectView>());
                
                for (int i = 0; i < effectSetting.InitialSize; i++)
                {
                    Despawn(Instantiate(effectSetting));
                }
            }
        }

        private void InstantiateTransformGroup(Dictionary<Type, Transform> transformGroups, Type fxType, string groupName)
        {
            var transformGroup = new GameObject();
            var transform =  transformGroup.GetComponent<Transform>() ?? transformGroup.AddComponent<Transform>();
            transformGroup.name = groupName;
            transformGroups.Add(fxType, transform);
        }

        private BaseEffectView Instantiate(EffectSettings settings)
        {
            return Object.Instantiate(settings.EffectPrefab);
        }
        

        private BaseEffectView Create<TEffect>(Vector2 position)
        {
            BaseEffectView fx = null;
            var fxType = typeof(TEffect);
            var setting = _effectSettings.First(e => e.EffectPrefab.GetType().Equals(fxType));
            if (_pooledEffects[fxType].Count > 0)
            {
                fx = _pooledEffects[fxType].First();
                _pooledEffects[fxType].Remove(fx);
            }
            else if (_pooledEffects[fxType].Count + _activeEffects[fxType].Count < setting.MaxCapacity)
            {
                fx = Instantiate(setting);
            }

            if (fx != null)
            {
                _activeEffects[fxType].Add(fx);
                fx.gameObject.SetActive(true);
                fx.transform.SetParent(_activeTransformGroups[fxType]);
                fx.transform.position = position;   
            }
            return fx;
        }
        
        private void Despawn(BaseEffectView fx)
        {
            var fxType = fx.GetType();
            fx.gameObject.SetActive(false);
            fx.transform.SetParent(_pooledTransformGroups[fxType]);
            if (_activeEffects[fxType].Contains(fx))
            {
                _activeEffects[fxType].Remove(fx);
            }
            _pooledEffects[fxType].Add(fx);
        }
        
        

        public void PlayEffect<TEffect>(Vector2 worldPositionValue) where TEffect : BaseEffectView
        {
            var setting = _effectSettings.First(e => e.EffectPrefab.GetType().Equals(typeof(TEffect)));
            var effect = Create<TEffect>(worldPositionValue);
            if (effect!=null)
                effect.StartEffect(setting.EffectDuration, OnEffectComplete);
        }
        public void PlayEffect<TEffect, TEffectData>(TEffectData data, Vector2 worldPositionValue)
            where TEffect : BaseEffectView<TEffectData>
        {
            var setting = _effectSettings.First(e => e.EffectPrefab.GetType().Equals(typeof(TEffect)));
            var effect = Create<TEffect>(worldPositionValue) as TEffect;
            if (effect!=null)
                effect.StartEffect(data, setting.EffectDuration, OnEffectComplete);
        }
        private void OnEffectComplete(BaseEffectView effect)
        {
            Despawn(effect);
        }
    }
}