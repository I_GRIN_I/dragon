using System;
using Common.Data;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Effects
{
    public abstract class BaseEffectView : MonoBehaviour
    {
        public abstract EffectType EffectType { get; }

        public void Initialize(Transform parent, Vector3 position)
        {
            transform.SetParent(parent);
            transform.position = position;
        }
        public abstract void StartEffect(float duration, Action<BaseEffectView> onComplete);

        public void Dispose()
        {
            Destroy(gameObject);
        }
    }
    public abstract class BaseEffectView<TData> : BaseEffectView
    {
        public abstract void StartEffect(TData data, float duration, Action<BaseEffectView> onComplete);
    }
}
