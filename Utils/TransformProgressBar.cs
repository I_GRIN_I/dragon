using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace Utils
{
    public class TransformProgressBar : MonoBehaviour
    {
        [SerializeField] private Transform _backgroundTransform;
        [SerializeField] private Transform _fillTransform;
        [SerializeField][Range(0f, 1f)] private float _fillValue = 1f;

        public float Progress
        {
            get => _fillValue;
            set
            {
                _fillValue = Mathf.Clamp01(value);
                UpdateProgressBar();
            }
        }
        private void OnValidate()
        {
            UpdateProgressBar();
        }

        private void UpdateProgressBar()
        {
            if (_backgroundTransform == null)
            {
                UnityEngine.Debug.LogWarning("[TransformProgressBar] Background Transform is not assigned in the inspector!");
                return;
            }
            if (_fillTransform == null)
            {
                UnityEngine.Debug.LogWarning("[TransformProgressBar] Fill Transform is not assigned in the inspector!");
                return;
            }
            if (!_fillTransform.IsChildOf(_backgroundTransform))
            {
                UnityEngine.Debug.LogWarning("[TransformProgressBar] Fill Transform is not a child of Background Transform!");
                return;
            }

            var backScale = _backgroundTransform.localScale;
            var startFillLocalPos = backScale.x;
            _fillTransform.localPosition = Vector3.right * (startFillLocalPos * (_fillValue-1));
            _fillTransform.localScale = new Vector3(_fillValue, 1, 1);
        }

        public TweenerCore<float, float, FloatOptions> DOProgress(float endValue, float duration)
        {
            return DOTween.To(() => Progress, v => Progress = v, endValue, duration);
        }
    }
}
