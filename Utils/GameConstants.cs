﻿namespace Utils
{
    public static class GameConstants
    {
        public static readonly float FLOOR_HIGHT = 17f;
        public static readonly float BRICKS_EXPLOSION_DURATION = 0.2f;
    }
}