using System;
using Bricks;
using Common.Data;
using Common.GameStates;
using Common.GameStates.States;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Utils
{
    public static class GameHelper
    {
        private static string[] _shortsArray = { "", "K", "M", "B", "Tri", "Qua", "Qui", "Sxt", "Sep", "Oct", "Non", "Dec", "Und", "Duo", "Tre", "QT", "QD", "SD", "ST", "OD", "ND", "Vig", "AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "II", "JJ", "KK", "LL", "MM", "NN", "OO", "PP", "QQ", "RR", "SS", "TT", "UU", "VV", "WW", "XX", "YY", "ZZ", "aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh", "ii", "jj", "kk", "ll", "mm", "nn", "oo", "pp", "qq", "rr", "ss", "tt", "uu", "vv", "ww", "xx", "yy", "zz", "aaA", "aaB", "aaC", "aaD", "aaE", "aaF", "aaG", "aaH", "aaI", "aaJ", "aaK", "aaL", "aaM", "aaN", "aaO", "aaP", "aaQ", "aaR", "aaS", "aaT", "aaU", "aaV", "aaQ", "aaX", "aaY", "aaZ", "aAA", "aAB", "aAC", "aAD", "aAE", "aAF", "aAG", "aAH", "aAI", "aAJ", "aAK", "aAL", "aAM", "aAN", "aAO", "aAP", "aAQ", "aAR", "aAS", "aAT", "aAU", "aAV", "aAW", "aAX", "aAY", "aAZ" };
        public static Vector3 ProjectOnVectorScreenToWorldPoint(Vector3 screenPoint, Camera camera)
        {
            return camera.ScreenToWorldPoint(screenPoint);
        }
        public static Vector3 ProjectOnVectorScreenToWorldPoint(Vector3 screenPoint, Camera camera,
            Vector3 projectionVector)
        {
            var targetWorldPos = camera.ScreenToWorldPoint(screenPoint);
            return Vector3.Project(targetWorldPos, projectionVector);
        }

        public static float GetSizeMultiplier(this BrickSizeType type)
        {
            switch (type)
            {
                case BrickSizeType.X1:
                    return 1;
                case BrickSizeType.X2:
                    return 2;
                case BrickSizeType.X4Vertical:
                case BrickSizeType.X4Cube:
                    return 4;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public static Vector2 GetFloorPositionWithLevel(bool spawnAbove, Vector3 anchorPos)
        {
            return anchorPos.x * Vector2.right + ((spawnAbove ? 1 : 0) * GameConstants.FLOOR_HIGHT + anchorPos.y) * Vector2.up;
        }

        public static void GizmoDrawThickLine(Vector3 startPos, Vector3 endPos, float thickness)
        {
            Gizmos.DrawLine(startPos, startPos);
            Gizmos.DrawLine(startPos + Vector3.right * 0.005f, endPos + Vector3.right * 0.005f);
            Gizmos.DrawLine(startPos - Vector3.right * 0.005f, endPos - Vector3.right * 0.005f);
        }

        public static T LoadDataFromPlayerPrefs<T>(string prefsKey)
        {
            return JsonUtility.FromJson<T>(PlayerPrefs.GetString(prefsKey));
        }
        public static void SaveDataToPlayerPrefs<T>(T data, string prefsKey)
        {
            PlayerPrefs.SetString(prefsKey, JsonUtility.ToJson(data));
        }

        public static BaseGameState StateFromFloorType(this FloorType floorType)
        {
            switch (floorType)
            {
                case FloorType.Regular:
                    return new FloorStageState();
                case FloorType.Treasure:
                    throw new NotImplementedException();
                case FloorType.Boss:
                    throw new NotImplementedException();
                case FloorType.PitStop:
                    return new PitStopStageState();
                default:
                    throw new ArgumentOutOfRangeException(nameof(floorType), floorType, null);
            }
        }
        public static string CoinsFormat(Currency currency)
        {
            var number = currency.ToString();
            if (number.Length <= 4)
                return number;
            var triples = (number.Length - 1) / 3;
            return $"{currency * Math.Pow(0.1f, triples * 3): 0.0}" + _shortsArray[triples];
        }
    }
    public static class Vector2Extension
    {
        public static Vector2 Rotate(this Vector2 v, float degrees) {
            var sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            var cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
         
            var tx = v.x;
            var ty = v.y;
            var newX = cos * tx - sin * ty;
            var newY = sin * tx + cos * ty;
            return new Vector2(newX, newY);
        }
    }
}