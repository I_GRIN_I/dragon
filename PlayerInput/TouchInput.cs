using UniRx;
using UnityEngine;
using Zenject;

namespace PlayerInput
{
    public class TouchInput : ITickable, IPlayerInput
    {
        public ReadOnlyReactiveProperty<bool> IsInputActive => _isTouchPressed.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<Vector3> ScreenLastInputPosition => _touchLastPos.ToReadOnlyReactiveProperty();
        private ReactiveProperty<bool> _isTouchPressed = new ReactiveProperty<bool>();
        private ReactiveProperty<Vector3> _touchLastPos = new ReactiveProperty<Vector3>();
    
        public void Tick()
        {
            _isTouchPressed.Value = Input.touchCount > 0;
            if (_isTouchPressed.Value)
            {
                _touchLastPos.Value = Input.GetTouch(0).position;
            }
        }
    }
}
