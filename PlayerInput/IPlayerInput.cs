using UniRx;
using UnityEngine;

namespace PlayerInput
{
    public interface IPlayerInput
    {
        ReadOnlyReactiveProperty<bool> IsInputActive { get; }
        ReadOnlyReactiveProperty<Vector3> ScreenLastInputPosition { get; }
    }
}