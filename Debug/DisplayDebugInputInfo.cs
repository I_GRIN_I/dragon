using System;
using PlayerInput;
using UnityEngine;
using Zenject;

namespace DragonTowerDebug
{
    public class DisplayDebugInputInfo : DebugMonoBehaviour
    {
        private IPlayerInput _input;
        [SerializeField] private int _debugFontSize = 18;
        private GUIStyle _guiStyle = new GUIStyle();

        [Inject]
        protected void Construct(IPlayerInput input)
        {
            _input = input;
        }
        private void OnGUI()
        {
            int verticalLayoutPos = 10;
            _guiStyle.fontSize = _debugFontSize;
            int layoutHeight = _debugFontSize + 10;
            
            GUI.backgroundColor = Color.white;
            GUI.contentColor = Color.black;
            GUI.Label(new Rect(10,verticalLayoutPos, 200, layoutHeight), GetType().Name, _guiStyle);
            verticalLayoutPos += layoutHeight;
            if (_input == null)
            {
                GUI.Label(new Rect(10,verticalLayoutPos, 200, layoutHeight), $"{typeof(IPlayerInput).Name} is null!", _guiStyle);
                return;
            }
            else
            {
                GUI.Label(new Rect(10,verticalLayoutPos, 200, layoutHeight), $"{_input.GetType().Name} info:", _guiStyle);
                verticalLayoutPos += layoutHeight;
            }
            GUI.Label(new Rect(10,verticalLayoutPos, 200, layoutHeight), $"IsInputActive: {_input.IsInputActive}", _guiStyle);
            verticalLayoutPos += layoutHeight;
            GUI.Label(new Rect(10,verticalLayoutPos, 200, layoutHeight), $"ScreenLastInputPosition: {_input.ScreenLastInputPosition}", _guiStyle);
        }

    }
}