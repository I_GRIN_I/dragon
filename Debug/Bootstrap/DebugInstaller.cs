using UnityEngine;
using Zenject;

namespace DragonTowerDebug.Bootstrap
{
    public class DebugInstaller : MonoInstaller
    {
        [SerializeField] private DisplayDebugInputInfo _debugInputInfo;
        [SerializeField] private EnemyTestSpawner _enemyTestSpawner;
        [SerializeField] private BricksControllerDebug _bricksControllerDebug;
        [SerializeField] private EnemiesControllerDebug _enemiesControllerDebug;
        [SerializeField] private FloorsControllerDebug _floorsControllerDebug;
        [SerializeField] private DestroyFloorWhenHitSpaceDebug _destroyFloorWhenHitSpaceDebug;
        public override void InstallBindings()
        {
            if (_debugInputInfo != null)
                Container.BindInterfacesAndSelfTo<DisplayDebugInputInfo>().FromInstance(_debugInputInfo).AsSingle();
            if (_enemyTestSpawner != null)
                Container.BindInterfacesAndSelfTo<EnemyTestSpawner>().FromInstance(_enemyTestSpawner).AsSingle();
            if (_bricksControllerDebug != null)
                Container.BindInterfacesAndSelfTo<BricksControllerDebug>().FromInstance(_bricksControllerDebug).AsSingle();
            if (_enemiesControllerDebug != null)
                Container.BindInterfacesAndSelfTo<EnemiesControllerDebug>().FromInstance(_enemiesControllerDebug).AsSingle();
            if (_floorsControllerDebug != null)
                Container.BindInterfacesAndSelfTo<FloorsControllerDebug>().FromInstance(_floorsControllerDebug).AsSingle();
            if (_destroyFloorWhenHitSpaceDebug != null)
                Container.BindInterfacesAndSelfTo<DestroyFloorWhenHitSpaceDebug>().FromInstance(_destroyFloorWhenHitSpaceDebug).AsSingle();
        }
    }
}