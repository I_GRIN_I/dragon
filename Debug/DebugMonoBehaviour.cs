using System;
using UnityEngine;
using Zenject;

namespace DragonTowerDebug
{
    public abstract class DebugMonoBehaviour : MonoBehaviour, IInitializable
    {
        private bool _isEnabledManually;
        public void Awake()
        {
            _isEnabledManually = enabled;
            enabled = false;
        }
        public void Initialize()
        {
            enabled = _isEnabledManually;
        }
    }
}