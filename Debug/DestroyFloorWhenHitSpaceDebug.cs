﻿using System;
using Common.GameStates;
using Common.GameStates.States;
using Floors;
using UnityEngine;
using Zenject;
namespace DragonTowerDebug
{
    public class DestroyFloorWhenHitSpaceDebug : DebugMonoBehaviour, ITickable
    {
        private GameStateController _gameStateController;
        private FloorsController _floorsController;

        [Inject]
        private void Construct(GameStateController gameStateController, FloorsController floorsController)
        {
            _floorsController = floorsController;
            _gameStateController = gameStateController;
        }

        public void Tick()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                TryCleanFloor();
            }
        }

        private void TryCleanFloor()
        {
            if (_floorsController.CurrentFloor != null && _floorsController.CurrentFloor.IsInitialized &&
                _gameStateController.CurrentState is FloorStageState)
            {
                _floorsController.CurrentFloor.Dispose(true);
            }
        }

        private float heightMultiplier => Screen.height / 800f; 
        private float widthMultiplier => Screen.width / 480f;
        public void OnGUI()
        {
            var buttonWidth = 100 * widthMultiplier;
            var buttonHeight = 40f * heightMultiplier;
            
            GUI.skin.button.fontSize = Mathf.RoundToInt(buttonHeight / 3f);
            var pressed = GUI.Button(new Rect(Screen.width - buttonWidth, 100 * heightMultiplier, buttonWidth, buttonHeight), "Clear Floor!");
           if (pressed)
               TryCleanFloor();
        }
    }
}