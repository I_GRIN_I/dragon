using Entities;
using Entities.Player.Controllers;
using Entities.Player.Data;
using PlayerInput;
using UnityEngine;
using Utils;
using Zenject;

namespace DragonTowerDebug
{
    public class DisplayTargetGizmoInfo : DebugMonoBehaviour
    {
        [Inject(Id = TransformType.PlayerAnchor)]
        private readonly Transform _anchorTransform;
        private IPlayerInput _input;
        private Camera _camera;
        private IAimController _aimController;
        private MoveByTweenController _moveController;

        [Inject]
        protected void Construct(IPlayerInput input, Camera sceneCamera, IAimController aimController,
            MoveByTweenController moveController)
        {
            _moveController = moveController;
            _aimController = aimController;
            _camera = sceneCamera;
            _input = input;
        }

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying)
                return;
        
            if (_input == null)
            {
                UnityEngine.Debug.LogError($"{nameof(IPlayerInput)} is null in {GetType().Name}");
                return;
            }
            if (_input.IsInputActive.Value)
            {
                Gizmos.color = Color.red;
                DrawMoveTargetLine();
                Gizmos.color = Color.magenta;
                DrawMoveVelocityLine();
            }
            if (_aimController.IsAiming.Value)
            {
                Gizmos.color = Color.yellow;
                DrawAimTargetLine();
            }
        }

        private void DrawMoveTargetLine()
        {
            var playerPosition = _moveController.CurrentWorldPosition.Value;
            var normalizedVertical =
                GameHelper.ProjectOnVectorScreenToWorldPoint(_input.ScreenLastInputPosition.Value, _camera,
                    Vector3.up) + _anchorTransform.position;
            GameHelper.GizmoDrawThickLine(playerPosition, normalizedVertical, 0.005f);
        }
        private void DrawMoveVelocityLine()
        {
            var playerPosition = _moveController.CurrentWorldPosition.Value;
            GameHelper.GizmoDrawThickLine(playerPosition, playerPosition + _moveController.CurrentVelocity, 0.005f);
        }
        
        private void DrawAimTargetLine()
        {
            var playerPosition = _aimController.OriginWorldPosition.Value;
            GameHelper.GizmoDrawThickLine(playerPosition, _aimController.TargetWorldPosition.Value, 0.008f);
        }
    }
}
