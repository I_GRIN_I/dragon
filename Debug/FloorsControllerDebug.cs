using System.Collections.Generic;
using Common.Signals;
using Floors;
using UniRx;
using UnityEngine;
using Zenject;

namespace DragonTowerDebug
{
    public class FloorsControllerDebug : DebugMonoBehaviour
    {
        [SerializeField] private List<string> FloorIds;
        private SignalBus _signalBus;
        private FloorsController _floorsController;

        [Inject]
        private void Construct(FloorsController floorsController, SignalBus signalBus)
        {
            _floorsController = floorsController;
            _signalBus = signalBus;
        }
        private void Start()
        {
            int i = 0;
            foreach (var floorId in FloorIds)
            {
                _floorsController.InitializeFloor(_floorsController.GetFloorIdByLevel(i));
                // onInited.Subscribe(floor =>
                // {
                //     floor.AreAllEnemiesKilled.Subscribe(_ => _floorsController.DestroyFloor(floor.FloorId)).AddTo(this);
                // }).AddTo(this);
                i++;
            }
        }
    }
}