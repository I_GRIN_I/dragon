using Common.Settings;
using UnityEngine;
using Zenject;

namespace DragonTowerDebug
{
    public class GeneralSettingDebugLogger : MonoBehaviour
    {
        [Inject]
        private void Construct(GeneralSettings generalSettings)
        {
            if (generalSettings != null)
            {
                UnityEngine.Debug.Log(generalSettings);
                var fields = generalSettings.GetType().GetFields();
                foreach (var fieldInfo in fields)
                {
                    UnityEngine.Debug.Log($"\t {fieldInfo.Name} : {fieldInfo.GetValue(generalSettings)}");
                }
            }
            else
            {
                UnityEngine.Debug.Log("GeneralSettings is null!");
            }
        }
    }
}
