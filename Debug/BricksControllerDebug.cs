using System;
using Bricks;
using Common.Signals;
using UniRx;
using UnityEngine;
using Zenject;

namespace DragonTowerDebug
{
    public class BricksControllerDebug : DebugMonoBehaviour
    {
        private BricksController _bricksController;
        [SerializeField] private string FloorId = "floor1";
        private SignalBus _signalBus;

        [Inject]
        private void Construct(BricksController bricksController, SignalBus signalBus)
        {
            _signalBus = signalBus;
            _bricksController = bricksController;
        }

        private void Start()
        {
            _signalBus.Subscribe<GameSignals.BrickDestroyed>(OnBrickDestroyed);
            if (_bricksController.IsInitialized(FloorId))
            {
                OnFloorInitialized(FloorId);
            }
            else
            {
                _bricksController.AreFloorBricksInitialized.Where(floorId => floorId.Equals(FloorId))
                    .Subscribe(OnFloorInitialized).AddTo(this);
            }
        }

        private void OnBrickDestroyed(GameSignals.BrickDestroyed signal)
        {
            UnityEngine.Debug.Log($"Brick \"{signal.Brick.name}\" type of {signal.Brick.Id}" +
                                  $" with initial HP {signal.Brick.MaxHp.Value} on {signal.FloorId} was destroyed");
        }

        private void OnFloorInitialized(string floorId)
        {
            _bricksController.GetAliveBricks(floorId).ObserveCountChanged().Where(count => count <= 0)
                .Subscribe(_ => UnityEngine.Debug.Log($"Bricks on {floorId} is cleared!")).AddTo(this);
        }
    }
}