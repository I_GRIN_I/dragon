using Common.Signals;
using Entities.Enemy;
using UniRx;
using UnityEngine;
using Zenject;

namespace DragonTowerDebug
{
    public class EnemiesControllerDebug : DebugMonoBehaviour
    {
        private EnemiesController _enemiesController;
        [SerializeField] private string FloorId = "floor1";
        private SignalBus _signalBus;

        [Inject]
        private void Construct(EnemiesController enemiesController, SignalBus signalBus)
        {
            _signalBus = signalBus;
            _enemiesController = enemiesController;
            
        }
        private void Start()
        {
            _signalBus.Subscribe<GameSignals.EnemyKilled>(OnEnemyKilled);
            
            if (_enemiesController.IsInitialized(FloorId))
            {
                OnFloorInitialized(FloorId);
            }
            else
            {
                _enemiesController.AreFloorEnemiesInitialized.Where(floorId => floorId.Equals(FloorId))
                    .Subscribe(OnFloorInitialized).AddTo(this);
            }
        }
        private void OnEnemyKilled(GameSignals.EnemyKilled signal)
        {
            UnityEngine.Debug.Log($"Enemy \"{signal.Enemy.name}\" type of {signal.Enemy.Settings.Id}" +
                                  $" with initial HP {signal.Enemy.MaxHp.Value} on {signal.FloorId} was destroyed");
        }

        private void OnFloorInitialized(string floorId)
        {
            _enemiesController.GetAliveEnemies(floorId)
                .ObserveCountChanged()
                .Where(count => count <= 0)
                .Subscribe(_ => UnityEngine.Debug.Log($"Enemies on {floorId} is cleared!")).AddTo(this);
        }
    }
}