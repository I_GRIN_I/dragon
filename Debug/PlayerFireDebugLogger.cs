using Common.Signals;
using UnityEngine;
using Zenject;

namespace DragonTowerDebug
{
    public class PlayerFireDebugLogger : MonoBehaviour
    {
        [Inject]
        private void Construct(SignalBus signalBus)
        {
            signalBus.Subscribe<GameSignals.PlayerFiredProjectile>(signal =>
            {
                UnityEngine.Debug.Log($"Fired projectile from start Pos. {signal.StartPosition} in Direction {signal.Direction}");
            });
        }
    }
}