using System;
using System.Collections.Generic;
using Entities.Enemy;
using UnityEngine;
using Zenject;

namespace DragonTowerDebug
{
    public class EnemyTestSpawner : DebugMonoBehaviour
    {
        [SerializeField] private List<EnemySpawnPoints> _spawnPoints;
        private EnemiesManager _enemiesManager;

        [Inject]
        private void Construct(EnemiesManager enemiesManager)
        {
            _enemiesManager = enemiesManager;
        }

        private void Start()
        {
            foreach (var point in _spawnPoints)
            {
                _enemiesManager.InvokeEnemy(point.EnemyId, point.SpawnTransform.position);
            }
        }
    }
    [Serializable]
    public class EnemySpawnPoints
    {
        public string EnemyId;
        public Transform SpawnTransform;
    }
}
