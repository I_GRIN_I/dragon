using DG.Tweening;
using TMPro;
using UnityEngine;

namespace UI
{
    public class FloorItemView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _floor;

        public void UpdateView(long level)
        {
            _floor.text = level.ToString();
            gameObject.SetActive(level > 0);
        }
    }
}
