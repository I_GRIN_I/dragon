using System;
using System.Globalization;
using Common.Data;
using Common.Signals;
using DG.Tweening;
using Economics;
using TMPro;
using UI.Popup;
using UnityEngine;
using UnityEngine.UI;
using Upgrades;
using Utils;
using Zenject;

namespace UI
{
    public enum DisplayType
    {
        None,
        Value,
        Percent
    }
    public class UpgradeCard : MonoBehaviour, IView
    {
        [SerializeField] private UpgradeType _type;
        [SerializeField] private DisplayType _displayType;
        [SerializeField] private TextMeshProUGUI _level;
        [SerializeField] private TextMeshProUGUI _cost;
        [SerializeField] private TextMeshProUGUI _upgradeValue;
        [SerializeField] private Button _button;
        
        private UpgradesController _upgradesController;
        private SignalBus _signalBus;
        private CurrencyController _currencyController;
        private Tween _buyAnimation;

        [Inject]
        public void Construct(SignalBus signalBus, UpgradesController upgradesController, CurrencyController currencyController)
        {
            _currencyController = currencyController;
            _signalBus = signalBus;
            _upgradesController = upgradesController;
        }

        private void Start()
        {
            _button?.onClick.AddListener(() => {
                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "click" });

                _upgradesController.TryToBuyUpgrade(_type);
                if (_buyAnimation.IsActive())
                    _buyAnimation.Kill(true);
                _buyAnimation = transform.DOPunchScale(Vector3.one * 0.1f, 0.5f).OnComplete(()=>UpdateView());
            });
            _signalBus.Subscribe<UpgradesSignals.UpgradeUpdated>(UpdateView);
            UpdateView();
        }
        

        private void UpdateView()
        {
            var upgradePrice = _upgradesController.GetUpgradeCost(_type);
            var playerWallet = _currencyController.Wallet;
            if(_button)
                _button.interactable = playerWallet >= upgradePrice;
            var upgradeLevel = _upgradesController.GetUpgradeLevel(_type) + 1;
            _level.text = $"LVL {upgradeLevel}";
            if(_cost)
                _cost.text = GameHelper.CoinsFormat(upgradePrice);
            if (_upgradeValue)
            {
                _upgradeValue.text = _displayType.Equals(DisplayType.None) ? "" :
                    (_displayType.Equals(DisplayType.Percent) ?
                    $"{_upgradesController.GetUpgradedValue(_type) * 100: 0}%"
                    : $"{_upgradesController.GetUpgradedValue(_type): 0.00}{(_type.Equals(UpgradeType.ChargeTime)?"/s":"")}");
            }
        }

        public void Show()
        {
            if (gameObject.activeSelf) return;
            gameObject.SetActive(true);
            UpdateView();
            transform.DOScale(Vector3.one, 0.65f).From(Vector3.zero).SetEase(Ease.InBounce);
        }

        public void Hide()
        {
            transform.DOScale(Vector3.zero, 0.65f).SetEase(Ease.InBounce).OnComplete(() => gameObject.SetActive(false));
        }

        private void OnDestroy()
        {
            _button?.onClick.RemoveAllListeners();
            _signalBus.TryUnsubscribe<UpgradesSignals.UpgradeUpdated>(UpdateView);
        }
    }
}
