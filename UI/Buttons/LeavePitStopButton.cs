using Common.Data;
using Common.Signals;
using DG.Tweening;
using UI.Popup;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI.Buttons
{
    [RequireComponent(typeof(Button))]
    public class LeavePitStopButton : MonoBehaviour, IView
    {
        [SerializeField] private GameObject _buttonObj;
        [SerializeField] private Button Button;
        private SignalBus _signalBus;

        [Inject]
        void Construct(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }


        public void OnDestroy()
        {
            Button.onClick.RemoveAllListeners();
        }

        public void Show()
        {
            if (_buttonObj.activeSelf) return;
            Button.onClick.AddListener(()=> {
                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "click" });
                _signalBus.Fire<ViewSignals.LeavePitStopButtonHit>();
            });
            _buttonObj.SetActive(true);
            DOTween.Sequence().Append(_buttonObj.transform.DOScaleY(1.2f, 0.65f).From(0).SetEase(Ease.InBack))
                .Append(_buttonObj.transform.DOScaleY(1f, 0.25f).SetEase(Ease.OutBack));
        }

        public void Hide()
        {
            Button.onClick.RemoveAllListeners();
            _buttonObj.transform.DOScaleY(0,  0.4f).SetEase(Ease.Flash).OnComplete(() => _buttonObj.SetActive(false));
        }
    }
}