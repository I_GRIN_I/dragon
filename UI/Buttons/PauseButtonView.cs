﻿using System.Collections.Generic;
using Common.Data;
using Common.Signals;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Progress;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI.Buttons
{
    [RequireComponent(typeof(Button))]
    public class PauseButtonView : MonoBehaviour
    {
        [SerializeField] private RectTransform _unfoldedTransform;
        [Space]
        [SerializeField] private RectTransform _mapContainer;
        [SerializeField] private Ease _slideEase;
        [Space]
        [SerializeField] private FloorItemView _currentView;
        [SerializeField] private List<FloorItemView> _pastViews;
        [SerializeField] private List<FloorItemView> _futureViews;
        
        protected Button _button;
        private SignalBus _signalBus;
        private Tween _tween;

        private Vector3 _foldedPos;
        private ProgressController _progressController;
        private Image _image;

        [Inject]
        void Construct(SignalBus signalBus, ProgressController  progressController)
        {
            _progressController = progressController;
            _signalBus = signalBus;
            
            _signalBus.Subscribe<ViewSignals.SetActiveView>(IsActiveButton);
        }

        private void IsActiveButton(ViewSignals.SetActiveView signal)
        {
            if (!signal.ViewType.Equals(ViewType.PauseButton))
                return;

            var tartgetPos = signal.IsActive ? _foldedPos : _unfoldedTransform.position;
            _image.raycastTarget = signal.IsActive;


            if (_tween.IsActive())
            {
                _tween.Kill(true);
            }
            _tween = _mapContainer.DOMove(tartgetPos, 0.3f).SetEase(_slideEase);
        }
        
        private void UpdateViews()
        {
            var level = _progressController.Level.Value + 1;
            _currentView.UpdateView(level);
            for (int i = 0; i < _pastViews.Count; i++)
            {
                _pastViews[i].UpdateView(level - i - 1);
            }
            for (int i = 0; i < _futureViews.Count; i++)
            {
                _futureViews[i].UpdateView(level + i + 1);
            }
        }

        private void Awake()
        {
            _foldedPos = _mapContainer.position;
            _button = GetComponent<Button>();
            _image = GetComponent<Image>();
        }

        private void Start()
        {
            _button.onClick.AddListener(()=> {
                _signalBus.Fire<PauseSignals.PauseFloorRequest>();
                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "click" });
            });
            _progressController.Level.Subscribe(_ => UpdateViews()).AddTo(this);
            UpdateViews();
            IsActiveButton(new ViewSignals.SetActiveView(ViewType.PauseButton,true));
        }

        public void OnDestroy()
        {
            if (_tween.IsActive())
            {
                _tween.Kill(true);
            }
            _button.onClick.RemoveAllListeners();
            _signalBus.TryUnsubscribe<ViewSignals.SetActiveView>(IsActiveButton);
        }
    }
}