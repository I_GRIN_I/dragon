using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Zenject;
using Common.Signals;

[RequireComponent(typeof(Toggle))]
public class DeclineableToggle : MonoBehaviour
{
    [SerializeField]
    private GameObject _declineSymbol;
    private Toggle _toggle;
    private SignalBus _signalBus;
    [Inject]
    private void Initialize(SignalBus signalBus)
    {
        _signalBus = signalBus;
    }
    private void Awake()
    {
        _toggle = GetComponent<Toggle>();
        _declineSymbol.SetActive(!_toggle.isOn);
        _toggle.onValueChanged.AsObservable()
            .Subscribe(x =>
            {
                _signalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "click" });
                _declineSymbol.SetActive(!x);
            });
    }
}
