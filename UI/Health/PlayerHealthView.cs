using System;
using System.Collections.Generic;
using Common.Settings;
using Entities.Player;
using UniRx;
using UnityEngine;
using Zenject;

namespace UI.Health
{
    public class PlayerHealthView : MonoBehaviour
    {
        private PlayerFacade _playerFacade;
        private List<HeartView> _heartsViews = new List<HeartView>();
        private HeartView.Factory _heartsFactory;
        private PlayerSettings _playerSettings;

        [Inject]
        private void Construct(PlayerFacade playerFacade, HeartView.Factory heartsFactory, PlayerSettings playerSettings)
        {
            _playerSettings = playerSettings;
            _heartsFactory = heartsFactory;
            _playerFacade = playerFacade;
        }

        private void Start()
        {
            ReInitializeHealthBar();
            _playerFacade.MaxHp.Subscribe(_ => ReInitializeHealthBar()).AddTo(this);
            _playerFacade.CurrentHp.Subscribe(_ => UpdateViews()).AddTo(this);
        }

        private void ReInitializeHealthBar()
        {
            Dispose();
            var heartsCount = Mathf.CeilToInt(_playerFacade.MaxHp.Value);
            for (int i = 0; i < heartsCount; i++)
            {
                var view = _heartsFactory.Create(_playerSettings.HealthPrefab);
                view.Initialize(transform);
                _heartsViews.Add(view);
            }
            UpdateViews();
        }

        private void UpdateViews()
        {
            for (int i = 0; i < _heartsViews.Count; i++)
            {
                var view = _heartsViews[i];
                var percent = Mathf.Clamp01(_playerFacade.CurrentHp.Value - i);
                view.UpdateView(percent);
            }
        }
        private void Dispose()
        {
            _heartsViews.ForEach(view => view.Dispose());
            _heartsViews.Clear();
        }
    }
}
