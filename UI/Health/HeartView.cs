using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Object = UnityEngine.Object;

namespace UI.Health
{
    [Serializable]
    internal class HeartStageSettings
    {
        public Sprite Image;
    }
    
    public class HeartView : MonoBehaviour
    {
        [SerializeField] private Image _heartImage;
        [Header("Settings")]
        [SerializeField] private List<HeartStageSettings> _heartViewSettings;

        private Tween _heartAnim;
        private float lastValue = 1f;
        public class Factory : PlaceholderFactory<Object, HeartView>
        { }

        public void Initialize(Transform parent)
        {
            transform.SetParent(parent);
            transform.localScale = Vector3.one;
        }

        public void UpdateView(float heartPercent)
        {
            if (Mathf.Approximately(lastValue, heartPercent))
                return;
            lastValue = heartPercent;
            var index = Mathf.RoundToInt(Mathf.Lerp(_heartViewSettings.Count - 1, 0, heartPercent));
            var setting = _heartViewSettings[index];
            _heartImage.sprite = setting?.Image;
            
            Dispose();
            _heartAnim = transform.DOPunchScale(0.25f * Vector3.one, 0.5f);
        }

        public void Dispose()
        {
            if (_heartAnim.IsActive())
                _heartAnim.Kill(true);
        }
    }
}
