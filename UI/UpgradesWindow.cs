using System;
using System.Collections.Generic;
using Common.Signals;
using UI.Buttons;
using UniRx;
using UnityEngine;
using Zenject;

namespace UI
{
    public class UpgradesWindow : MonoBehaviour
    {
        [SerializeField] private float _delayOffset = 0f;
        [SerializeField]
        private List<UpgradeCard> _cards;
        [SerializeField]
        private LeavePitStopButton _button;

        private SignalBus _signalBus;
        private IDisposable _showStream;
        private IDisposable _hideStream;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        private void Start()
        {
            _signalBus.Subscribe<UpgradesSignals.ShowWindow>(ShowViews);
            _signalBus.Subscribe<UpgradesSignals.HideWindow>(HideViews);
        }

        private void OnDestroy()
        {
            _signalBus.TryUnsubscribe<UpgradesSignals.ShowWindow>(ShowViews);
            _signalBus.TryUnsubscribe<UpgradesSignals.HideWindow>(HideViews);
            _showStream?.Dispose();
            _hideStream?.Dispose();
        }

        public void ShowViews()
        {
            _showStream = Observable.Interval(TimeSpan.FromSeconds(_delayOffset))
                .Subscribe(index =>
                {
                    if (index < _cards.Count)
                        _cards[(int) index].Show();
                    else
                        _showStream.Dispose();
                });
            _button.Show();
        }

        public void HideViews()
        {
            _hideStream = Observable.Interval(TimeSpan.FromSeconds(_delayOffset))
                .Subscribe(index =>
                {
                    if (index < _cards.Count)
                        _cards[(int) index].Hide();
                    else
                        _hideStream.Dispose();
                });
            _button.Hide();
        }
    }
}
