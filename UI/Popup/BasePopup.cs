﻿using Common.Data;
using Common.Signals;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace UI.Popup
{
    public abstract class BasePopup : MonoBehaviour
    {
        [SerializeField] private float _animationDuration = 0.7f;
        [SerializeField] private CanvasGroup _canvasGroup;

        private Tween _animationTween;
        protected SignalBus SignalBus;

        public abstract ViewType ViewType { get; }
        
        [Inject]
        private void Construct(SignalBus signalBus)
        {
            gameObject.SetActive(false);
            SignalBus = signalBus;
            SignalBus.Subscribe<ViewSignals.SetActiveView>(IsActiveView);
        }

        private void IsActiveView(ViewSignals.SetActiveView signal)
        {
            if (!signal.ViewType.Equals(ViewType))
                return;
            
            
            if (signal.IsActive)
                Show();
            else
                Hide();
        }
        public virtual void Show()
        {
            gameObject.SetActive(true);
            
            if (_animationTween.IsActive())
            {
                _animationTween.Kill();
            }
            _animationTween = _canvasGroup.DOFade(1f, _animationDuration).From(0f).SetRecyclable(false);
        }

        public void Hide()
        {
            if (_animationTween.IsActive())
            {
                _animationTween.Kill();
            }
            gameObject.SetActive(false);
            // _animationTween = _canvasGroup.DOFade(0f, _animationDuration).From(1f)
            //     .OnComplete((() => gameObject.SetActive(false))).SetRecyclable(false);
        }
        
        public virtual void OnDestroy()
        {
            SignalBus.TryUnsubscribe<ViewSignals.SetActiveView>(IsActiveView);
        }
    }
}