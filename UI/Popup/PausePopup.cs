using Common.Data;
using Common.Signals;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class PausePopup : BasePopup
    {
        public override ViewType ViewType => ViewType.PauseView;
        [SerializeField] private Button _resumeButton;
        
    
        private void Start()
        {
            _resumeButton.onClick.AddListener(()=> SignalBus.Fire<PauseSignals.ResumeFloorRequest>());
        }
        
        public override void OnDestroy()
        {
            base.OnDestroy();
            _resumeButton.onClick.RemoveAllListeners();
        }
    }
}
