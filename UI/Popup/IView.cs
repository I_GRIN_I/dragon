﻿namespace UI.Popup
{
    
    public interface IView
    {
        public void Show();
        public void Hide();
    }
}