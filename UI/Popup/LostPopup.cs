﻿using Common.Data;
using Common.Signals;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class LostPopup : BasePopup
    {
        [SerializeField] private Button _retryButton;

        public override ViewType ViewType => ViewType.LostView;
        
        private void Start()
        {
            _retryButton.onClick.AddListener(()=> {
                SignalBus.Fire(new AudioSignals.PlaySound() { RandomPitch = true, SoundName = "click" });
                SignalBus.Fire<GameSignals.RetryRequest>(); 
            });
        }
        
        public override void OnDestroy()
        {
            base.OnDestroy();
            _retryButton.onClick.RemoveAllListeners();
        }
        
    }
}